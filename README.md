# XiVO PBX mobile application

This repository contains sources of XiVO mobile application that you can find on Android or iOS store. As a foreword, it is neccesary to mention that this project took its root from a POC that with the time became our standard.

That may explain why you can 'still' find awkward code or some parts that are not definitely state of the art.

## Intro

This application has been developped a while ago (2017) to be able to create a webrtc softphone on mobile for XiVO VOIP solution. It survived along the upgrades of iOS and ANdroid version until it faced battery savers policy that forced a completly rewrite to avoid background poll service and use standard push notification which is a must now for recent mobile OS versions.

## Feature overview

*   [x] **Telephoy** all  XiVO CTI features (dial, hangup, transfer...)
*   [x] **Device management** to switch between Softphone on PC orkeep to receive call on mobile
*   [x] **Directory** to find users aong org directory or among favorites
*   [x] **Hiistory** to see your last calls 


## How to build the app

The app is based on react native to keep only one codebase for both OS.
It is recommended to be fammiliar with RN framework and VOIP application before trying to run/build this project for your own need.

It is definitely really high coupled to XiVO and cannot be used easily as a seed.


### Requirements

* Use git to clone this repository into your computer.
* Having node 20 (nvm is highly recommended)
* Having a working XiVO with an edge module + firebase / apns valid account to send push notifications.

### Build Android

First install dependencies in root folder of the project

```
npm i 
```

Then launch react native to create the Android js bundle and kotlin sources associated

```
npx react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res/
```

### Build iOS

First install dependencies in root folder of the project

```
npm i 
```

Then launch react native to create the iOS js bundle and swift sources associated

```
npx react-native bundle --dev false --entry-file index.js --bundle-output ios/main.jsbundle --platform ios
```

### Note 

Pay attention to not git either js bundle or native code generated


## How to run the app (on simulator, or attached device)

First you need to have build once the bundle or use metro watcher to rebuild your web app on the fly.

### Run on Android

```
npx react-native run-android
```

### Run on iOS

```
npx react-native run-ios
```

## Testing

No tests no sucess... this is a huge WIP to catch up this.

## Logging

All business code is part of module, some modules have been pinpointed to focus on some app behaviour such as:

* **[Permission]** Related to permission granted by the user to the app
* **[Login]** Login process and token renewal
* **[Notification]** Local Notification for missed call and also remote push management registration/wake up
* **[System]** LOw level foreground process to keep the app awake when receiving VOIP calls
* **[WebSockets]** Websockets lifecyle for both Xuc (CTI) and Asterisk (SIP)
* **[Webrtc]**  actions / events related to the protocol  
* **[Audio]**  Ringing tones and audio routes  

## Contribute

Pull requests are welcome, even just a thumb up to not feel alone in this repository!


## License
[MIT](https://choosealicense.com/licenses/mit/)



