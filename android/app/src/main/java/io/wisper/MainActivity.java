package io.wisper;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.defaults.DefaultNewArchitectureEntryPoint;
import com.facebook.react.defaults.DefaultReactActivityDelegate;
import android.os.Bundle;

import androidx.annotation.NonNull;


import java.util.Objects;

import io.wazo.callkeep.RNCallKeepModule;

public class MainActivity extends ReactActivity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(null);
		SettingsCompat.canDrawOverlays(getApplicationContext());
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RNCallKeepModule.REQUEST_READ_PHONE_STATE) {
            RNCallKeepModule.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
	}

	/**
	 * Returns the instance of the {@link ReactActivityDelegate}. Here we use a util class {@link
	 * DefaultReactActivityDelegate} which allows you to easily enable Fabric and Concurrent React
	 * (aka React 18) with two boolean flags.
	 */
	@Override
	protected ReactActivityDelegate createReactActivityDelegate() {
		return new DefaultReactActivityDelegate(
			this, Objects.requireNonNull(getMainComponentName()),
			// If you opted-in for the New Architecture, we enable the Fabric Renderer.
			DefaultNewArchitectureEntryPoint.getFabricEnabled(), // fabricEnabled
			// If you opted-in for the New Architecture, we enable Concurrent React (i.e. React 18).
			DefaultNewArchitectureEntryPoint.getConcurrentReactEnabled() // concurrentRootEnabled
			);
	}

	/**
	 * Returns the name of the main component registered from JavaScript. This is used to schedule
	 * rendering of the component.
	 */
	@Override
	protected String getMainComponentName() {
		return "Wisper";
	}
}