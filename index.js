import {AppRegistry} from "react-native";
import {FileLogger} from "react-native-file-logger";
let ReactNativeForegroundService;
if (Platform.OS === 'android') {
  ReactNativeForegroundService = require('rn-foreground-service').default;
} else {
  ReactNativeForegroundService = null;
}


import App from "@src/App";
import {name as appName} from "./app.json";

import NotificationManager from "@helpers/NotificationManager";

FileLogger.configure();

console.log("------====== Starting xivo-mobile application ======------");

NotificationManager.init();
if (Platform.OS !== "ios") {
    ReactNativeForegroundService.register({
        config : {
          alert : true,
          onServiceErrorCallBack : () => {
            console.error("[System] Error while running foreground service", arguments);
          }
        }
      });
}

AppRegistry.registerComponent(appName, () => App);
