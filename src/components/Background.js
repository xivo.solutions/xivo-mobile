import React, { useCallback, useEffect } from "react";
import {
  AppState,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { EventRegister } from "react-native-event-listeners";

import EventNames from "@configs/events";
import { Data } from "@redux/actions";

const Background: () => React$Node = (props) => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.data);

  const handleAppStateChange = useCallback((nextAppState) => {
    EventRegister.emit(EventNames.APP_CHANGE_STATE, nextAppState);
  }, [data]);

  useEffect(() => {
    let event = AppState.addEventListener("change", handleAppStateChange);
    return () => {
      event.remove();
    };
  }, [handleAppStateChange]);

  useEffect(() => {
    EventRegister.addEventListener(EventNames.CTI.FETCH_DATA, (key) => {
      dispatch(Data.fetchData(key));
    });

    EventRegister.addEventListener(EventNames.CTI.ADD_DATA, (d) => {
      dispatch(Data.addData(d.key, d.data, typeof d.info !== "undefined" ? d.info : {}));
    });

    EventRegister.addEventListener(EventNames.CTI.UPDATE_STATUS_DATA, (d) => {
      dispatch(Data.upateStatusData(d));
    });

    EventRegister.addEventListener(EventNames.CTI.UPDATE_FAVORITES, (d) => {
      dispatch(Data.updateFavorites(d));
    });

    EventRegister.addEventListener(EventNames.CTI.UPDATE_VOICEMAIL, (d) => {
      dispatch(Data.updateVoiceMail(d));
    });

    EventRegister.addEventListener(EventNames.CTI.ATTACH_EVENT, (event) => {
      dispatch(Data.attachEvent(event));
    });

    return () => {
      EventRegister.removeEventListener(EventNames.CTI.FETCH_DATA);
      EventRegister.removeEventListener(EventNames.CTI.ADD_DATA);
      EventRegister.removeEventListener(EventNames.CTI.UPDATE_STATUS_DATA);
      EventRegister.removeEventListener(EventNames.CTI.UPDATE_FAVORITES);
      EventRegister.removeEventListener(EventNames.CTI.UPDATE_VOICEMAIL);
      EventRegister.removeEventListener(EventNames.CTI.ATTACH_EVENT);
    }
  }, []);

  return (null);
};

export default Background;
