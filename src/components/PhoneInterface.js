import React from "react";
import {
    Dimensions,
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from "react-native";
import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import {    
    faDeleteLeft,
    faPhone
} from "@fortawesome/free-solid-svg-icons";

import Colors from "@assets/styles/colors";
import SocketsManager from "@helpers/SocketsManager";
import ButtonChildren from "@components/ButtonChildren";
import Screen from "../helpers/Screen";


const PhoneInterface: () => React$Node = (props) => {

    const [number, setNumber] = React.useState("");

    return (
        <View>
            <View style={styles.wrapperNumbersTop}>
                <Text numberOfLines={1} style={styles.numberComposeText}>{number}</Text>

                {number.length > 0 && (
                    <TouchableOpacity
                        onPress={() => {
                            setNumber(number.slice(0, -1));
                        }}
                    >
                        <FontAwesomeIcon
                            icon={faDeleteLeft}
                            color={Colors.grey}
                            size={25}
                            style={{
                                margin: 10,
                            }}
                        />
                    </TouchableOpacity>
                )}
            </View>
            <View style={[styles.wrapperNumbers, props.style !== undefined ? props.style : {}]}>
                {[
                    ["1", "2", "3"],
                    ["4", "5", "6"],
                    ["7", "8", "9"],
                    ["*", "0", "#"],
                ].map((line, indexLine) => {
                    return (
                        <View key={"viewLine" + indexLine} style={styles.numbersLine}>
                            {line.map((num, index) => {
                                return (
                                    <TouchableOpacity
                                        key={"number" + index}
                                        style={styles.btnNumberPhone}
                                        onPress={() => {
                                            setNumber(number + "" + num);
                                        }}
                                    >
                                        <Text style={styles.btnNumberPhoneText}>{num}</Text>
                                    </TouchableOpacity>
                                );
                            })}
                        </View>
                    );
                })}
                <View style={styles.wrapperNumbersBottom}>
                    {number.length > 0 && (
                        <ButtonChildren onPress={() => {
                            SocketsManager.sip.call(number);
                            setNumber("");
                        }} >
                            <FontAwesomeIcon icon={faPhone} color={Colors.green} size={50}/>
                        </ButtonChildren>
                    )}
                </View>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    wrapperNumbersTop: {
        height: 41,
        marginTop: 10,
        marginBottom: 10,
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: "row",
    },
    numberComposeText: {
        fontSize: 34,
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Book",
        } : {}),
        color: Colors.greyDark,
        width: "80%",
        textAlign: "center",
    },
    wrapperNumbers: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        height: Screen.RPH(50),
    },
    numbersLine: {
        flex: 1,
        flexDirection: "row",
        marginBottom: 0,
    },
    btnNumberPhone: {        
        backgroundColor: Colors.whiteDark,
        marginBottom : 0,
        marginTop : 0,
        marginRight : 10,
        marginLeft : 10,
        borderRadius: 50,
        height: Screen.RPH(8),
        width: Screen.RPW(20),
        textAlign: "center",
        alignItems: "center",
        justifyContent: "center",
    },
    btnNumberPhoneText: {
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Book",
        } : {}),
        color: Colors.greyDark,
        fontSize: 30,
    },
    wrapperNumbersBottom: {
        height: 50,
        marginTop: 15,
        marginBottom: 10,
        alignItems: "center",
        justifyContent: "center",
    }
});

export default PhoneInterface;
