import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
} from "react-native";
import btnsStyle from "@styles/buttons";

const Button: () => React$Node = (props) => {
  let style = btnsStyle.btn;
  if (props.color && btnsStyle[props.color]) {
    style = { ...btnsStyle.btn, ...btnsStyle[props.color] };
  }
  if (props.style) {
    style = { ...style, ...props.style };
  }


  return (
    <TouchableOpacity onPress={props.onPress.bind(this)}>
      <View style={style}>
        {props.text && <Text style={btnsStyle.text}>{props.text}</Text>}
        {props.children && props.children}
      </View>
    </TouchableOpacity>
  );
};
export default Button;
