import React, {useEffect} from "react";
import Contacts from "react-native-contacts";
import {parsePhoneNumberFromString} from "libphonenumber-js";
import {request, PERMISSIONS, check, openSettings} from "react-native-permissions";
import {useDispatch} from "react-redux";
import {ifIphoneX} from "react-native-iphone-x-helper";

import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Modal,
    Platform,
    Linking,
} from "react-native";
import {EventRegister} from "react-native-event-listeners";
import VersionCheck from "react-native-version-check";

import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import {
    faCircleCheck,
    faCircleXmark,
    faServer,
    faPowerOff,
    faTimes,
    faSquareEnvelope,
} from "@fortawesome/free-solid-svg-icons";


import Colors from "@assets/styles/colors";
import ButtonChildren from "@components/ButtonChildren";
import ModalSendLog from "@components/ModalSendLog";
import LoginHelper from "@helpers/Login";
import EventNames from "@configs/events";
import Constants from "@configs/constants";
import ModalServer from "@components/ModalServer";

import {Data} from "@redux/actions";

const Menu: () => React$Node = (props) => {
    const dispatch = useDispatch();

    let [perm, setPerm] = React.useState(false);
    let [openLogInfo, setOpenLogInfo] = React.useState(false);
    let [openServerInfo, setOpenServerInfo] = React.useState(false);


    useEffect(() => {
        let listener = EventRegister.addEventListener(EventNames.CLOSE_ALL_MODAL, () => {
            setOpenLogInfo(false);
            setOpenServerInfo(false);
        });

        EventRegister.addEventListener(EventNames.BACKBUTTON, () => {
            setOpenLogInfo(false);
            setOpenServerInfo(false);
        });

        return () => {
            if (listener && typeof (listener.removeEventListener) === "function") {
                listener.removeEventListener(listener);
            }
        };
    }, []);

    const logout = () => {
        LoginHelper.logout();
        props.setMenuOpen(false);
    };

    const askPermission = (silence) => {
        if (typeof (silence) === "undefined") {
            silence = false;
        }

        let method = silence ? check : request;

        let permission = PERMISSIONS.IOS.CONTACTS;
        if (Platform.OS === "android") {
            permission = PERMISSIONS.ANDROID.READ_CONTACTS;
        }
        method(permission).then((result) => {
            if (silence === false) {
                openSettings().catch(() => console.warn("cannot open settings"));
            }
            if (result === "granted") {
                setPerm(true);
                return;
            }
            setPerm(false);
        });
    };


    function setContacts(contacts) {
        let contactsClean = [];
        contacts.map((contact) => {
            let c = {
                name: contact.givenName + " " + contact.familyName,
                numbers: [],
            };
            contact.phoneNumbers.map((phone) => {
                let temp = parsePhoneNumberFromString(phone.number, "FR");
                if (temp) {
                    c.numbers.push(temp.formatInternational());
                }
            });
            contactsClean.push(c);
        });
        dispatch(Data.addData("contact", contactsClean));
    }

    const getContact = () => {
        if (perm) {
            Contacts.getAll((err, contacts) => {
                if (err === "denied") {
                    setPerm(false);
                    return;
                }
                setContacts(contacts);
                setPerm(true);
            });
        }
    };


    useEffect(() => {
        getContact();
        askPermission(true);
    }, [props.open, perm]);


    const serverConfig = () => {
        setOpenServerInfo(true);
    };

    return (
        <Modal
            animationInTiming={1}
            animationOutTiming={1}
            visible={props.open}
            onRequestClose={() => {
                if (openLogInfo) {
                    setOpenLogInfo(false);
                    return;
                }
                if (openServerInfo) {
                    setOpenServerInfo(false);
                    return;
                }
                props.setMenuOpen(false);
            }}>

            {openLogInfo && (
                <ModalSendLog
                    setOpenLogInfo={setOpenLogInfo}
                />
            )}

            {openServerInfo && (
                <ModalServer
                    setOpenServerInfo={setOpenServerInfo}
                />
            )}

            <View style={styles.wrapper}>
                <View style={styles.topWrapper}>

                    <Text style={styles.textName}>Configuration</Text>

                    <TouchableOpacity
                        style={styles.closeWrapper}
                        onPress={() => {
                            props.setMenuOpen(false);
                        }}>
                        <FontAwesomeIcon icon={faTimes} color={Colors.greyDark} size={20}/>
                    </TouchableOpacity>
                </View>

                <View style={styles.middleWrapper}>

                    <View style={styles.wrapperItem}>
                        <View style={styles.itemContent}>
                            <View style={styles.itemContentText}>
                                <ButtonChildren style={styles.itemButton} onPress={() => {
                                    askPermission(false);
                                }}>
                                    <Text style={styles.itemText}>
                                        {perm && (
                                            "Supprimer l'accès aux contacts de mon téléphone"
                                        )}
                                        {!perm && (
                                            "Autoriser l'accès aux contacts de mon téléphone"
                                        )}

                                    </Text>
                                </ButtonChildren>
                            </View>
                            <View style={styles.itemIcon}>
                                {perm && (
                                    <FontAwesomeIcon icon={faCircleXmark} color={Colors.grey} size={20}/>
                                )}
                                {!perm && (
                                    <FontAwesomeIcon icon={faCircleCheck} color={Colors.grey} size={20}/>
                                )}
                            </View>
                        </View>
                    </View>

                    <View style={styles.wrapperItem}>
                        <View style={styles.itemContent}>
                            <View style={styles.itemContentText}>
                                <ButtonChildren style={styles.itemButton} onPress={serverConfig}>
                                    <Text style={styles.itemText}>Info du serveur XiVO</Text>
                                </ButtonChildren>
                            </View>
                            <View style={styles.itemIcon}>
                                <FontAwesomeIcon icon={faServer} color={Colors.grey} size={20}/>
                            </View>
                        </View>
                    </View>

                    <View style={styles.wrapperItem}>
                        <View style={styles.itemContent}>
                            <View style={styles.itemContentText}>
                                <ButtonChildren style={styles.itemButton} onPress={() => {
                                    setOpenLogInfo(true);
                                }}>
                                    <Text style={styles.itemText}>Envoyer les logs au support</Text>
                                </ButtonChildren>
                            </View>
                            <View style={styles.itemIcon}>
                                <FontAwesomeIcon icon={faSquareEnvelope} color={Colors.grey} size={20}/>
                            </View>
                        </View>

                    </View>

                </View>

                <View style={styles.bottomWrapper}>
                    <ButtonChildren style={styles.btnLogout} onPress={logout}>
                        <View style={styles.iconLogout}>
                            <FontAwesomeIcon icon={faPowerOff} color={Colors.orange} size={40}/>
                        </View>
                    </ButtonChildren>

                    <View style={styles.wrapperMentionAndVersion}>
                        <Text
                            style={styles.mentions}
                            onPress={() => {
                                Linking.openURL(Constants.LINK_MENTIONS);
                            }}
                        >Mentions légales</Text>
                        <Text
                            style={styles.version}
                        >
                            Version: {VersionCheck.getCurrentVersion() + " (" + VersionCheck.getCurrentBuildNumber() + ")"}</Text>
                    </View>

                </View>
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        ...ifIphoneX({
            paddingBottom: 25,
        }, {}),
    },
    topWrapper: {
        width: "100%",
        height: 125,
        justifyContent: "center",
        padding: 30,
        flexDirection: "row",
        paddingTop: Platform.OS === 'ios' ? 80 : 0
    },
    textName: {
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Bold",
        } : {}),
        fontSize: 16,
        letterSpacing: 0,
        color: Colors.greyDark,
        height: 25,
        position: "absolute",
        top: 65
    },
    closeWrapper: {
        position: "absolute",
        justifyContent: "center",
        alignItems: "center",
        top: Platform.OS === 'ios' ? 50 : 0,
        right: 0,
        width: 75,
        height: 75,
        zIndex: 999
    },
    middleWrapper: {
        flex: 1,
    },
    wrapperItem: {
        width: "100%",
        height: 57,
    },
    itemContent: {
        marginLeft: 25,
        marginRight: 25,
        flexDirection: "row",
        borderBottomColor: Colors.greyLight,
        borderBottomWidth: 1,
    },
    lastItemContent: {
        marginLeft: 25,
        marginRight: 25,
        flexDirection: "row",
    },
    itemContentText: {
        width: "85%",
    },
    itemButton: {
        position: "relative",
        height: 57,
        alignItems: "center",
        paddingLeft: 30,
        justifyContent: "center",
    },
    itemText: {
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Medium",
        } : {}),
        fontSize: 16,
        lineHeight: 18,
        color: Colors.grey,
        textAlign: "center",
    },
    itemIcon: {
        alignItems: "center",
        justifyContent: "center",
        width: "15%",
    },
    bottomWrapper: {
        width: "100%",
        height: 100,
        flexDirection: "column",
        justifyContent: "center",
    },
    btnLogout: {
        paddingTop: 10,
        justifyContent: "center",
    },
    iconLogout: {
        width: 50,
        height: 50,
        backgroundColor: Colors.white,
        borderRadius: 50,
        paddingBottom: 25,
        justifyContent: "center",
        alignItems: "center",
    },
    wrapperMentionAndVersion: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "flex-end",
    },
    version: {
        fontSize: 10,
        paddingRight: 15,
        paddingBottom: 10,
        textAlign: "right",
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Medium",
        } : {}),
    },
    mentions: {
        fontSize: 10,
        paddingLeft: 15,
        paddingBottom: 10,
        textAlign: "left",
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Medium",
        } : {}),
    },
});

export default Menu;
