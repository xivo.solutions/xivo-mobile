import React from "react";
import {
  Platform,
  StyleSheet, Text,
  View,
} from "react-native";

import Colors from "@assets/styles/colors";
import ButtonChildren from "@components/ButtonChildren";
import PageNames from "@configs/pages";
import { useSelector } from "react-redux";

import {
  faClock,
  faStar
} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import Svg, { Path } from 'react-native-svg';

import SocketsManager from "@helpers/SocketsManager";


const pagesButton = [
  {
    key: "favorites",
    icon: faStar,
    type: 'fa',
    text: "Favoris",
    pageName: PageNames.Favorites,
  },
  {
    key: "phone",
    type: 'svg',
    text: "Téléphone",
    pageName: PageNames.Phone,
  },
  {
    key: "histories",
    icon: faClock,
    type: 'fa',
    text: "Historique",
    pageName: PageNames.Histories,
  },
];

const Footer: () => React$Node = (props) => {
  const dataSessions = useSelector((state) => state.data.sessions.data);

  const getButton = (page, index, callback) => {
    if (typeof callback !== "function") {
      callback = () => {
        if (typeof page.pageName !== "undefined" && typeof PageNames[page.pageName]) {
          props.navigation.navigate(page.pageName, {
            random: Math.random(),
            search: false
          });
        }
      };
    }

    let notification = false;

    if (page.key === "histories") {
      notification = SocketsManager.cti.preference.missingCall;
    }
    if (page.key === "phone") {
      notification = dataSessions.length;
    }
    if (parseInt(notification) === 0) {
      notification = false;
    }

    if (notification > 9) {
      notification = "9+";
    }

    function DialpadIcon(props) {
      return (
        <Svg
          viewBox="0 0 25 30"      
          {...props}
        >   
            <Path d="M12 19c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zM6 1c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12-8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm-6 8c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm6 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0-6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-6 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0-6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z" />
        </Svg>
      )
    }

    let active = false;
    if (typeof props.page !== "undefined" && page.key === props.page) {
      active = true;
    }

    return (
      <ButtonChildren key={"btnPage-"+page+"-"+index} style={styles.wrapperBtn} onPress={callback}>
        {page.type == 'fa' ? (
          <FontAwesomeIcon icon={page.icon} color={active ? Colors.orange : Colors.grey} size={20} />
        ) : (
          <DialpadIcon width={25} height={25} fill={active ? Colors.orange : Colors.grey} />
        )}
        <Text style={[styles.btnText, active ? styles.btnTextActive : {}]}>{page.text}</Text>
        {notification && (
            <Text style={styles.notificationText}>{notification}</Text>
        )}
      </ButtonChildren>
    );
  };


  return (
      <View style={styles.footer}>
        <View style={styles.wrapperBtns}>
          {pagesButton.map(function(page, index) {
            return getButton(page, index);
          })}
        </View>
      </View>
  );

};

const styles = StyleSheet.create({
  footer: {
    paddingTop: 10,
    height: 70
  },
  wrapperBtns: {
    flexDirection: "row",
    justifyContent:"space-evenly",
    width: "100%",
  },
  wrapperBtn: {
    flexDirection: "column",
    height: 45
  },
  btnText: {
    ...(Platform.OS !== "ios" ? {
      fontFamily: "Gotham-Bold",
    } : {}),
    paddingTop:10,
    fontSize: 10,
    letterSpacing: 0,
    color: Colors.grey,
    textAlign: "center",
  },
  btnTextActive: {
    color: Colors.orange
  },
  notificationText: {
    ...(Platform.OS !== "ios" ? {
      fontFamily: "Gotham-Bold",
    } : {}),
    fontSize: 8,
    textAlign: "center",
    color: Colors.white,
    backgroundColor: Colors.red,
    opacity: 0.8,
    width: 12,
    height: 12,
    borderRadius: 12,
    position: "absolute",
    top: 0,
    right: 8,
  },
});

export default Footer;
