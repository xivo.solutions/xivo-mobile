import React, { useEffect } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Alert,
  Platform,
} from "react-native";
import Keychain from "react-native-keychain";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faSquareEnvelope } from "@fortawesome/free-solid-svg-icons";
import { FileLogger } from "react-native-file-logger";
import VersionCheck from "react-native-version-check";

import Colors from "@assets/styles/colors";
import inputsStyle from "@styles/inputs";
import Button from "@components/Button";
import LoginHelper from "@helpers/Login";


const regexDigit = new RegExp("^\\d+$");
const Popup: () => React$Node = (props) => {
  const {
    setOpenLogInfo,
  } = props;

  const [open, setOpen] = React.useState(true);
  const [ticketId, setTicketId] = React.useState(false);
  const [login, setLogin] = React.useState(false);
  const [server, setServer] = React.useState(false);


  const getCredentials = async () => {
    try {
      const credentials = await Keychain.getGenericPassword();
      if (credentials) {
        setLogin(credentials.username);
      }
    } catch (error) {
      console.log("[System] - Error with Keychain", error);
    }

    LoginHelper.getServer((u) => {
      setServer(u && u !== null ? u : "");
    });
  };

  useEffect(() => {
    getCredentials();
  }, []);

  useEffect(() => {
    setOpenLogInfo(open);
  }, [open]);


  const sendLogs = () => {
    FileLogger.sendLogFilesByEmail({
      to: "logs-mobile-support@xivo.solutions",
      subject: "[XIVO #" + ticketId + "]",
      body: "J'ai rencontré un problème sur l'application, vous trouverez les logs de l'application ci-joint \n" +
        "Voici les informations additionelles : \n" +
        "Identifiant : " + login + " \n" +
        "Url Serveur : " + server + "\n" +
        "Version App : " + VersionCheck.getCurrentVersion() + " (" + VersionCheck.getCurrentBuildNumber() + ")\n"
    });
  };

  const cancel = () => {
    setOpen(false);
    setTicketId(false);
  };

  const valid = () => {
    let temp = ticketId + "";
    if (temp.trim() === "" || ticketId === false) {
      Alert.alert(
        "Erreur",
        "Vous devez renseigner un ID de ticket (10 chiffres)",
        [
          { text: "OK" },
        ],
        { cancelable: false },
      );
      return;
    }

    if (
      temp.indexOf("20") !== 0 ||
      temp.length !== 10 ||
      regexDigit.test(temp) === false
    ) {
      Alert.alert(
        "Erreur",
        "Le numéro du ticket est incorrect",
        [
          { text: "OK" },
        ],
        { cancelable: false },
      );
      return;
    }

    sendLogs();
    cancel();
  };

  return (
    <View
      style={styles.modal}
    >
      <View style={styles.wrapper}>
        <View style={styles.iconHeader}>
          <FontAwesomeIcon icon={faSquareEnvelope} color={Colors.greyBlack} size={22} />
        </View>
        <Text style={styles.title}>Envoi de log au support</Text>

        <View style={styles.inputWrapper}>
          <Text style={styles.label}>Merci de nous fournir votre ID de ticket </Text>

          <View style={[inputsStyle.container, inputsStyle.server]}>
            <TextInput
              style={inputsStyle.input}
              autoCorrect={false}
              autoCompleteType={"off"}
              placeholder={"Saississez l'ID du ticket"}
              editable
              autoCapitalize={"none"}
              onChangeText={(text) => {
                setTicketId(text);
              }}
              onSubmitEditing={valid}
            />
          </View>

        </View>
        <View style={styles.wrapperBtns}>
          <Button style={styles.buttonCancel} onPress={cancel} text={"annuler"} color={"red"} />
          <Button style={styles.buttonValid} onPress={valid} text={"valider"} color={"orange"} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  modal: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: "#f6f6f6",
    flex: 1,
    zIndex: 99999999,
  },
  wrapper: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#f6f6f6",
    padding: 0,
    margin: 0,
    zIndex: 99999999,
    paddingTop: 100,
  },
  iconHeader: {
    width: 36,
    height: 36,
    borderRadius: 36,
    backgroundColor: Colors.white,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 15,
  },
  title: {
    ...(Platform.OS !== "ios" ? {
      fontFamily: "Gotham-Bold",
    } : {}),
    fontSize: 18.8,
    letterSpacing: 0.67,
    textAlign: "center",
    color: Colors.orange,
    marginBottom: 65,
  },
  inputWrapper: {
    width: "100%",
    alignItems: "center",
    marginBottom: 60,
  },
  label: {
    ...(Platform.OS !== "ios" ? {
      fontFamily: "Gotham",
    } : {}),
    fontSize: 12.5,
    letterSpacing: 1.04,
    textAlign: "left",
    color: Colors.greyDark,
    alignSelf: "flex-start",
    paddingLeft: "5%",
    marginBottom: 15,
  },
  wrapperBtns: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "center",
  },
  buttonCancel: {
    marginRight: 15,
    width: 120,
  },
  buttonValid: {
    width: 120,
  },
});

export default Popup;
