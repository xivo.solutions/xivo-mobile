import React from "react";
import {
    StyleSheet,
    View,
    Text,
    Dimensions, Platform,
} from "react-native";
import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import {faStar, faPhone, faSortDown, faSortUp, faUsers} from "@fortawesome/free-solid-svg-icons";
import Highlighter from "@sanar/react-native-highlight-text";
import Collapsible from "react-native-collapsible";

import Colors, {PHONE_STATUS_COLOR} from "@assets/styles/colors";

import ButtonChildren from "@components/ButtonChildren";
import Timer from "@components/Timer";
import SocketsManager from "@helpers/SocketsManager";
import String from "@helpers/String";
import Constants from "@configs/constants";
import {useSelector} from "react-redux";


const windowWidth = Dimensions.get("window").width;

const Row: () => React$Node = (props) => {
    const dataStatus = useSelector((state) => state.data.status.data);
    let entry = props.entry;
    let borderColor = Colors.grey;
    const [openDetail, setOpenDetail] = React.useState(false);
    const [closeDetail, setCloseDetail] = React.useState(false);

    if (typeof (dataStatus[entry.num]) !== "undefined" && PHONE_STATUS_COLOR[dataStatus[entry.num]]) {
        borderColor = PHONE_STATUS_COLOR[dataStatus[entry.num]];
    }
    /*
    if (typeof (entry.status) !== "undefined" && PHONE_STATUS_COLOR[entry.status]) {
        borderColor = PHONE_STATUS_COLOR[entry.status];
    }

    if (typeof (entry.statusUser) !== "undefined" && PHONE_STATUS_COLOR[entry.statusUser]) {
        borderColor = PHONE_STATUS_COLOR[entry.statusUser];
    }*/

    const updateFav = (evt) => {
        if (typeof entry.fav !== "undefined") {
            SocketsManager.cti[entry.fav ? "removeOnFavorite" : "addOnFavorite"](entry.contactId, entry.data.source);
        }
        evt.stopPropagation();
    };

    const openRow = () => {
        if (!openDetail) {
            setTimeout(() => {
                setCloseDetail(true);
            }, 1000);
        } else {
            setCloseDetail(false);
        }
        setOpenDetail(!openDetail);

    };

    let name = (<Text numberOfLines={2} style={styles.name}>{decodeURI(entry.name)}</Text>);
    if (props.search) {
        name = (
            <Highlighter
                numberOfLines={2}
                autoEscape={true}
                highlightStyle={styles.textHighlight}
                searchWords={[encodeURI(props.search)]}
                textToHighlight={entry.name}
            />
        );
    }

    let nums = [];
    if (entry.num) {
        nums.push({
            name: "Interne",
            num: entry.num,
        });
    }
    if (entry.mobile) {
        nums.push({
            name: "Mobile",
            num: entry.mobile,
        });
    }

    if (entry.extra) {
        nums.push({
            name: "Extra",
            num: entry.extra,
        });
    }

    const call = (numData) => {
        SocketsManager.sip.call(decodeURI(numData.num), ""/*decodeURI(entry.name) + '(' + numData.num + ')'*/);
    };

    let calcWidthName = (windowWidth - 50 - 39 - 35);
    if (nums.length !== 0) {
        calcWidthName -= 35;
        if (entry.hour) {
            calcWidthName -= 70;
        }
    }

    return (
        <>
            <View style={[styles.wrapper, openDetail ? {
                marginBottom: 0,
            } : (!closeDetail ? {marginBottom: 10} : {}),
            ]}>

                {entry.name && (
                    <ButtonChildren style={styles.fav} onPress={updateFav}>

                        <View style={styles.icon}>
                            <View style={[styles.avatar, {
                                borderColor: borderColor,
                                backgroundColor: borderColor + "5e",
                            }]}>

                                {entry.source !== Constants.TYPES_ITEM.CONF && (
                                    <Text style={styles.avatarText}>
                                        {String.getInitials(entry.name, 2)}
                                    </Text>
                                )}
                                {entry.source === Constants.TYPES_ITEM.CONF && (
                                    <FontAwesomeIcon
                                        icon={faUsers}
                                        color={Colors.black}
                                    />
                                )}


                            </View>
                            <View style={styles.iconFav}>
                                {typeof entry.fav !== "undefined" && entry.contactId && (
                                    <FontAwesomeIcon icon={faStar}
                                                     color={entry.fav ? Colors.orange : Colors.grey}/>
                                )}
                            </View>
                        </View>
                    </ButtonChildren>
                )}

                <ButtonChildren
                    style={styles.wrapperBtnRow}
                    onPress={nums.length > 1 ? openRow : () => {
                        if (nums.length === 1) {
                            call(nums[0]);
                        }
                    }}
                >
                    <View
                        style={[styles.name, {width: calcWidthName}]}>
                        {name}
                    </View>
                    {nums.length !== 0 && (
                        <View style={styles.action}>
                            {entry.hour && (
                                <View style={styles.time}>
                                    <Timer hour={entry.hour} type={entry.type}/>
                                </View>
                            )}

                            <View style={styles.wrapperIcon}>
                                {nums.length === 1 && (
                                    <FontAwesomeIcon
                                        icon={faPhone}
                                        color={Colors.green}
                                        size={15}
                                    />
                                )}

                                {nums.length > 1 && (
                                    <>
                                        {openDetail && (<FontAwesomeIcon icon={faSortUp} color={Colors.orange}/>)}
                                        {!openDetail && (<FontAwesomeIcon icon={faSortDown} color={Colors.orange}/>)}
                                    </>
                                )}
                            </View>

                        </View>
                    )}
                </ButtonChildren>

            </View>

            <Collapsible collapsed={!openDetail}>
                {nums.map((num, indexNum) => {
                    return (
                        <ButtonChildren
                            key={"Num" + indexNum}
                            style={styles.wrapperRowCollapse}
                            onPress={() => {
                                call(num);
                            }}>

                            <View style={styles.wrapperCollapse}>
                                <Text style={styles.textNameCollapse}>{num.name} - </Text>
                                <Text style={styles.textNumberCollapse}>{decodeURI(num.num)}</Text>
                            </View>
                            <View style={styles.wrapperIconCollapse}>
                                <View style={styles.wrapperIcon}>
                                    <FontAwesomeIcon
                                        icon={faPhone}
                                        color={Colors.green}
                                        size={15}
                                    />
                                </View>
                            </View>

                        </ButtonChildren>
                    );
                })}
            </Collapsible>

        </>
    );
};

const styles = StyleSheet.create({
    textHighlight: {
        color: Colors.orange,
    },
    wrapper: {
        display: "flex",
        height: 55,
        borderBottomWidth: 1,
        borderBottomStyle: "solid",
        borderBottomColor: Colors.whiteDark,
        marginLeft: 25,
        marginRight: 25,
        flexDirection: "row",
        justifyContent: "flex-start",
    },
    fav: {
        width: 40,
    },
    icon: {},
    avatar: {
        width: 39,
        height: 39,
        backgroundColor: Colors.blueLight,
        borderWidth: 2,
        borderRadius: 39,
        alignItems: "center",
        justifyContent: "center",
    },
    avatarText: {
        fontSize: 17,
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Medium",
        } : {}),
        textTransform: "uppercase",
        color: Colors.black,
    },
    iconFav: {
        position: "absolute",
        bottom: -2,
        right: -2,
    },
    wrapperBtnRow: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        marginLeft: 20,
    },
    action: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
    },
    name: {
        fontSize: 14,
        color: Colors.greyDark,
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Medium",
        } : {}),
    },
    time: {
        marginRight: 5,
        marginBottom: 3,
        minWidth: 40,
        justifyContent: "center",
        alignItems: "center",
    },
    wrapperIcon: {
        backgroundColor: Colors.white,
        alignItems: "center",
        justifyContent: "center",
        width: 35,
        height: 35,
        borderRadius: 35,
    },
    wrapperRowCollapse: {
        backgroundColor: Colors.whiteDark,
        borderBottomWidth: 1,
        borderBottomStyle: "solid",
        borderBottomColor: Colors.greyLight,
        height: 55,
        flexDirection: "row",
        justifyContent: "flex-start",
    },
    wrapperCollapse: {
        flex: 1,
        marginLeft: 25,
        flexDirection: "row",
        alignItems: "flex-start",
    },
    textNameCollapse: {
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Bold",
        } : {}),
    },
    textNumberCollapse: {
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Light",
        } : {}),
    },
    wrapperIconCollapse: {
        marginRight: 25,
    },

});

export default Row;
