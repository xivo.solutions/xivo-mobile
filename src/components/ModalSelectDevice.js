import React, { useEffect } from "react";
import { EventRegister } from "react-native-event-listeners";

import {
  StyleSheet,
  View,
  Text,
  Modal,
  KeyboardAvoidingView,
  Platform,
  Switch, TextInput, Appearance, Dimensions, Alert,
} from "react-native";

import Button from "@components/Button";
import ButtonChildren from "@components/ButtonChildren";

import Constants from "@configs/constants"

import Colors from "@assets/styles/colors";
import Images from "@assets/images";
import EventNames from "@configs/events";

import inputsStyle from "@styles/inputs";

import SocketsManager from "@helpers/SocketsManager";

const Popup: () => React$Node = (props) => {

  const [selectedDevices, setSelectedDevices] = React.useState({
    mobile: (SocketsManager.cti.preference.device === Constants.ROUTING.webAppApp || SocketsManager.cti.preference.device === Constants.ROUTING.app),
    webRTC: (SocketsManager.cti.preference.device === Constants.ROUTING.webAppApp || SocketsManager.cti.preference.device === Constants.ROUTING.webApp),
  });

  const [activeNoPerturbe, setActiveNoPerturbe] = React.useState(false);
  const [redirectAllCall, setRedirectAllCall] = React.useState(false);
  const [numberRedirect, setNumberRedirect] = React.useState("");
  const [redirectCallNoResponse, setRedirectCallNoResponse] = React.useState(false);
  const [numberNoResponse, setNumberNoResponse] = React.useState("");

  useEffect(() => {
    let listener = EventRegister.addEventListener(EventNames.CLOSE_ALL_MODAL, (data) => {
      props.setOpen(false);
    });

    return () => {

      if (listener && typeof (listener.removeEventListener) === "function") {
        listener.removeEventListener(listener);
      }
    };
  }, [props.open]);

  const closeModal = () => {
    props.setOpen(false);
  };

  useEffect(() => {
    let pref = false;
    if (selectedDevices.mobile && selectedDevices.webRTC) {
      pref = Constants.ROUTING.webAppApp;
    } else if (selectedDevices.mobile) {
      pref = Constants.ROUTING.app;
    } else if (selectedDevices.webRTC) {
      pref = Constants.ROUTING.webApp;
    }
    SocketsManager.cti.changePreferenceDevice(pref);
  }, [selectedDevices]);

  useEffect(() => {
    setActiveNoPerturbe(SocketsManager.cti.user.dndEnabled);
    setRedirectAllCall(SocketsManager.cti.user.uncFwdEnabled);
    setNumberRedirect(SocketsManager.cti.user.uncFwdDestination);

    setRedirectCallNoResponse(SocketsManager.cti.user.naFwdEnabled);
    setNumberNoResponse(SocketsManager.cti.user.naFwdDestination);

  }, []);

  return (
    <View>
      <Modal visible={props.open} onRequestClose={() => {
        closeModal();
      }}>
        <KeyboardAvoidingView
          style={styles.wrapper}
          behavior={Platform.OS === "ios" ? "padding" : "height"}
          enabled={Platform.OS === "ios"}
          keyboardVerticalOffset={100}
        >
          <View style={styles.wrapperText}>
            <Text style={styles.title}>Gestion des appels</Text>
            <Text style={styles.description}>Choix des périphériques de réceptions d'appels</Text>
          </View>

          <View style={styles.wrapperDevices}>
            <ButtonChildren
              style={styles.btnWebRTC}
              onPress={() => {
                let temp = { ...selectedDevices };
                temp.webRTC = !temp.webRTC;
                if (!temp.mobile) {
                  temp.mobile = true;
                }
                setSelectedDevices(temp);
              }}
            >
              <Images.LogoWebRTC style={styles.imageLogo} width={80}
                                 fill={!selectedDevices.webRTC ? Colors.greyDark : Colors.orange} />

              <View
                style={[styles.legendBtnDevices, styles.legendBtnWebRTC, selectedDevices.webRTC ? styles.onDeviceActive : {}]}>
                <Text
                  style={[styles.textBtnDevices, selectedDevices.webRTC ? styles.onDeviceTextActive : {}]}>NAVIGATEUR</Text>
              </View>

            </ButtonChildren>
            <ButtonChildren
              style={styles.btnMobile}
              onPress={() => {
                let temp = { ...selectedDevices };
                temp.mobile = !temp.mobile;
                if (!temp.webRTC) {
                  temp.webRTC = true;
                }
                setSelectedDevices(temp);
              }}
            >
              <Images.LogoMobile style={styles.imageLogo} width={80}
                                 fill={!selectedDevices.mobile ? Colors.greyDark : Colors.orange} />

              <View
                style={[styles.legendBtnDevices, styles.legendBtnMobile, selectedDevices.mobile ? styles.onDeviceActive : {}]}>
                <Text style={[styles.textBtnDevices, selectedDevices.mobile ? styles.onDeviceTextActive : {}]}>APPLI
                  MOBILE</Text>
              </View>

            </ButtonChildren>
          </View>

          <View style={[styles.wrapperBlock, styles.wrapperBlockActive]}>
            <Text>Activer mode "ne pas déranger"</Text>
            <View style={styles.toogle}>
              <Switch
                trackColor={{ false: "#767577", true: Colors.orange + "50" }}
                thumbColor={activeNoPerturbe ? Colors.orange : "#f4f3f4"}
                onValueChange={() => {
                  SocketsManager.cti.changeDistrub(!activeNoPerturbe);
                  setActiveNoPerturbe(!activeNoPerturbe);
                }}
                value={activeNoPerturbe}
              />
            </View>
          </View>

          <View style={styles.wrapperBlockDouble}>
            <View style={[styles.wrapperBlockDoubleSub, styles.wrapperBlockDoubleSubToogle]}>
              <Text>Renvoyer tous les appels </Text>
              <View style={styles.toogle}>
                <Switch
                  trackColor={{ false: "#767577", true: Colors.orange + "50" }}
                  thumbColor={redirectAllCall ? Colors.orange : "#f4f3f4"}
                  onValueChange={() => {
                    if (numberRedirect.trim() === "") {
                      Alert.alert(
                        "Erreur",
                        "Vous devez renseigner un numéro de téléphone",
                        [
                          { text: "OK" },
                        ],
                        { cancelable: false },
                      );
                      return;
                    }

                    setRedirectCallNoResponse(false);
                    setRedirectAllCall(!redirectAllCall);
                    SocketsManager.cti.changeTransfert(!redirectAllCall, numberRedirect);
                  }}
                  value={redirectAllCall}
                />
              </View>
            </View>
            <View style={[styles.wrapperBlockDoubleSub, styles.wrapperBlockDoubleSubInput]}>
              <Text>Destination </Text>
              <View style={{
                ...inputsStyle.container,
                marginLeft: 20,
              }}>
                <TextInput
                  style={{
                    ...inputsStyle.input,
                    width: 190,
                  }}
                  returnKeyType="done"
                  keyboardType={Platform.os !== "android" ? "number-pad" : "phone-pad"}
                  autoCorrect={false}
                  placeholder={"Numéro de destination"}
                  placeholderTextColor={Appearance.getColorScheme() === "dark" ? Colors.greyMedium : Colors.grey}
                  value={numberRedirect}
                  onChangeText={(text) => {
                    setNumberRedirect(text);
                  }}
                  onSubmitEditing={() => {
                    SocketsManager.cti.changeTransfert(redirectAllCall, numberRedirect, true);
                  }}
                />
              </View>
            </View>
          </View>

          <View style={styles.wrapperBlockDouble}>
            <View style={[styles.wrapperBlockDoubleSub, styles.wrapperBlockDoubleSubToogle]}>
              <Text>Renvoyer les appels sur non reponse</Text>
              <View style={styles.toogle}>
                <Switch
                  trackColor={{ false: "#767577", true: Colors.orange + "50" }}
                  thumbColor={redirectCallNoResponse ? Colors.orange : "#f4f3f4"}
                  onValueChange={() => {
                    if (numberNoResponse.trim() === "") {
                      Alert.alert(
                        "Erreur",
                        "Vous devez renseigner un numéro de téléphone",
                        [
                          { text: "OK" },
                        ],
                        { cancelable: false },
                      );
                      return;
                    }
                    setRedirectAllCall(false);
                    setRedirectCallNoResponse(!redirectCallNoResponse);
                    SocketsManager.cti.changeTransfertNoResponse(!redirectCallNoResponse, numberNoResponse);

                  }}
                  value={redirectCallNoResponse}
                />
              </View>
            </View>
            <View style={[styles.wrapperBlockDoubleSub, styles.wrapperBlockDoubleSubInput]}>
              <Text>Destination </Text>
              <View style={{
                ...inputsStyle.container,
                marginLeft: 20,
              }}>
                <TextInput
                  style={{
                    ...inputsStyle.input,
                    width: 190,
                  }}
                  returnKeyType="done"
                  keyboardType={Platform.os !== "android" ? "number-pad" : "phone-pad"}
                  autoCorrect={false}
                  placeholder={"Numéro de destination"}
                  placeholderTextColor={Appearance.getColorScheme() === "dark" ? Colors.greyMedium : Colors.grey}
                  value={numberNoResponse}
                  onChangeText={(text) => {
                    setNumberNoResponse(text);
                  }}
                  onSubmitEditing={() => {
                    SocketsManager.cti.changeTransfertNoResponse(redirectCallNoResponse, numberNoResponse, true);
                  }}

                />
              </View>
            </View>
          </View>

          <View style={styles.wrapperButton}>
            <Button style={styles.buttonClose} onPress={closeModal} text={"Fermer"} color={"grey"} />
          </View>
        </KeyboardAvoidingView>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapperBlockDoubleSubInput: {
    justifyContent: "flex-end",
  },
  wrapperBlockDoubleSubToogle: {
    marginBottom: 10,
  },
  wrapperBlockActive: {
    borderWidth: 1,
    borderColor: Colors.greyMedium,
  },
  wrapperBlockDoubleSub: {
    flexDirection: "row",
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: "space-between",
    alignItems: "center",
    width: Dimensions.get("window").width,
  },
  wrapperBlockDouble: {
    paddingTop: 15,
    paddingBottom: 15,
  },
  toogle: {},
  wrapperBlock: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 15,
    paddingBottom: 15,

  },
  wrapper: {
    //"#f6f6f6", "#e1e1e1"
    backgroundColor: "#f6f6f6",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  wrapperText: {
    width: "100%",
    marginBottom: 20,
  },
  title: {
    ...(Platform.OS !== "ios" ? {
      fontFamily: "Gotham-Bold",
    } : {}),
    fontSize: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: Colors.greyDark,
  },
  description: {
    fontFamily: "Gotham",
    fontSize: 15,
    letterSpacing: 0,
    textAlign: "center",
    marginTop: 25,
    color: Colors.greyDark,
  },
  wrapperDevices: {
    display: "flex",
    flexDirection: "row",
    width: "90%",
    height: 150,
    marginBottom: 20,
  },
  imageLogo: {
    marginBottom: 10,
  },
  onDeviceActive: {
    backgroundColor: Colors.orange,
  },
  onDeviceTextActive: {
    color: Colors.white,
  },
  legendBtnDevices: {
    backgroundColor: Colors.greyMedium,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: 40,
  },
  textBtnDevices: {
    ...(Platform.OS !== "ios" ? {
      fontFamily: "Gotham-Bold",
    } : {}),
    fontSize: 14,
    letterSpacing: 0,
    alignItems: "center",
    justifyContent: "center",
    color: Colors.greyDark,
  },
  btnWebRTC: {
    display: "flex",
  },
  legendBtnWebRTC: {
    borderTopLeftRadius: 25,
    borderBottomLeftRadius: 25,
  },
  btnMobile: {
    display: "flex",
  },
  legendBtnMobile: {
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
  },
  wrapperButton: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "center",
  },
  buttonClose: {
    width: 120,
  },
});

export default Popup;
