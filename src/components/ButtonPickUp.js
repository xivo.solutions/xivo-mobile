import React from 'react';
import {StyleSheet, View} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faPhoneAlt} from '@fortawesome/free-solid-svg-icons';

import Colors from '@assets/styles/colors';
import Button from '@components/Button';

const ButtonPickUp: () => React$Node = (props) => {
  let style = {};
  if (props.style) {
    style = props.style;
  }
  return (
    <Button

      onPress={props.onPress}
      icon={'phone'}
      color={'green'}
      style={{...styles.btn, ...style}}
      children={(
        <View style={styles.wrapperIconsButton}>
          <FontAwesomeIcon
            icon={faPhoneAlt}
            color={Colors.white}
            size={25}
          />
        </View>
      )}
    />
  );
};


const styles = StyleSheet.create({
  btn: {
    width: 150,
    alignItems: 'center',
    justifyContent: 'center'
  },
  wrapperIconsButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
});

export default ButtonPickUp;
