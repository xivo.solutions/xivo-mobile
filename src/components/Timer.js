import React from "react";
import {
	Platform,
	StyleSheet,
	Text,
	View,
} from "react-native";

import Colors from "@assets/styles/colors";
import Images from "@assets/images";

const Timer: () => React$Node = (props) => {
	let timeFormatted = "";

	let image = (null);
	if (props.hour) {
		timeFormatted = props.hour;
		if (props.type) {
			if (props.type === "ongoing" || props.type === "answered") {
				image = (
					<Images.CallIncoming
						width={30}
						height={30}
					/>
				);
			}

			if (props.type === "missed") {
				image = (
					<Images.CallMissing
						width={30}
						height={30}
					/>
				);
			}

			if (props.type === "emitted") {
				image = (
					<Images.CallOutgoing
						width={30}
						height={30}
					/>
				);
			}
		}
	}
	return (
		<View style={styles.wrapper}>
			{image}
			<Text style={styles.time}>{timeFormatted}</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	wrapper: {
		flexDirection: "row",
		alignItems: "center",
		paddingRight: 15
	},
	time: {...(Platform.OS !== "ios" ? {
			fontFamily: "Gotham",
		} : {}),
		fontSize: 11,
		letterSpacing: 0,
		color: Colors.greyDark
	},
});

export default Timer;
