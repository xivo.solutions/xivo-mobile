import React from "react";
import {
  StyleSheet,
  View,
  Text, Platform,
} from "react-native";
import Row from "@components/Row";
import Colors from "@assets/styles/colors";

const List: () => React$Node = (props) => {
  const search = props.search ? props.search.trimEnd() : "";

  return (
    <View style={styles.wrapperRows}>
      {props.entries.length === 0 ? (
        <Text style={styles.noResults}>{props.noResults ? props.noResults : "Aucun résultat"}</Text>
      ) : (null)}
      {props.entries.map((entry, index) => {
        if (entry.typeRow && entry.typeRow === "header") {
          return (
            <View style={styles.wrapperHeader} key={"header" + index}>
              <Text style={styles.textHeader}>{entry.title}</Text>
            </View>
          );
        }
        return (
          <Row
            key={"row" + index}
            entry={entry}
            route={props.route ? props.route : false}
            search={search}
            lastRow={index === props.entries.length - 1}
          />
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  wrapperRows: {
    width: "100%",
  },
  noResults: {
    ...(Platform.OS !== "ios" ? {
      fontFamily: "Gotham-Medium",
    } : {}),
    fontSize: 15,
    letterSpacing: 0,
    textAlign: "center",
    color: Colors.greyDark,
    paddingTop: 20,
  },
  wrapperHeader: {
    height: 25,
    justifyContent: "center",
    alignItems: "flex-start",
    marginLeft: 25,
    marginRight: 25,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomStyle: "solid",
    borderBottomColor: Colors.grey,
  },
  textHeader: {
    ...(Platform.OS !== "ios" ? {
      fontFamily: "Gotham",
    } : {}),
    fontSize: 14,
    letterSpacing: 0,
    color: Colors.grey,
    textAlign: "center",
  },
});
export default List;
