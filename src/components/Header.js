import React, {useEffect} from "react";
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    Appearance,
    Alert,
    Dimensions,
    Platform,
    TouchableOpacity,
} from "react-native";

import {EventRegister} from "react-native-event-listeners";

import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import {faMagnifyingGlass, faXmark, faPhone} from "@fortawesome/free-solid-svg-icons";
import {parsePhoneNumberFromString} from "libphonenumber-js";

import Images from "@assets/images";
import Colors from "@assets/styles/colors";

import {useSelector} from "react-redux";

import ButtonChildren from "@components/ButtonChildren";
import Menu from "@components/Menu";

import SocketsManager from "@helpers/SocketsManager";
import StringHelper from "@helpers/String";

import PhoneConfig from "@configs/phone";
import Pages from "@configs/pages";
import EventNames from "@configs/events";

import ModalSelectDevice from "@components/ModalSelectDevice";
import ModalAlertRoutage from "@components/ModalAlertRoutage";

const windowWidth = Dimensions.get("window").width;

const regexDigit = new RegExp("^\\d+$");
const Header: () => React$Node = (props) => {

    let [search, setSearch] = React.useState("");
    let [menuOpen, setMenuOpen] = React.useState(false);
    let [openModalSelectDevice, setOpenModalSelectDevice] = React.useState(false);
    let [openedModalRouting, setOpenModalRouting] = React.useState(false);


    const voiceMailData = useSelector((state) => state.data.voiceMail);

    const callVoiceMail = () => {
        SocketsManager.sip.call(PhoneConfig.VOICEMAIL_NUMBER);
    };

    const openMenu = () => {
        setMenuOpen(true);
    };

    const closeMenu = () => {
        setMenuOpen(false);
    };

    const openModalRouting = () => {
        setOpenModalRouting(true);
    };

    const closeModalRouting = () => {
        setOpenModalRouting(false);
    };
    const closeModals = () => {
        setOpenModalRouting(false);
        setOpenModalSelectDevice(false);
    }

    const openSelectDevice = () => {
        setOpenModalSelectDevice(true);
    };


    useEffect(() => {

        let listeners = [
            EventRegister.addEventListener(EventNames.OPENMENU, openMenu),
            EventRegister.addEventListener(EventNames.CLOSEMENU, closeMenu),
            EventRegister.addEventListener(EventNames.CLOSE_ALL_MODAL, closeModals),
            EventRegister.addEventListener(EventNames.OPEN_ALERT_ROUTING, openModalRouting)
        ];

        return () => {
            listeners.map((listener) => {
                if (listener && typeof (listener.removeEventListener) === "function") {
                    listener.removeEventListener(listener);
                }
            })
        };
    }, []);

    useEffect(() => {
        setSearch(props.search !== false ? props.search : "");
    }, [props.search]);

    let name = "";
    if (SocketsManager.cti.user) {
        name = SocketsManager.cti.user.fullName;
    }

    let imageDevice = (null);
    let marginDevice = 0;

    if (SocketsManager.cti.user.dndEnabled) {
        marginDevice = 5;
        imageDevice = (<Images.Dnd width={35} fill={Colors.greyDark}/>);
    } else {
        if (SocketsManager.cti.preference.device === "WebAppAndMobileApp") {
            marginDevice = -5;
            imageDevice = (<Images.WebrtcMobile width={35} fill={Colors.greyDark}/>);
            if (SocketsManager.cti.user.naFwdEnabled) {
                imageDevice = (<Images.WebrtcMobileForwardNa width={35} fill={Colors.greyDark}/>);
            } else if (SocketsManager.cti.user.uncFwdEnabled) {
                imageDevice = (<Images.WebrtcMobileForward width={35} fill={Colors.greyDark}/>);
            }
        } else if (SocketsManager.cti.preference.device === "MobileApp") {
            marginDevice = 5;
            imageDevice = (<Images.Mobile width={35} fill={Colors.greyDark}/>);
            if (SocketsManager.cti.user.naFwdEnabled) {
                imageDevice = (<Images.MobileForwardNa width={35} fill={Colors.greyDark}/>);
            } else if (SocketsManager.cti.user.uncFwdEnabled) {
                imageDevice = (<Images.MobileForward width={35} fill={Colors.greyDark}/>);
            }
        } else if (SocketsManager.cti.preference.device === "WebApp") {
            marginDevice = 5;
            imageDevice = (<Images.WebRTC width={35} fill={Colors.greyDark}/>);
            if (SocketsManager.cti.user.naFwdEnabled) {
                imageDevice = (<Images.WebRTCForwardNa width={35} fill={Colors.greyDark}/>);
            } else if (SocketsManager.cti.user.uncFwdEnabled) {
                imageDevice = (<Images.WebRTCForward width={35} fill={Colors.greyDark}/>);
            }
        }

    }


    const isNumber = (num) => {
        return num &&
            parsePhoneNumberFromString(num, "FR") &&
            regexDigit.test(num);
    };

    const launchSearch = () => {
        if (isNumber(search)) {
            SocketsManager.sip.call(search);
            return;
        }

        if (search.length < 3) {
            Alert.alert("Recherche", "Vous devez saisir au minimum 3 caractères", [{text: "OK"}], {cancelable: true});
            return;
        }
        setSearch(""); //TODO(Bug on padding)
        props.navigation.navigate(Pages.Search, {
            search,
        });
    };

    let callNumber = false;
    if (isNumber(search)) {
        callNumber = true;
    }


    return (
        <View style={styles.header}>
            <Menu
                open={menuOpen}
                setMenuOpen={setMenuOpen}
                navigation={props.navigation}
            />

            {openedModalRouting && (
                <ModalAlertRoutage
                    open={openedModalRouting}
                    setOpen={setOpenModalRouting}
                />
            )}

            {openModalSelectDevice && (
                <ModalSelectDevice
                    open={openModalSelectDevice}
                    setOpen={setOpenModalSelectDevice}
                />
            )}

            <View style={SocketsManager.sip.status.register ? styles.avatarConnected : styles.avatarDisconnected}>
                <Text style={styles.avatarText}>{StringHelper.getInitials(name, 2)}</Text>

                <View style={styles.avatarCall}>
                    <ButtonChildren style={styles.voiceMail} onPress={callVoiceMail}>
                        <Images.VoiceMail
                            width={25}
                            height={10}
                        />
                        {voiceMailData.notificationCount > 0 && (<View style={styles.numberVoiceMail}>
                            <Text style={styles.textNumberVoiceMail}>{voiceMailData.notificationCount}</Text>
                        </View>)}
                    </ButtonChildren>
                </View>

            </View>

            <View style={styles.infos}>
                <View style={styles.infoTop}>
                    <View>
                        <Text numberOfLines={1}
                              style={[styles.name, {width: windowWidth - 80 - 14 - 25 - 90}]}>{name}</Text>
                        {SocketsManager.sip.configs && SocketsManager.sip.configs.number && (
                            <Text style={styles.number}>{SocketsManager.sip.configs.number}</Text>
                        )}
                    </View>
                    <View
                        style={styles.topButtons}
                    >
                        <ButtonChildren
                            style={{
                                ...styles.selectDevice,
                                marginTop: marginDevice,
                            }}
                            onPress={openSelectDevice}>
                            {imageDevice}
                        </ButtonChildren>
                        <ButtonChildren
                            style={styles.menuIcon}
                            onPress={openMenu}
                            noFull={true}
                        >
                            <View style={[styles.barMenu, styles.barMenu1]}></View>
                            <View style={[styles.barMenu, styles.barMenu2]}></View>
                            <View style={[styles.barMenu, styles.barMenu3]}></View>
                        </ButtonChildren>
                    </View>
                </View>
                <View style={styles.infoBottom}>
                    <View style={styles.wrapperSearch}>
                        <TextInput
                            style={styles.inputSearch}
                            autoCorrect={false}
                            value={search}
                            onChangeText={(text) => {
                                setSearch(text);
                            }}
                            placeholder="Rechercher un nom"
                            placeholderTextColor={Appearance.getColorScheme() === "dark" ? Colors.greyMedium : Colors.grey}
                            returnKeyType="search"
                            onSubmitEditing={launchSearch}
                        />

                        {search.length > 0 && (
                            <TouchableOpacity onPress={() => {
                                setSearch("");
                            }} style={styles.removeInput}>
                                <FontAwesomeIcon
                                    icon={faXmark}
                                    color={Colors.greyDark}
                                    size={15}
                                />
                            </TouchableOpacity>
                        )}

                        <TouchableOpacity
                            onPress={launchSearch}
                            style={styles.actionInput}>
                            {!callNumber && (
                                <FontAwesomeIcon
                                    icon={faMagnifyingGlass}
                                    color={Colors.greyDark}
                                    size={19}
                                />
                            )}
                            {callNumber && (
                                <FontAwesomeIcon
                                    icon={faPhone}
                                    color={Colors.greyDark}
                                    size={19}
                                />
                            )}
                        </TouchableOpacity>
                    </View>

                </View>
            </View>

        </View>);

};

const styles = StyleSheet.create({
    topButtons: {
        flexDirection: "row",
        width: 90,
        height: 65,
        justifyContent: "space-between",
    },
    actionInput: {
        width: 25,
        alignItems: "center",
        justifyContent: "center",
    },
    removeInput: {
        marginRight: 10,
        marginLeft: 10,
        width: 15,
        alignItems: "center",
        justifyContent: "center",
    },
    wrapperSearch: {
        height: 40,
        backgroundColor: Colors.white,
        flexDirection: "row",
        alignItems: "center",
        borderRadius: 25,
        paddingLeft: 15,
        paddingRight: 15,
    },
    inputSearch: {
        flex: 1,
    },
    btnLoupe: {
        width: 20,
    },
    header: {
        margin: 25,
        marginRight: 0,
        marginTop: 0,
        flexDirection: "row",
        backgroundColor: Colors.greyLight,
    },
    avatarDisconnected: {
        width: 80,
        height: 80,
        backgroundColor: Colors.grey,
        borderRadius: 80,
        marginRight: 14,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 25,
    },
    avatarConnected: {
        width: 80,
        height: 80,
        backgroundColor: Colors.green,
        borderRadius: 80,
        marginRight: 14,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 25,
    },
    avatarText: {
        color: Colors.white,
        fontSize: 25,
        textTransform: "uppercase",
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Medium",
        } : {}),
    },
    avatarCall: {
        backgroundColor: Colors.white,
        position: "absolute",
        borderRadius: 15,
        bottom: 0,
        right: 0,
    },
    voiceMail: {
        width: 30.5,
        height: 20,
    },
    numberVoiceMail: {
        width: 14,
        height: 14,
        backgroundColor: Colors.orange,
        borderRadius: 15,
        position: "absolute",
        right: 0,
        bottom: -5,
    },
    textNumberVoiceMail: {
        color: Colors.white,
        textAlign: "center",
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham",
        } : {}),
        fontSize: 8,
        letterSpacing: 0,
        lineHeight: 15,
    },
    infos: {
        flex: 2,
        width: "100%",
        paddingRight: 20,
    },
    infoTop: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    name: {
        fontSize: 20,
        color: Colors.black,
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Bold",
        } : {}),
        marginTop: 25,
    },
    btnClear: {
        padding: 10,
        position: "absolute",
        end: -3,
    },
    selectDevice: {
        marginLeft: 5,
    },
    menuIcon: {
        width: 50,
        paddingLeft: 10,
        height: 65,
        alignItems: "flex-start",
    },
    barMenu: {
        backgroundColor: Colors.greyDark,
        height: 3,
        width: 21,
        borderRadius: 3,
        marginTop: 6,
    },
    barMenu1: {},
    barMenu2: {
        width: 15,
    },
    barMenu3: {},
    infoBottom: {
        marginTop: 0,
        width: "100%",
    },
    number: {
        fontSize: 12,
        marginBottom: 5
    }
});

export default Header;
