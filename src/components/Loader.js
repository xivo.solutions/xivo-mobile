import React from "react";
import {
	View,
	StyleSheet,
} from "react-native";
import {BarIndicator} from "react-native-indicators";

import Colors from "@assets/styles/colors";

const Loader: () => React$Node = (props) => {
	return (
		<View style={styles.wrapper}>
			<BarIndicator color={Colors.orange} />
		</View>
	);
};

const styles = StyleSheet.create({
	wrapper: {
		alignItems: "center",
		justifyContent: "center",
		flex: 1,
	},
});

export default Loader;
