import React, {useEffect} from "react";
import {EventRegister} from "react-native-event-listeners";
import {
    StyleSheet,
    View,
    Text, Platform,
} from "react-native";
import Modal from "react-native-modal";

import EventNames from "@configs/events";
import Button from "@components/Button";
import Colors from "@assets/styles/colors";
import ButtonChildren from "./ButtonChildren";

const Popup: () => React$Node = (props) => {
    const [open, setOpen] = React.useState(false);
    const [dtmf, setDTMF] = React.useState("");

    useEffect(() => {
        let listener = EventRegister.addEventListener(EventNames.OPEN_MODAL_DTMF, (data) => {
            setOpen(true);
        });

        let listener2 = EventRegister.addEventListener(EventNames.CLOSE_ALL_MODAL, (data) => {
            setOpen(false);
        });

        return () => {
            if (listener && typeof (listener.removeEventListener) === "function") {
                listener.removeEventListener(listener);
            }
            if (listener2 && typeof (listener2.removeEventListener) === "function") {
                listener2.removeEventListener(listener2);
            }
        };
    }, []);

    const cancel = () => {
        setDTMF("");
        setOpen(false);
    };

    const getButton = (number) => {
        let styleWrapper = {};
        styleWrapper.marginTop = 28;
        styleWrapper.marginBottom = 28;
        return (
            <ButtonChildren style={styleWrapper} onPress={() => {
                setDTMF(dtmf + "" + number);
                if (props.sendDTMF) {
                    props.sendDTMF(number);
                }
            }}>
                <Text style={styles.textDTMF}>{number}</Text>
            </ButtonChildren>
        );
    };

    return (
        <View>
            <Modal
                isVisible={open}
                backdropColor={"white"}
                backdropOpacity={0.98}
                style={{
                    padding: 0,
                    margin: 0,
                }}
            >
                <View style={styles.wrapper}>
                    <View style={styles.wrapperInput}>
                        <Text style={styles.textDTMF}>{dtmf}</Text>
                    </View>
                    <View style={styles.wrapperNumbers}>
                        <View style={styles.lineNumbers}>
                            {getButton("1")}
                            {getButton("2")}
                            {getButton("3")}
                        </View>
                        <View style={styles.lineNumbers}>
                            {getButton("4")}
                            {getButton("5")}
                            {getButton("6")}
                        </View>
                        <View style={styles.lineNumbers}>
                            {getButton("7")}
                            {getButton("8")}
                            {getButton("9")}
                        </View>
                        <View style={styles.lineNumbers}>
                            {getButton("*")}
                            {getButton("0")}
                            {getButton("#")}
                        </View>
                    </View>
                    <View style={styles.wrapperCancel}>
                        <Button style={styles.buttons} onPress={cancel} text={"Fermer"} color={"red"}/>
                    </View>
                </View>
            </Modal>
        </View>
    );
};

const styles = StyleSheet.create({
    textDTMF: {
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Book",
        } : {}),
        fontSize: 25,
        letterSpacing: 0,
        textAlign: "center",
        color: Colors.greyBlack,
    },
    lineNumbers: {
        flexDirection: "row",
    },
    wrapperInput: {
        width: "100%",
        borderTopWidth: 1,
        borderTopColor: Colors.grey,
        borderBottomWidth: 1,
        borderBottomColor: Colors.grey,
        backgroundColor: Colors.greyLight,
        paddingTop: 9,
        paddingBottom: 9,
        marginBottom: 28,
        minHeight: 50
    },
    wrapperNumbers: {
        width: "100%",
        alignItems: "center",
        marginBottom: 28,
    },
    wrapperCancel: {
        width: "100%",
        alignItems: "center",
    },
    wrapper: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
});

export default Popup;
