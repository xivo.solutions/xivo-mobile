import React, { useCallback, useEffect, useState } from "react";
import {
  View,
  StyleSheet,
  Text, Platform,
} from "react-native";
import moment from "moment";

import Colors from "@assets/styles/colors";
import ButtonChildren from "@components/ButtonChildren";
import SessionSipManager from "@helpers/SessionSipManager";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import {
  faPhone,
  faLevelUpAlt,
  faPlay,
} from "@fortawesome/free-solid-svg-icons";

let interval = false;

const PhoneMin: () => React$Node = (props) => {

  const [currentTime, setCurrentTime] = useState(0);
  if (typeof (props.session) === "undefined") {
    return (null);
  }

  let session = props.session.item;
  let event = props.session.event;


  let timeFormatted = moment("1988-03-18")
    .startOf("day")
    .seconds(currentTime)
    .format("mm:ss");

  const hangUp = () => {
    SessionSipManager.removeSession(session.idInterne);
  };

  const pickUp = () => {
    if (session !== false) {
      SessionSipManager.switchSession(props.session.item.idInterne);
      try {
        SessionSipManager.anwser(props.session.item.idInterne);
      } catch (e) {
        console.error("[Webrtc] - Error while answering call ", e);
      }
    }
  };

  const play = () => {
    SessionSipManager.switchSession(props.session.item.idInterne);
  };

  const transfer = () => {
    SessionSipManager.transfert();
  };

  let name = false;
  let num = false;
  let direction = false;
  let displayHangUpIn = false;

  if (
    event && (
      event.eventType === "EventDialing" ||
      event.eventType === "EventRinging"
    )
  ) {
    name = event.otherDName.trim();
    num = event.otherDN.trim();    
    if (event.callDirection === "Incoming") {      
      direction = "APPEL ENTRANT";
      displayHangUpIn = true;
    } else {
      direction = "APPEL SORTANT";
      displayHangUpIn = false;
    }
  }
  let isHold = session.isOnHold().local;

  const updateCurrenTime = useCallback(() => {
    setCurrentTime(parseInt(currentTime, 10) + 1);
  }, [currentTime]);

  useEffect(() => {
    if (isHold) {
      if (interval !== false) {
        clearInterval(interval);
      }
      interval = setInterval(updateCurrenTime, 1000);
    }

    if (!isHold) {
      if (interval !== false) {
        clearInterval(interval);
        interval = false;
      }
    }

  }, [isHold, updateCurrenTime]);

  useEffect(() => {
    return () => {
      if (interval !== false) {
        clearInterval(interval);
        interval = false;
      }
    };
  }, []);

  return (
    <View style={styles.wrapper}>
      <View style={styles.infos} key={"info"}>
        {isHold && (<Text key={"detail"} style={styles.textStatusAndNum}>APPEL En Attente- {timeFormatted}</Text>)}
        {(!isHold && direction) && (<Text key={"direction"} style={styles.textStatusAndNum}>{direction}</Text>)}
        {name !== false && (<Text key={"name"} style={styles.textName}>{name}</Text>)}
        {num !== false && (<Text key={"numm"} style={styles.textStatusAndNum}>{num}</Text>)}
      </View>
      <View style={styles.actions} key={"action"}>

      {displayHangUpIn && (
          <ButtonChildren style={styles.wrapperBtnAction} onPress={hangUp}>
            <FontAwesomeIcon icon={faPhone} color={Colors.red} size={20} style={styles.btnHangUpIcon} />
          </ButtonChildren>
        )}
        {displayHangUpIn && (
          <ButtonChildren style={styles.wrapperBtnAction} onPress={pickUp}>
            <FontAwesomeIcon icon={faPhone} color={Colors.green} size={20} />
          </ButtonChildren>
        )}

        {props.transfer && (
          <ButtonChildren style={styles.wrapperBtnAction} onPress={transfer}>
            <FontAwesomeIcon icon={faLevelUpAlt} color={Colors.green} size={20} />
          </ButtonChildren>
        )}

        {isHold && (
          <ButtonChildren style={styles.wrapperBtnAction} onPress={play}>
            <FontAwesomeIcon icon={faPlay} color={Colors.greyDark} size={20} />
          </ButtonChildren>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    justifyContent: "center",
    height: 75,
    backgroundColor: Colors.whiteDark,
    borderTopColor: Colors.grey,
    borderBottomColor: Colors.grey,
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
  infos: {
    flex: 1,
    justifyContent: "center",
    paddingLeft: 16,
  },
  textStatusAndNum: {
    ...(Platform.OS !== "ios" ? {
      fontFamily: "Gotham",
    } : {}),
    fontSize: 12.5,
    letterSpacing: 0,
    color: Colors.greyBlack,
  },
  textName: {
    ...(Platform.OS !== "ios" ? {
      fontFamily: "Gotham-Bold",
    } : {}),
    fontSize: 15,
    lineHeight: 15,
    letterSpacing: 0,
    color: Colors.orange,
    marginTop: 2,
    marginBottom: 2,
  },
  actions: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingRight: 16,
  },
  wrapperBtnAction: {
    padding: 25,
  },
  btnHangUpIcon: {
    transform: [{
      rotate: "135deg",
    }],
  },
});

export default PhoneMin;
