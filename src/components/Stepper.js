import React, { useState } from "react";
import { View, Text, TouchableOpacity, ScrollView, StyleSheet } from "react-native";

import Colors from "@styles/colors";

const search = (keyName, myArray) => {
  let value = false;
  myArray.map((val) => {
    if (val === keyName) {
      value = true;
    }
  });
  return value;
};
const Stepper = (props) => {
  const {
    active,
    content,
    onNext,
    onFinish,
    showButton = true,
  } = props;

  const [step, setStep] = useState([0]);

  const pushData = (val) => {
    setStep((prev) => [...prev, val]);
  };

  return (React.createElement(View, { style: styles.wrapperMain },
    React.createElement(View, {
      style: styles.wrapperStep,
    }, content.slice(0, -1).map((_, i) => {
      return (React.createElement(React.Fragment, { key: i },
        i !== 0 && (React.createElement(View, {
          style: styles.tick,
        })),
        React.createElement(View, {
          style: [
            styles.circle,
            {
              opacity: search(i, step) ? 1 : 0.3,
            },
          ],
        }, search(i, step) ? (React.createElement(Text, {
          style: styles.text,
        }, "\u2713")) : (React.createElement(Text, {
          style: styles.text,
        }, i + 1)))));
    })),
    React.createElement(ScrollView, { showsVerticalScrollIndicator: false }, content[active]),
    showButton && (React.createElement(View, {
        style: styles.wrapper,
      },
      content.length - 1 !== active && (React.createElement(TouchableOpacity, {
          style: styles.btn, onPress: () => {
            pushData(active + 1);
            onNext();
          },
        },
        React.createElement(Text, { style: styles.btnTxt }, "Suivant"))),
      content.length - 1 === active && (React.createElement(TouchableOpacity, {
          style: styles.btn, onPress: () => onFinish(),
        },
        React.createElement(Text, { style: styles.btnTxt }, "Terminer")))))));
};


const styles = StyleSheet.create({

  wrapperMain: {
    height: "100%",
  },
  circle: {
    backgroundColor: Colors.orange,
    width: 30,
    height: 30,
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    color: Colors.white,
  },
  tick: {
    flex: 1,
    height: 1,
    backgroundColor: Colors.grey,
    opacity: 1,
    marginHorizontal: 10,
  },
  wrapperStep: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  wrapper: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  btn: {
    padding: 15,
    borderRadius: 4,
    backgroundColor: Colors.orange,
  },
  btnTxt: {
    color: "white",
    fontSize: 20,
    textTransform: "uppercase"
  },
});

export default Stepper;
