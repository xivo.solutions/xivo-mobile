import React from "react";
import {
	TouchableOpacity,
	StyleSheet,
} from "react-native";

const ButtonChildren: () => React$Node = (props) => {
	let s = [styles.wrapper];
	if (props.style) {
		s.push(props.style);
		if (
			typeof (props.style.width) === "undefined" &&
			typeof (props.style.height) === "undefined" &&
			typeof (props.noFull) === "undefined"
		) {
			s.push(styles.flexible);
		}
	} else {
		s.push(styles.flexible);
	}
	return (
		<TouchableOpacity onPress={props.onPress} style={s}>
			{props.children}
		</TouchableOpacity>
	);
};


const styles = StyleSheet.create({
	flexible: {
		flex: 1,
	},
	wrapper: {
		justifyContent: "center",
		alignItems: "center",
	},
});

export default ButtonChildren;
