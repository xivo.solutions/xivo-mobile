import React, {useEffect, useState} from "react";

import {
    StyleSheet,
    View,
    Text,
    Modal,
    Platform,
    SafeAreaView,
} from "react-native";

import Button from "@components/Button";

import SocketsManager from "@helpers/SocketsManager";

import Constants from "@configs/constants";
import Colors from "@assets/styles/colors";

let timer = null;
let interval = null;

const maxDisplay = 5;
const oneSecond = 1000;

const Popup: () => React$Node = (props) => {
    const {
        open,
        setOpen = () => {
        }
    } = props;
    const [time, setTime] = useState(maxDisplay);

    const closeModal = () => {
        setOpen(false);
        clearTimeout(timer);
        clearInterval(interval);
    }

    useEffect(() => {
        if (open) {
            setTime(maxDisplay);

            timer = setTimeout(() => {
                accept();
            }, maxDisplay * oneSecond)

            let localTimer = maxDisplay;
            interval = setInterval(() => {
                localTimer--;
                if (localTimer < 0) {
                    localTimer = 0;
                }
                setTime(localTimer);
            }, oneSecond)
        }
    }, [open])

    const accept = () => {
        closeModal();
        SocketsManager.cti.changePreferenceDevice(Constants.ROUTING.webAppApp);
    }

    const cancel = () => {
        closeModal();
    }

    return (
        <Modal
            style={styles.modal}
            visible={open}
            onRequestClose={() => {
                closeModal();
            }}
            transparent={true}
            animationType='slide'
        >
            <SafeAreaView style={styles.wrapper}>
                <View style={styles.wrapper}>
                    <View style={styles.wrapperText}>
                        <Text style={styles.title}>Gestion des appels</Text>
                        <Text style={styles.description}>Vous allez désormais recevoir vos appels sur les applications
                            web et mobile.{"\n"}
                            Cliquez sur
                            Annuler si vous ne souhaitez pas changer le routage actuel.</Text>
                    </View>

                    <View style={styles.wrapperButton}>
                        <Button style={styles.button} onPress={cancel} text={"Annuler"} color={"grey"}/>
                        <Button style={styles.button} onPress={accept} text={"Accepter (" + time + ")"} color={"orange"}/>
                    </View>
                </View>
            </SafeAreaView>
        </Modal>
    );
};

const styles = StyleSheet.create({
    modal: {
        margin: 0, // This is the important style you need to set
        alignItems: undefined,
        justifyContent: undefined,
        padding: 0
    },
    wrapperSafe: {
        flex: 1,
        justifyContent: "center",
        width: "100%"
    },        
    wrapper: {
        position: "absolute",
        bottom: 0,
        backgroundColor: 'white',
        height: 185,
        width: "100%",
        alignItems: "center",
        borderTopWidth: 1,
        borderTopColor: Colors.orange,
        borderStyle: "solid"
    },
    wrapperText: {
        paddingTop: 5,
        width: "80%",
    },
    title: {
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Bold",
        } : {}),
        fontSize: 20,
        letterSpacing: 0,
        textAlign: "center",
        color: Colors.greyDark,
    },
    description: {
        fontSize: 14,
        letterSpacing: 0,
        textAlign: "center",
        color: Colors.greyDark,
        marginTop: 5
    },
    wrapperButton: {
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-around",
        marginTop: 10,
    },
    button: {
        width: 120,
    },
});

export default Popup;
