import React, { useEffect } from "react";
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  View,
  Text,
  ScrollView, Platform,
} from "react-native";
import { EventRegister } from "react-native-event-listeners";

import Button from "@components/Button";
import Images from "@assets/images";
import Colors from "@styles/colors";
import Header from "@components/Header";
import Footer from "@components/Footer";
import PageNames from "@configs/pages";

import EventNames from "@configs/events";

const Wrapper: () => React$Node = (props) => {
  let [error, setError] = React.useState(false);

  let header = true;
  if (typeof props.header !== "undefined") {
    header = props.header;
  }

  let footer = true;
  if (typeof props.footer !== "undefined") {
    footer = props.footer;
  }

  let search = false;
  if (typeof props.search !== "undefined") {
    search = props.search;
  }
  useEffect(() => {
    if (typeof props.error !== "undefined") {
      setError(props.error);
    }
  }, [props]);


  useEffect(() => {
    EventRegister.addEventListener(EventNames.GO_LOGIN, (data = {}) => {
      props.navigation.navigate(PageNames.Login, {
        random: Math.random(),
        ...data,
      });
    });

    EventRegister.addEventListener(EventNames.BACKBUTTON, () => {
      let routes = props.navigation.getState().routes;
      if (routes && routes.length > 2) {
        if (routes[routes.length - 2].name !== "Login") {
          props.navigation.navigate(routes[routes.length - 2].name, {
            random: Math.random(),
          });
        }
      }
    });

    EventRegister.addEventListener(EventNames.DISPLAY_PHONE, () => {
      props.navigation.navigate(PageNames.Phone);
    });

    return () => {
      EventRegister.removeEventListener(EventNames.DISPLAY_PHONE);
      EventRegister.removeEventListener(EventNames.BACKBUTTON);
      EventRegister.removeEventListener(EventNames.GO_LOGIN);
    };
  }, []);

  return (

      <View style={styles.background}>
        <StatusBar barStyle="dark-content" />

        {error !== false && (
          <View
            style={styles.wrapperError}
          >
            <Images.LogoHome
              width={240}
              height={167}
              style={styles.logo}
            />
            <Text style={styles.text}>
              {error}
            </Text>
            <Button
              onPress={() => {
                if (typeof props.setError === "function") {
                  props.setError(false);
                }
              }}
              text={"Annuler"}
              color={"red"}
            />
          </View>
        )}

        <SafeAreaView style={styles.wrapper}>
          {header && (<Header navigation={props.navigation} page={props.page} search={search} />)}
          <View style={styles.wrapper}>
            <View style={styles.inner}>
              <ScrollView style={styles.scrollView} contentContainerStyle={styles.scrollViewContainer}>
                {props.children}
              </ScrollView>
            </View>
          </View>
          {footer && (<Footer page={props.page} navigation={props.navigation} />)}
        </SafeAreaView>
      </View>
  );
};

const styles = StyleSheet.create({
  background: {
    backgroundColor: Colors.greyLight,
    flex: 1,
  },
  wrapperError: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: Colors.greyLight,
    zIndex: 99,
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    paddingTop: 100,
    paddingBottom: 100,
  },
  wrapper: {
    flex: 1,
    flexDirection: "column",
    width: "100%",
    height: "100%",
  },
  logo: {
    marginBottom: 20,
  },
  text: {
    ...(Platform.OS !== "ios" ? {
      fontFamily: "Gotham-Medium",
    } : {}),
    lineHeight: 20,
    color: Colors.greyDark,
    fontSize: 16,
    textAlign: "center",
    marginLeft: 30,
    marginRight: 30,
  },
  inner: {
    height: "100%",
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  scrollView: {
    width: "100%",
  },
  scrollViewContainer: {
    width: "100%",
    flexGrow: 1,
    justifyContent: "space-between",
  },
});

export default Wrapper;
