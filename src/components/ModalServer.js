import React, { useEffect } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Platform,
} from "react-native";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import LinearGradient from "react-native-linear-gradient";

import Colors from "@assets/styles/colors";
import inputsStyle from "@styles/inputs";
import Button from "@components/Button";

import LoginHelper from "@helpers/Login";
import SocketsManager from "@helpers/SocketsManager";

const Popup: () => React$Node = (props) => {
  const {
    setOpenServerInfo,
  } = props;

  const [open, setOpen] = React.useState(true);
  const [server, setServer] = React.useState("");
  const [serverICE, setServerICE] = React.useState("");

  const updateLocalVar = () => {
    LoginHelper.getServer((s) => {
      setServer(s);
    });
    if (SocketsManager.sip && SocketsManager.sip.iceConfig){      
      setServerICE(SocketsManager.sip.iceConfig[0].urls[0]);
    }
  };

  useEffect(() => {
    updateLocalVar();
    setOpenServerInfo(open);
  }, [open]);


  const valid = () => {
    setOpen(false);
  };

  return (
    <View
      style={styles.modal}
    >
      <LinearGradient colors={["#f6f6f6", "#e1e1e1"]} style={styles.wrapper}>
        <View style={styles.iconHeader}>
          <FontAwesomeIcon icon={faCog} color={Colors.greyBlack} size={22} />
        </View>
        <Text style={styles.title}>Informations XiVO</Text>

        <View style={styles.inputWrapper}>
          <Text style={styles.label}>Serveur d'authentification</Text>

          <View style={[inputsStyle.container, inputsStyle.server]}>
            <TextInput
              style={inputsStyle.input}
              key={"server"}
              autoCorrect={false}
              autoCompleteType={"off"}
              editable={false}
              defaultValue={server}
              autoCapitalize={"none"}
            />
          </View>

        </View>

        <View style={styles.inputWrapper}>
          <Text style={styles.label}>Configuration TURN</Text>

          <View style={[inputsStyle.container, inputsStyle.server]}>
            <TextInput
              style={inputsStyle.input}
              key={"serverICE"}
              autoCorrect={false}
              autoCompleteType={"off"}
              editable={false}
              defaultValue={serverICE}
              autoCapitalize={"none"}
            />
          </View>

        </View>
        <View style={styles.wrapperBtns}>
          <Button style={styles.buttonValid} onPress={valid} text={"retour"} color={"grey"} />
        </View>
      </LinearGradient>
    </View>
  );
};

const styles = StyleSheet.create({
  modal: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: "#f6f6f6",
    flex: 1,
    zIndex: 99999999,
  },
  wrapper: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#f6f6f6",
    padding: 0,
    margin: 0,
    zIndex: 99999999,
    paddingTop: 100,
  },
  iconHeader: {
    width: 36,
    height: 36,
    borderRadius: 36,
    backgroundColor: Colors.white,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 15,
  },
  title: {
    ...(Platform.OS !== "ios" ? {
      fontFamily: "Gotham-Bold",
    } : {}),
    fontSize: 18.8,
    letterSpacing: 0.67,
    textAlign: "center",
    color: Colors.orange,
    marginBottom: 65,
  },
  inputWrapper: {
    width: "100%",
    alignItems: "center",
    marginBottom: 60,
  },
  label: {
    ...(Platform.OS !== "ios" ? {
      fontFamily: "Gotham",
    } : {}),
    fontSize: 12.5,
    letterSpacing: 1.04,
    textAlign: "left",
    color: Colors.greyDark,
    alignSelf: "flex-start",
    paddingLeft: "5%",
    marginBottom: 15,
  },
  wrapperBtns: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "center",
  },
  buttonCancel: {
    marginRight: 15,
    width: 120,
  },
  buttonValid: {
    width: 120,
  },
});

export default Popup;
