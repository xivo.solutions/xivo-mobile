import {
    Dimensions,    
} from "react-native";

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;

export default {
    // Responsive Height Function
    RPH: (percentage) => {
        return (percentage / 100) * screenHeight; 
    },
    // Responsive Width Function 
    RPW: (percentage) => {
        return (percentage / 100) * screenWidth;
    },
    getH: () => screenHeight,
    getW: () => screenWidth
};
