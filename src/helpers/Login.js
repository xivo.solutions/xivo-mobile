import AsyncStorage from "@react-native-async-storage/async-storage";
import * as Keychain from "react-native-keychain";
import moment from "moment";
import {Alert, Platform} from "react-native";
import {EventRegister} from "react-native-event-listeners";
let ReactNativeForegroundService;
if (Platform.OS === 'android') {
  ReactNativeForegroundService = require('rn-foreground-service').default;
} else {
  ReactNativeForegroundService = null;
}

import Request from "@helpers/Request";
import URLs from "@configs/urls";
import Constants from "@configs/constants";

const formatDateTime = "YYYY-MM-DD HH:mm:ss";
import SocketsManager from "@helpers/SocketsManager";
import EventNames from "@configs/events";
import SessionSipManager from "./SessionSipManager";

const logout = () => {
    console.log("[Login] - logout");
    if (Platform.OS !== "ios") {
        console.log("[System] - Clean foreground service");
        ReactNativeForegroundService.stop();
    }
    SocketsManager.cti.changePreferenceDevice("WebApp");
    removeStoredToken();
    removePassword();
    removePushTokenSent();
    removeIceConfig();
    removeLineConfig();

    setTimeout(() => {
        SocketsManager.cti.token = false;
        SocketsManager.cti.preference.device = false
        SocketsManager.cti.reset();

        SocketsManager.sip.configs = null;
        SocketsManager.sip.token = false;
        SocketsManager.sip.reset();

        EventRegister.emit(EventNames.GO_LOGIN, {clear: true});
    }, 500);
};


const setTransfert = async (number, callback) => {
    if (typeof callback !== "function") {
        callback = () => {

        };
    }
    try {
        await AsyncStorage.setItem(Constants.STORAGE_NUMBER_TRANSFERT, number);
        callback();
    } catch (error) {
        console.error(error);
        Alert.alert(
            "Erreur",
            "Une erreur est survenue lors du stockage des informtaions sur le mobile.",
            [
                {text: "OK"},
            ],
            {cancelable: false},
        );
    }
};

const getTransfert = async (callback = () => {
}) => {
    try {
        const number = await AsyncStorage.getItem(Constants.STORAGE_NUMBER_TRANSFERT);
        callback(number);
    } catch (error) {
        callback("");
    }
};


const saveStoredToken = async (login, token, ttl) => {
    console.log("[Login] - Storing new token ", token, "for user ", login, ttl);
    try {
        await AsyncStorage.setItem(Constants.KEY_STORAGE + ":token", token);
        await AsyncStorage.setItem(Constants.KEY_STORAGE + ":login", login);
        await AsyncStorage.setItem(Constants.KEY_STORAGE + ":ttl", "" + ttl);
        await AsyncStorage.setItem(Constants.KEY_STORAGE + ":date", "" + moment().format(formatDateTime));
    } catch (error) {
        console.error("[Login] - Error while storing login info ", error);
    }
};

const removeStoredToken = async () => {
    try {
        await AsyncStorage.removeItem(Constants.KEY_STORAGE + ":token");
        await AsyncStorage.removeItem(Constants.KEY_STORAGE + ":login");
        await AsyncStorage.removeItem(Constants.KEY_STORAGE + ":ttl");
        await AsyncStorage.removeItem(Constants.KEY_STORAGE + ":date");
    } catch (error) {
        return false;
    }
};

const getStoredToken = async () => {
    try {
        const token = await AsyncStorage.getItem(Constants.KEY_STORAGE + ":token");
        const login = await AsyncStorage.getItem(Constants.KEY_STORAGE + ":login");
        const ttl = await AsyncStorage.getItem(Constants.KEY_STORAGE + ":ttl");
        const date = await AsyncStorage.getItem(Constants.KEY_STORAGE + ":date");
        return {
            token,
            login,
            ttl: parseInt(ttl, 10),
            date,
        };
    } catch (error) {
        return false;
    }
};

const savePassword = async (login, password) => {
    await Keychain.setGenericPassword(login, password);
};

const removePassword = async () => {
    await Keychain.resetGenericPassword();
};

const getPassword = async () => {
    await Keychain.getGenericPassword();
};

const removePushTokenSent = async () => {
    try {
        await AsyncStorage.removeItem(Constants.STORAGE_SERVER_PUSH_TOKEN_SENT);
    } catch (error) {
        return false;
    }
};

const removeIceConfig = async () => {
    try {
        await AsyncStorage.removeItem(Constants.STORAGE_ICE_CONFIG);
        await AsyncStorage.removeItem(Constants.STORAGE_ICE_CONFIG_TIME);
    } catch (error) {
        return false;
    }
};


const removeLineConfig = async () => {
    try {
        await AsyncStorage.removeItem(Constants.STORAGE_LINECONFIG);
    } catch (error) {
        return false;
    }
};

const getLastLogin = async (type, callback) => {
    try {
        let propsName = type === "cti" ? "STORAGE_LOGIN_CTI" : "STORAGE_LOGIN_SIP";
        let lastlogin = await AsyncStorage.getItem(Constants[propsName]);
        callback(lastlogin);
    } catch (error) {
        console.error("[Login] - Error while getting last connection time to websockets",error);
        callback(false);
    }
};

const setLastLogin = async (type, value) => {
    try {
        let propsName = type === "cti" ? "STORAGE_LOGIN_CTI" : "STORAGE_LOGIN_SIP";
        AsyncStorage.setItem(Constants[propsName], value);
    } catch (error) {
        console.error("[Login] - Error while setting last connection time to websockets ", error);
    }
};

const getServer = async (callback) => {
    try {
        const server = await AsyncStorage.getItem(Constants.STORAGE_SERVER_URL);
        callback(server);
    } catch (error) {
        console.error("[Login] - Error on get server ", error);
        callback(null);
    }
};

const setServer = async (server, callback) => {
    try {
        await AsyncStorage.setItem(Constants.STORAGE_SERVER_URL, server);
        callback();
    } catch (error) {
        console.error("[Login] - Error while storing server url", error);
        Alert.alert(
            "Erreur",
            "Une erreur est survenue lors du stockage des informations sur le mobile.",
            [
                {text: "OK"},
            ],
            {cancelable: false},
        );
    }
};


const signIn = (login, password, callback = () => {}) => {
    console.log("[Login] - Start SignIn request for ", login);
    getServer((u) => {
        let url = URLs.URL_API_REST + "auth/login";
        if (u !== "" && u !== null) {
            url = "https://" + u + URLs.END_API + "auth/login";
        }
        console.info("[Login] - Using XUC url to ask token ", url)
        Request(
            {
                login: login,
                password: password,
                softwareType: "mobile",
            },
            url,
            (error, data) => {
                if (error) {
                    console.error("[Login] - Error occured while contacting XUC ", error)
                    if (error && error.toLowerCase() === "invalid credentials") {
                        error = "Utilisateur non reconnu ou mauvais mot de passe";
                        removePassword();
                    }

                    if (error && error.toLowerCase().indexOf("mobile push token") !== -1) {
                        error = "Votre infrastructure n'a pas les pré-requis pour l'utilisation de l'application mobile.";
                        removePassword();
                    }
                    return callback(true, error);
                }

                saveStoredToken(login, data.token, data.TTL);
                savePassword(login, password);        

                SessionSipManager.init()
                return callback(false, {
                    ...data,
                });
            },
        );
    });
};

const getValidToken = (credentials, callback) => {

    const preCallBack = (token, type) => {
        if (typeof callback === "function") {
            callback(token, type);
        }
    };

    const handleSignIn = (login, password) => {
        signIn(login, password, (error, data) => {
            if (error) {
                console.error("[Login] - Error while renewing token ");
                preCallBack(false);
                return;
            }
            preCallBack(data.token, "new");
        });
    };

    getStoredToken().then((storedData) => {
        if (storedData.login) {
            console.debug("[Login] - Retrieve and check validity of stored token for ", storedData.login, storedData.token, "ttl ", storedData.ttl+"s");
            let now = moment();
            let dateToken = moment(storedData.date);
            let maxTokenDate = dateToken.add(storedData.ttl, "seconds").subtract(storedData.ttl/2, "seconds");
            console.debug("[Login] - Stored token for ", storedData.login, " will expire at ", maxTokenDate.format(Constants.DATE_FORMAT));

            if (maxTokenDate.isBefore(now)) {
                console.log("[Login] - local XUC token is too old, force signIn to get a new one");
                if (!credentials) {                
                    Keychain.getGenericPassword().then(c => {
                        handleSignIn(storedData.login, c.password);
                    })
                } else {
                    handleSignIn(storedData.login, credentials.password);
                }           
            } else {
                preCallBack(storedData.token, "old");
            }
        } else {
            console.log("[Login] - No stored token found, probably because user is not logged in yet")
        }    
    }).catch((error) => {
        console.error("[Login] - Error", error);
        preCallBack(false);
    });
};

export default {
    saveStoredToken,
    removeStoredToken,
    getStoredToken,
    savePassword,
    removePassword,
    getPassword,
    signIn,
    getServer,
    setServer,
    getValidToken,
    logout,
    getTransfert,
    setTransfert,
    getLastLogin,
    setLastLogin
};
