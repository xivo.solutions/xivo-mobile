import Sound from "react-native-sound";

function playHangUp() {
  return;
}

function playDTMF() {
  return;
}

function stopCall() {
  if (loopCall !== false) {
    loopCall.stop();
    loopCall = false;
  }
}

let loopCall = false;

function playCall() {
  console.log("[Audio] - play ring back tone for outgoing Call ");
  loopCall = new Sound("a1614_3s.mp3", Sound.MAIN_BUNDLE, (error) => {
    if (error) {
      console.error("[Audio] - failed to load the sound", error);
      return;
    }
    console.log("[Audio] ring tone - duration in seconds: " + loopCall.getDuration() + "number of channels: " + loopCall.getNumberOfChannels());


    loopCall.setVolume(1);
    loopCall.setNumberOfLoops(-1);

    loopCall.play((success) => {
      if (success) {
        console.log("[Audio] - successfully finished playing ring tone");
      } else {
        console.error("[Audio] - playback failed due to audio decoding errors");
      }
    });
  });
}

export default {
  playHangUp,
  playCall,
  playDTMF,
  stopCall,
};
