import axios from 'axios';

export default function ajax(object, dstUrl, callback, headers, method) {
	let params = {url: dstUrl};

	let requestMethod = method;
	if (typeof requestMethod === 'undefined') {
		axios.defaults.headers.common['Content-Type'] = 'application/json';
		requestMethod = 'post';
	}
	params.method = requestMethod;

	if (typeof headers !== 'undefined') {
		params.headers = headers;
	}

	if (typeof object !== 'undefined') {
		params.data = object;
	}

	axios(params).then((response) => {
		if (response.status === 200 || response.status === 201) {
			return callback(false, response.data);
		}
		console.log("[Request] - Received response",response.data);
		callback('Erreur dans la réponse au serveur', false);
	})
		.catch((error) => {
			if (error.response && error.response.data && error.response.data.message) {
				return callback(error.response.data.message, false);
			}
			console.error('[Request] - Error occured ', error);
			callback("Impossible de contacter le serveur XiVO, vérifiez vos paramètres", false);
		});
}
