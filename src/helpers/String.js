export default {
    getInitials: (name, length=null) => {
        if (name) {
            return name.split(" ").map((n, index) => {
                if(length === null){
                    return n[0]
                } else {
                    if(index > length - 1){
                        return ""
                    } else {
                        return n[0]
                    }
                }
            }).join("");

        }
        return "";
    },
};
