import messaging from "@react-native-firebase/messaging";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {Platform} from "react-native";

import PushNotification from "react-native-push-notification";
import VoipPushNotification from 'react-native-voip-push-notification';

let ReactNativeForegroundService;
if (Platform.OS === 'android') {
  ReactNativeForegroundService = require('rn-foreground-service').default;
} else {
  ReactNativeForegroundService = null;
}

import {Notifications} from "react-native-notifications";
import moment from "moment";

import Request from "@helpers/Request";
import SocketsManager from "@helpers/SocketsManager";
import URLs from "@configs/urls";
import Constants from "@configs/constants";


const channelId = "caller-notification";
let NotificationManager = {
    token: false,
    tokenOldIos: false,
    sendLocalPush: (name) => {
        console.log("[Notification] - Send local notification for ", name);

        if (Platform.OS !== "ios") {
            try {
                PushNotification.localNotification({
                    channelId,
                    autoCancel: true,
                    bigText: "Un appel manqué de " + name,
                    title: "Appel manqué",
                    data: {},
                    message: "Un appel manqué de " + name,
                    vibrate: true,
                    vibration: 300,
                    playSound: true,
                    soundName: "default",
                });
            } catch (error) {
                console.error("[Notification] - Local notification error", error);
            }

            return;
        }
        Notifications.postLocalNotification({
            body: "Un appel manqué de " + name,
            title: "Appel manqué",
            silent: false,
            extra: "data",
            category: "Gestion d'appel",
            userInfo: {},
        });
    },
    init: () => {
        console.log("[Notification] - NotificationManager:init");

        PushNotification.setApplicationIconBadgeNumber(0);
        PushNotification.createChannel(
            {
                channelId,
                channelName: "Notifications d'appel XiVO",
            }
            ,
            (created) => console.log(`[Notification] - createChannel returned '${created}'`),
        );


        messaging().requestPermission()
            .then(authorizationStatus => {
                console.log("[Notification] - Ask for firebase permissions", authorizationStatus);
            });


        messaging().getToken()
            .then(token => {
                console.log("[Notification] - Received Android Push token", token);
                if (Platform.OS !== "ios") {
                    NotificationManager.token = token;
                } else {
                    NotificationManager.tokenOldIos = token;
                }
            });

        messaging().onTokenRefresh(token => {
            console.log("[Notification] - Android Push token refresh", token);
            if (Platform.OS !== "ios") {
                NotificationManager.token = token;
            } else {
                NotificationManager.tokenOldIos = token;
            }
        });

        messaging().onMessage(async localMessage => {
            console.log("[Notification] - Received local notification", localMessage);
            NotificationManager.keepAwake(localMessage);
        });


        messaging().setBackgroundMessageHandler(async remoteMessage => {
            console.log("[Notification] - Received remote notification", remoteMessage);
            NotificationManager.keepAwake(remoteMessage);
        });

        if (Platform.OS === "ios") {
            VoipPushNotification.addEventListener('register', (token) => {
                console.log("[Notification] - Received iOS voip push notification token", token);
                NotificationManager.token = token;
            });

            VoipPushNotification.addEventListener('notification', (notification) => {
                console.log("[Notification] - Received iOS VOIP notification", notification);
                NotificationManager.keepAwake('VoipPushNotification');
            });

            VoipPushNotification.registerVoipToken();

        }

    },
    keepAwake: async () => {

        SocketsManager.setBlockCheckSocket(false);
        if (Platform.OS !== "ios") {
            if (!ReactNativeForegroundService.is_running()) {
                console.log("[System] - Start android foreground service to wake up websockets");
                ReactNativeForegroundService.start({
                    id: 18031988,
                    title: "Xivo service",
                    message: "Démarrage du service en tâche de fond",
                    ServiceType: "phoneCall"
                });
            }
        }


        if (SocketsManager.sip && SocketsManager.sip && typeof SocketsManager.sip.sip.unregister === "function") {
            SocketsManager.sip.sip.unregister();
            console.log("[Notification] - NotificationManager:sip.unregister");
        }

        SocketsManager.sip.status.forceRegister = true;
        SocketsManager.checkSockets(false, "notification");

        SocketsManager.setBlockCheckSocket(true);
    },
    checkTimePushToken: async (xucUrl, xucToken, time) => {

        const sendPushToken = () => {

            const setPushTokenSent  = () => {
                console.log("[Notification] - Store push token (firebase/apns) new sent date ", time);
                try {
                    AsyncStorage.setItem(Constants.STORAGE_SERVER_PUSH_TOKEN_SENT, time);
                } catch (error) {
                    console.error("[Notification] - Error while storing info about push token sent ", error);
                }
            };
    
            const sendRequest = (url, callback = false) => {
                let savedPushToken = NotificationManager.token;
        
                if (Platform.OS === 'ios') {
                    if (callback === false) {
                        savedPushToken = NotificationManager.tokenOldIos;
                    }
                }
        
                if (savedPushToken === false) {
                    console.warn("[Notification] - no push token found, check notification startup logs");
                    return;
                }
                console.log("[Notification] - Sending device Push notification token to XUC ", savedPushToken);
                Request(
                    {
                        token: savedPushToken,
                    },
                    url,
                    (error) => {
                        if (callback !== false && error) {
                            console.error('[Notification] - error on new XUC API to handle push tokens');
                            return callback();
                        }
                        if (error) {
                            Alert.alert(
                                "Erreur",
                                typeof error === "string" ? error : "Erreur lors de l'enregistement du token, l'application ne peut fonctionner correctement.",
                                [
                                    {text: "OK"},
                                ],
                                {cancelable: false},
                            );
                            logout();
                            console.error("[Notification] - Error", error);
                            return;
                        }
                        console.log("[Notification] - Push Token has been sent");           
                        setPushTokenSent();     
                    },
                    {
                        "Authorization": "Bearer " + xucToken,
                    },
                    "post",
                );
            }
        
        
            const baseUrl = "config/mobile/push/register/" + Platform.OS.toLowerCase() + '?apiVersion=2.0';
            let url = "https://" + xucUrl + URLs.END_API + baseUrl;
        
            const baseUrlOld = "mobile/push/register";
            let urlOld = "https://" + xucUrl + URLs.END_API + baseUrlOld;
        
            sendRequest(url, () => {
                console.warn('[Notification] - Using old API for registering push token')
                sendRequest(urlOld);
            });
        
        };

        const getPushTokenSent = () => {
            return AsyncStorage.getItem(Constants.STORAGE_SERVER_PUSH_TOKEN_SENT);            
        };    

        getPushTokenSent().then((val) => {
            console.log("[Notification] - Last push token (firebase/apns) sent date known ", val);
            let now = moment();
    
            if (val === null) {
                sendPushToken();
            } else {            
                let prevDate = moment(val);
                prevDate.add(1, "month");
                if (prevDate.unix() < now.unix()) {
                    console.log("[Notification] - Push token (firebase/apns) sent date expired, send a new one");
                    sendPushToken();
                }
            }
        }).catch((error) =>{
            console.error("[Notification] - Error while get info about push token sent ", error);        
            return null;
        });
    },    
    
};

export default NotificationManager;
