import {check, PERMISSIONS, RESULTS} from "react-native-permissions";
import JsSIP from "jssip";
import uuid from "react-native-uuid";

import SoundPlayer from "@helpers/Sound";

import {
    registerGlobals,
} from "react-native-webrtc";

import {EventRegister} from "react-native-event-listeners";
import {Platform, Alert} from "react-native";
import moment from "moment";

import LoginHelper from "@helpers/Login";
import URLs from "@configs/urls";
import EventNames from "@configs/events";
import Constants from "@configs/constants";

import SessionSipManager from "./SessionSipManager";
let ReactNativeForegroundService;
if (Platform.OS === 'android') {
  ReactNativeForegroundService = require('rn-foreground-service').default;
} else {
  ReactNativeForegroundService = null;
}
import AsyncStorage from "@react-native-async-storage/async-storage";
import Toast from 'react-native-toast-message'
import NotificationManager from "./NotificationManager";

//JsSIP.debug.enable("JsSIP:*");
registerGlobals();

let currentState = "active";
let blockCheckSocket = false;
let hasReachableNetwork = true;
let sessionTimeout = null;
let candidateTimeout = null;

EventRegister.addEventListener(EventNames.APP_CHANGE_STATE, (state) => {
    currentState = state;
    console.log("[System] - App changed state", state);
    if (currentState === "active") {
        
        blockCheckSocket = false;
        if (Platform.OS !== "ios") {
            if (!ReactNativeForegroundService.is_running()) {
                checkSockets();
            }
        } else {
            checkSockets();
        }    
    }

    if (currentState === "background") {

        const reset = () => {
            blockCheckSocket = true;
            if (!SessionSipManager.sessions.length) {
                SocketSip.reset();
            }
        }        

        if (Platform.OS !== "ios") {
            if (!ReactNativeForegroundService.is_running()) {
                reset();
            }
        } else {
            reset();
        }            
    }
});

const getNowFormatted = () => {
    let format = Constants.DATE_FORMAT;
    let now = moment();
    
    return now.format(format);
};


export function checkSockets(forceResetAll = false, by = false) {

    if (hasReachableNetwork === false) {
        console.log("[WebSockets] - Check sockets ignored, no network available");
        return;
    }

    if (blockCheckSocket) {
        console.log("[WebSockets] - Check sockets ignored, application is starting or is going to hibernate");
        blockCheckSocket = false;
        return;
    }

    if (!forceResetAll) {
        console.log('[WebSockets] - SIP (Asterisk) connected:'+ SocketSip.status.connected + ' | registered:' + SocketSip.status.register);
        console.log('[WebSockets] - CTI (XUC) connected:'+ SocketCti.status.connected + ' | loggedIn:' + SocketCti.status.logged);
    }

    const after = () => {
        SocketCti.status.reconnexionWithPush = false;
        SocketSip.status.reconnexionWithPush = false;
    };

    const startInit = (wsType, token, after, force = false) => {
        console.log("[WebSockets] start init of Websocket ", wsType);
        if (token === false) {
            callback(false);
            console.error("[WebSockets] - Cannot init websocket, token is missing");
            return;
        }

        switch (wsType) {
            case 'cti': 
                SocketCti.init(token, after, force);
                break;
            case 'sip':
                SocketSip.init(token, SocketSip.configs, after);
                break;
            default:
                console.error("[WebSockets] - Cannot init websocket, ws type is missing");
        }
    }    

    const resetAll = (token) => {
        if (
            SocketSip.status.reconnexionWithPush === false &&
            SocketCti.status.reconnexionWithPush === false &&
            SocketSip.status.initializationStarting === false &&
            SocketCti.status.initializationStarting === false
        ) {
            SocketSip.status.reconnexionWithPush = true;
            SocketCti.status.reconnexionWithPush = true;
            SocketCti.reset("push");
            SocketSip.reset("push");

            startInit('cti', token, after, true);
        } else {
            console.log("[WebSockets] - Ignore reset for both");
        }
    };

    if (SocketCti.status.connected) {
        SocketCti.launchPing();
        checkIceConfig();
    }

    let serviceTimeout = null;
    if (Platform.OS !== "ios") {
        if (by !== "notification") {
            if (SessionSipManager.getTotalSessions() === 0 
                && ReactNativeForegroundService.is_running() 
                && SocketSip.status.register            
            ){                              
                if (serviceTimeout!=null)
                    clearTimeout(serviceTimeout);

                serviceTimeout = setTimeout(() => {
                    if (SessionSipManager.getTotalSessions() === 0 ) {
                        console.log("[Webrtc] - Stopping Android foreground service - No active or future sessions since 5 seconds");
                        ReactNativeForegroundService.stop();
                    }
                }, 5000); // Delay to not stop service if inbetween two retries of XiVO.            
            }
        }
    }

    if (!SocketSip.status.register) {
        SocketSip.forceRegister();
    }

    LoginHelper.getValidToken(null, (token, type) => {

        if (token && type === "new") {            
            SocketCti.token = token;
            SocketSip.token = token;
            if (
                SocketCti.status.connected === false ||
                SocketSip.status.connected === false ||
                SocketSip.status.register === false 
            ) {
                forceResetAll = true;
            }
        }

        if (forceResetAll) {
            console.log("[WebSockets] - Force reset of both sockets");
            resetAll(token);
        } else if (SocketCti.status.connected === false && SocketSip.status.connected === false && SocketSip.status.register === false) {
            console.log("[WebSockets] - Both sockets are disconnected");
            resetAll(token);
        } else if (SocketCti.status.connected === false) {
            console.log("[WebSockets] - Only CTI (XUC) Socket is disconnected");
            if (
                SocketCti.status.reconnexionWithPush === false &&
                SocketCti.status.initializationStarting === false &&
                SessionSipManager.getTotalSessions() == 0                
            ) {
                SocketCti.status.reconnexionWithPush = true;
                SocketCti.reset("push");
                startInit('cti', token, after);

            }
        } else if (SocketSip.status.connected === false && SocketSip.status.register === false) {
            console.log("[WebSockets] - Only SIP (Asterisk) Socket is disconnected");
            if (
                SocketSip.status.reconnexionWithPush === false &&
                SocketSip.status.initializationStarting === false &&
                SessionSipManager.getTotalSessions() == 0
            ) {
                SocketSip.status.reconnexionWithPush = true;
                SocketSip.reset("push");
                startInit('sip', token, after);
            }
        }
    });
}

let prevTypeConnection = false;

const checkNetwork= (networkState) => {
    console.log("[Network] - Network change ", prevTypeConnection + ' => ' + networkState.type);

    hasReachableNetwork = networkState.isConnected;
    if (
        prevTypeConnection !== false &&
        prevTypeConnection !== networkState.type
    ) {
        prevTypeConnection = networkState.type;
        console.log("[Network] - NetWork change force socket reconnection");

        if (SessionSipManager.getTotalSessions() === 0) {
            checkSockets(true);
        }

        console.log("[Webrtc] - Total current sessions", SessionSipManager.getTotalSessions());
        if (SessionSipManager.getTotalSessions() > 0) {
            console.warn("[Network] - Your call might have been cut by network change");
        }

    }

    if (prevTypeConnection === false) {
        prevTypeConnection = networkState.type;
    }
    EventRegister.emit(networkState.isConnected ? EventNames.INTERNET_ON : EventNames.INTERNET_OFF);
}

let SocketSip = {
    status: {
        connected: false,
        initializationStarting: false,
        register: false,
        reconnexionWithPush: false,
    },
    socket: false,
    sip: false,
    token: false,
    configs: {},
    count: 0,
    ignore: {
        unregister: false,
        disconnect: false,
    },
    reset: function reset(to = false) {
        console.log("[WebSockets] - Reset of SIP (Asterisk) websocket");
        SocketSip.status = {
            connected: false,
            initializationStarting: false,
            register: false,
            reconnexionWithPush: false
        };

        SocketSip.ignore = {
            unregister: true,
            disconnect: true,
        };

        if (SocketSip.sip && typeof SocketSip.sip.stop === "function") {
            SocketSip.sip.stop();
        }

        SocketSip.sip = false;
        SocketSip.socket = false;
        SocketSip.session = false;

        if (to !== "push") {
            SocketSip.status.reconnexionWithPush = false;
        }

    },
    check: function check() {
        checkSockets();
    },

    launchPing: function () {
        if (SocketSip.socket && typeof SocketSip.socket.send !== "undefined") {
            SocketSip.socket.send(JSON.stringify({
                claz: "ping",
            }));
        }
    },

    init: function (token, config, callback) {
        console.log("[WebSockets] - Init SIP (Asterisk)");

        if (SocketSip.status.initializationStarting) {
            console.warn("[WebSockets] - SIP initialization already in progress, abort");
            return;
        }

        if (SocketSip.status.connected) {
            console.warn("[WebSockets] - SIP already initialized and connected, abort");
            return;
        }

        if (token === false) {
            console.warn("[WebSockets] - SIP initilization is missing XUC token, abort");
            return;
        }

        if (SocketSip.checkSIPpossible() === false) {
            console.error("[WebSockets] - SIP is not possible, check jsSIP library, abort");
            return;
        }

        if (!config || (config && typeof config.name === "undefined")) {
            console.warn("[WebSockets] - Missing SIP configuration, LINECONFIG event not received yet, abort");
            return;
        }

        if (SocketSip.status.reconnexionWithPush) {
            console.log("[WebSockets] - SIP force reconnection after checking websocket state");
        }

        SocketSip.token = token;
        SocketSip.status.initializationStarting = true;

        LoginHelper.getServer((u) => {
            let domain = "wss://" + URLs.DOMAIN + URLs.END_WS_SIP + "?token=" + token;

            let proxy = false;
            if (typeof (config.sipProxyName) != "undefined" && config.sipProxyName != "" && config.sipProxyName != "default") {
                proxy = config.sipProxyName;
            }

            if (u !== "" && u !== null) {
                let wsServer = URLs.END_WS_SIP;
                if (proxy !== false) {
                    wsServer += "-" + proxy;
                }
                domain = "wss://"+ u + wsServer + "?token=" + token;
            }

            console.log("[WebSockets] - starting SIP using DOMAIN", domain.split("=")[0] + "=XXXX");
            SocketSip.socket = new JsSIP.WebSocketInterface(domain);

            console.log("[WebSockets] - SIP configuration uri sip:" + config.name + "@" + (proxy === false ? "default" : proxy));
            var sip_uri = "sip:" + config.name + "@" + (proxy === false ? "default" : proxy) + ":5060";
            var configuration = {
                sockets: [SocketSip.socket],
                uri: sip_uri,
                contact_uri: sip_uri,
                password: config.password,
                display_name: config.name,
                connection_recovery_min_interval: 1,
                connection_recovery_max_interval: 20,
                user_agent: "XiVO PBX",
                register_expires: 120,
                session_timers: false
            };

            SocketSip.sip = new JsSIP.UA(configuration);
            SocketSip.sip.on("connected", function (e) {
                console.log("[WebSockets] - SIP connected");
                SocketSip.status.connected = true;
                checkTimeConnexionSocket("sip");

                SocketSip.ignore = {
                    unregister: false,
                    disconnect: false,
                };

                if (typeof callback === "function") {
                    callback();
                }
            });
            SocketSip.sip.on("disconnected", function (e) {
                console.log("[WebSockets] - SIP disconnected with " + SessionSipManager.getTotalSessions() + " ongoing session(s)");

                if (!SocketSip.status.register) {
                    SocketSip.status.connected = false;
                }

                if (SessionSipManager.getTotalSessions() > 0) {                    
                    console.log("[WebSockets] - Call(s) might be dropped due to network outage");
                    Toast.show({
                        type: 'info',
                        text1: 'Votre appel a été possiblement coupé ℹ️',
                        text2: 'Vérifiez votre connexion au réseau',
                        position: "bottom",
                        bottomOffset: 110,
                        visibilityTime: 8000
                    });
                }

                if (SocketSip.ignore.disconnect) {
                    console.log("[WebSockets] - SIP disconnected but ignored");
                    SocketSip.ignore.disconnect = false;
                    return;
                }

                SocketSip.status.initializationStarting = false;
                SocketSip.status.reconnexionWithPush = false;

                SocketSip.ignore = {
                    unregister: false,
                    disconnect: false,
                };

                checkSockets();
            });
            SocketSip.sip.on("newRTCSession", function (e) {
                console.log("[Webrtc] - got new SIP call (newRTCSession)");
                let autoAnswer = false;
                if (typeof (e.request.headers["Alert-Info"]) !== "undefined") {
                    for (const key in e.request.headers["Alert-Info"]) {
                        let data = e.request.headers["Alert-Info"][key];
                        if (data.raw.indexOf("autoanswer") !== -1) {
                            autoAnswer = true;
                        }
                    }
                }
                e.session.on('icecandidate', function(candidate) {
                    console.log('[Webrtc] - Getting an ICE candidate' + candidate.candidate.candidate);
                    if (candidateTimeout!=null)
                        clearTimeout(candidateTimeout);
    
                    // 500 ms timeout after the first icecandidate received
                    candidateTimeout = setTimeout(candidate.ready, 500);
                });

                if (e.session.direction == "incoming" && SocketSip.checkSIPpossible() && SessionSipManager.checkIfCallPossible()) {

                    console.log("[Webrtc] - session is SIP incoming call");

                    SessionSipManager.addSession(e.session, "in");
                    let name = "";
                    let number = "";

                    if (
                        typeof (e.session._remote_identity) !== "undefined" &&
                        typeof (e.session._remote_identity._display_name) !== "undefined"
                    ) {
                        name = e.session._remote_identity._display_name;
                    }

                    if (
                        typeof (e.session._remote_identity) !== "undefined" &&
                        typeof (e.session._remote_identity._uri) !== "undefined" &&
                        typeof (e.session._remote_identity._uri._user) !== "undefined"
                    ) {
                        number = e.session._remote_identity._uri._user;
                    }

                    if (name === "") {
                        name = "inconnu";
                        if (number !== "") {
                            name = number;
                        }
                    }

                    if (number === "") {
                        number = "inconnu";
                    }

                    if (!autoAnswer) {
                        e.session.callerName = name;
                        SessionSipManager.displayCall(e.session.idInterne, number, name);
                    }

                    e.session.on("progress", function (e) {                        
                        console.log("[Webrtc] - SIP call in progress");
                    });
                    e.session.on("confirmed", function (e) {
                        console.log("[Webrtc] - SIP call confirmed");
                    });

                    e.session.on("failed", function (e) {
                        console.error("[Webrtc] - SIP call failed", e);
                        let sendPush = true;
                        if (e && e.message && e.message.data && typeof e.message.data === "string") {
                            if (e.message.data.toLowerCase().indexOf("call completed elsewhere") !== -1) {
                                sendPush = false;
                            }
                        }
                        SessionSipManager.removeSession(this.idInterne, 1, "eventSIPfailed", sendPush);
                    });
                    e.session.on("ended", function (e) {
                        console.info("[Webrtc] - SIP call ended");
                        SessionSipManager.removeSession(this.idInterne, 2, "eventSIPended");
                    });

                    if (autoAnswer === true) {
                        SessionSipManager.anwser(e.session.idInterne);
                    }
                }
            });
            SocketSip.sip.on("registered", function (e) {
                console.log("[WebSockets] - SIP registered");                                        
                SocketSip.status.register = true;
                SocketSip.status.connected = true;
                SocketSip.status.initializationStarting = false;
                SocketSip.status.reconnexionWithPush = false;
                blockCheckSocket = false;
                Toast.show({
                    type: 'success',
                    text1: 'Vous êtes enregistré au serveur XiVO ✅',
                    position: "bottom",
                    bottomOffset: 110
                });                
            });
            SocketSip.sip.on("unregistered", function (e) {
                console.log("[WebSockets] - SIP unregistered");                

                if(SocketSip.status.forceRegister){
                    SocketSip.forceRegister();
                }

                if (SocketSip.ignore.unregister) {
                    console.log("[WebSockets] - SIP unregistered but ignored");
                    SocketSip.ignore.unregister = false;
                    return;
                }
                SocketSip.status.register = false;

                checkSockets();
            });
            SocketSip.sip.on("registrationFailed", function (e) {
                console.log("[WebSockets] - SIP registration failed",e);
                SocketSip.status.register = false;
            });
            SocketSip.sip.start();
        });
    },
    checkSIPpossible: function checkSIPpossible() {
        return true;
    },
    forceRegister: function forceRegister() {
        if (SocketSip.sip && typeof SocketSip.sip.register === "function" && SocketSip.status.connected) {
            console.log("[WebSockets] - Force SIP Register");
            SocketSip.sip.register();
        }
    },
    renewIceConfig: function renewIceConfig() {
        console.log("[Webrtc] - Request ICE config ");
        SocketCti.getIceConfig();
    },
    call: function call(number, nameNumber = false) {
        console.log("[Webrtc] - Launch call ", number, nameNumber);
        let withTransfert = false;

        if (nameNumber === false) {
            nameNumber = number;
        }

        if (!SocketSip.status.register) {
            Toast.show({
                type: 'info',
                text1: 'Votre appel n\'a pas pu être initiliasé ℹ️',
                text2: 'Vérifiez votre connexion au réseau',
                position: "bottom",
                bottomOffset: 110
            });
        }
        EventRegister.emit(EventNames.DISPLAY_PHONE);

        if (!SocketSip.status.connected && !SocketSip.status.register) {
            console.error("[WebSockets] - SIP error call code 1 (not connected)");
            return;
        }

        if (!SocketSip.status.register) {
            console.error("[WebSockets] - SIP error call code 2 (not registered)");
            return;
        }

        if (SessionSipManager.sessions.length > 0) {
            withTransfert = true;
        }

        async function callSip() {
            if (!SessionSipManager.checkIfCallPossible()) {
                console.warn("[Webrtc] - Impossible to manage more than 2 calls ");
                Toast.show({
                    type: 'info',
                    text1: 'Vous ne pouvez avoir que 2 appels simultanés ℹ️',
                    position: "bottom",
                    bottomOffset: 110
                });
                return;
            }

            function sendCall() {
                if (withTransfert) {
                    console.log("[Webrtc] - Hold current calls for attented transfer");
                    SessionSipManager.holdAllSession();
                    SocketCti.attendedTransfer(number);
                    let id = uuid.v4();
                    SessionSipManager.saveTempUdid(id);
                    SessionSipManager.startCall(id, number, nameNumber);
                    return;
                }

                let sessionParams = {
                    "eventHandlers": {
                        "progress": function (e) {
                            console.log("[Webrtc] - Outgoing call in progress", e);
                        },
                        "failed": function (e) {
                            console.error("[Webrtc] - Outgoing call failed", e);
                            SoundPlayer.stopCall();
                            SessionSipManager.removeSession(this.idInterne, 1, "eventSIPfailed");
                        },
                        "ended": function (e) {
                            console.log("[Webrtc] - Outgoing call ended");
                            SoundPlayer.stopCall();
                            SessionSipManager.removeSession(this.idInterne, 2, "eventSIPended");
                        },
                        "confirmed": function (e) {
                            SoundPlayer.stopCall();
                            SessionSipManager.startCall(this.idInterne, number, nameNumber);

                            if (sessionTimeout != null) {
                                clearTimeout(sessionTimeout);
                            }
                            sessionTimeout = setTimeout(() => {
                                SessionSipManager.activeCall(this.idInterne);
                            }, 1000);
                        },
                    },
                    "mediaConstraints": {"audio": true, "video": false},
                };

                if (typeof SocketSip.iceConfig !== "undefined") {
                    sessionParams.pcConfig = {
                        "iceServers": SocketSip.iceConfig,
                    };
                }

                try {
                    number = number.replace(/\s/g, "");
                    var session = SocketSip.sip.call("sip:" + number, sessionParams);
                } catch (e) {
                    console.error("[Webrtc] - Error while making SIP call ", e);
                }

                SoundPlayer.playCall();
                SessionSipManager.addSession(session, "out");
            }
            
            const displayError = () => {
                Alert.alert(
                    "Erreur permission",
                    "Vous devez accepter les permissions du micro pour passer un appel",
                    [
                        {
                            text: "OK",
                            style: "cancel",
                        },
                    ],
                    {
                        cancelable: true,
                    },
                );
            };

            let permissionResult = await check(Platform.OS === "ios" ? PERMISSIONS.IOS.MICROPHONE : PERMISSIONS.ANDROID.RECORD_AUDIO);
                switch (permissionResult) {
                case RESULTS.GRANTED:
                    sendCall();
                    break;
                default:
                    displayError();
                    break;
            }
        }

        function callCallback() {
            SocketCti.dialFromMobile(number);
        }

        if (SocketSip.checkSIPpossible()) {
            callSip();
        } else {
            callCallback();
        }

        return false;
    },
};

const initSipByLineConfig = (token) => {
    if (!SocketSip.configs || typeof SocketSip.configs.name === "undefined") {
        console.log("[Webrtc] - SIP configuration is not set, check for stored one");
        getLastLineConfig((val) => {
            if (val === null) {
                console.log("[Webrtc] - SIP configuration is missing in storage, ask for a new LINECONFIG");
                SocketCti.getLineConfig();
            } else {
                console.log("[Webrtc] - Last SIP configuration known ", val.id, ". Proceed to SIP initialization");    
                SocketSip.init(token, val);
            }
        });
    } else {
        console.log("[Webrtc] - SIP configuration set for", SocketSip.configs.name, ". Proceed to SIP initialization");
        SocketSip.init(token, SocketSip.configs);
    }
};


const getLastLineConfig = async (callback) => {
    try {
        let lastLineCfg = await AsyncStorage.getItem(Constants.STORAGE_LINECONFIG);
        lastLineCfg = lastLineCfg != null ? JSON.parse(lastLineCfg) : null;
        callback(lastLineCfg);
    } catch (error) {
        callback();
    }
};

const setLastLineConfig = async (value) => {
    console.log("[Webrtc] - Save SIP configuration", value.id, "in storage ");
    try {
        await AsyncStorage.setItem(Constants.STORAGE_LINECONFIG, JSON.stringify(value));
    } catch (error) {
        console.error("[Webrtc] - Error while setLastLineConfig ", error);
    }
};


const checkIceConfig = () => {
    getLastIceConfig((val) => {
        console.log("[Webrtc] - Last ICE config known ", val.time);
        let now = moment();

        if (val.time === null) {
            SocketSip.renewIceConfig();
        } else {
            let prevConnexion = moment(val.time);
            prevConnexion.add(30, "minutes");
            console.debug("[Webrtc] - Stored ICE config will expire at ", prevConnexion.format(Constants.DATE_FORMAT));
            if (prevConnexion.unix() < now.unix()) {
                console.log("[Webrtc] - ICE config expired, renew it");
                SocketSip.renewIceConfig();
            } else {
                SocketSip.iceConfig = val.data;
            }
        }
    });
};


const getLastIceConfig = async (callback) => {
    try {
        let lastIceCfg = await AsyncStorage.getItem(Constants.STORAGE_ICE_CONFIG);
        lastIceCfg = lastIceCfg != null ? JSON.parse(lastIceCfg) : null;
        let lastIceCfgTime = await AsyncStorage.getItem(Constants.STORAGE_ICE_CONFIG_TIME);
        callback({data: lastIceCfg, time: lastIceCfgTime});
    } catch (error) {
        callback({data: null, time: null});
    }
};



const setLastIceConfig = async (value, time) => {
    console.log("[Webrtc] - Save ICE config time request ", time);
    try {
        await AsyncStorage.setItem(Constants.STORAGE_ICE_CONFIG, JSON.stringify(value));
        await AsyncStorage.setItem(Constants.STORAGE_ICE_CONFIG_TIME, time);
    } catch (error) {
        console.error("[Webrtc] - Error while setLastIceConfig ", error);
    }
};


const checkTimeConnexionSocket = (type) => {
    LoginHelper.getLastLogin(type, (val) => {
        console.log("[WebSockets] - Previous successful " + type +" connection known was ", val);
        LoginHelper.setLastLogin(type, getNowFormatted());
    });
}


let SocketCti = {
    status: {
        connected: false,
        initializationStarting: false,
        logged: false,
        reconnexionWithPush: false,
    },
    ignore: {
        disconnect: false,
        updateUser: false,
    },
    user: false,
    socket: false,
    token: false,
    preference: {
        device: false,
        missingCall: 0,
    },
    reset: function () {
        console.log("[WebSockets] - Reset of CTI (XUC) websocket");
        SocketCti.status = {
            connected: false,
            initializationStarting: false,
            logged: false,
            reconnexionWithPush: false
        };
        if (typeof SocketCti.socket.close === "function") {
            this.ignore.disconnect = true;
            SocketCti.socket.close();
        }
        SocketCti.socket = false;
    },
    init: function (token, callback = () => {}, force = false) {

        console.log("[WebSockets] - Init CTI (XUC)", "force:", force);


        if (token === false) {
            console.warn("[WebSockets] - CTI initilization is missing XUC token, abort");
            callback(false);
            return;
        }

        if (SocketCti.status.initializationStarting) {
            console.warn("[WebSockets] - CTI initilization already in progress, abort");
            callback(false);
            return;
        }

        if (SocketCti.status.connected) {
            console.log("[WebSockets] - CTI already initialized and connected, abort");
            callback(false);
            EventRegister.emit(EventNames.SKIP_LOGIN);
            return;
        }

        if (SocketCti.status.reconnexionWithPush && !force) {
            console.log("[WebSockets] - CTI reconnection has not been forced, abort");
            callback(false);
            return;
        }

        SocketCti.token = token;
        SocketSip.token = token;
        SocketCti.status.initializationStarting = true;
        LoginHelper.getServer((u) => {
            let url = URLs.URL_WS;
            if (u !== "" && u !== null) {
                url = "wss://" + u + URLs.END_WS;
            }

            console.log("[WebSockets] - CTI connection via ", url);

            SocketCti.socket = new WebSocket(url + token);

            SocketCti.socket.onopen = (error) => {
                SocketCti.status.connected = true;
                SocketCti.status.initializationStarting = false;
                SocketCti.status.reconnexionWithPush = false;
                console.log("[WebSockets] - CTI connected ");                
                EventRegister.emit(EventNames.SKIP_LOGIN);
                
                initSipByLineConfig(token);

                checkTimeConnexionSocket("cti");
                checkIceConfig();
                NotificationManager.checkTimePushToken(u, token, getNowFormatted());            

                if (typeof callback === "function") {
                    callback(true);
                }
            };

            SocketCti.socket.onmessage = (event) => {
                SocketCti.status.initializationStarting = false;
                SocketCti.status.reconnexionWithPush = false;
                if (event.data) {
                    let data = JSON.parse(event.data);
                    if (typeof data.msgType !== "undefined") {
                        console.debug("[WebSockets] - CTI received message " + data.msgType, data);
                    }

                    EventRegister.emit(EventNames.CTI.ADD_DATA, {
                        key: "message",
                        data: data.ctiMessage,
                    });

                    if (data.msgType === "LinkStatusUpdate") {
                        if (data.ctiMessage.status === "down") {
                            SocketCti.status.connected = false;
                            checkSockets();
                            return;
                        }
                    }
                    if (data.msgType === "LoggedOn") {
                        SocketCti.status.logged = true;
                    }

                    if (data.msgType === "UserPreference") {
                        if (typeof data.ctiMessage.PREFERRED_DEVICE !== "undefined") {
                            if (SocketCti.preference.device === false) {
                                if (data.ctiMessage.PREFERRED_DEVICE.value === Constants.ROUTING.webApp) {
                                    EventRegister.emit(EventNames.OPEN_ALERT_ROUTING);
                                }
                            }
                            SocketCti.preference.device = data.ctiMessage.PREFERRED_DEVICE.value;
                        }
                        if (typeof data.ctiMessage.NB_MISSED_CALL !== "undefined") {
                            SocketCti.preference.missingCall = data.ctiMessage.NB_MISSED_CALL.value;
                        }
                        console.log("[WebSockets] - CTI User preference message content received: ", data.ctiMessage);
                    }

                    if (data.msgType === "UserConfigUpdate") {
                        SocketCti.user = data.ctiMessage;
                    }

                    if (data.msgType === "PhoneHintStatusEvent") {
                        EventRegister.emit(EventNames.CTI.UPDATE_STATUS_DATA, {
                            data: data.ctiMessage,
                        });
                    }

                    if (data.msgType === "PhoneEvent" && data.ctiMessage.eventType !== "EventReleased") {
                        EventRegister.emit(EventNames.CTI.ATTACH_EVENT, data.ctiMessage);                
                    }
                    if (data.msgType === "LineConfig") {
                        if (typeof data.ctiMessage.isUa !== "undefined" && data.ctiMessage.isUa) {
                            Alert.alert(
                                "Erreur",
                                "Votre compte utilisateur n'est pas compatible avec l'application mobile.",
                                [
                                    {
                                        text: "OK",
                                        style: "cancel",
                                        onPress: () => {
                                            LoginHelper.logout();
                                        },
                                    },
                                ],
                                {
                                    cancelable: true,
                                },
                            );
                        }
                        SocketSip.configs = data.ctiMessage;
                        setLastLineConfig(data.ctiMessage);
                        initSipByLineConfig(token);
                    }

                    if (data.msgType === "DirectoryResult") {
                        EventRegister.emit(EventNames.CTI.ADD_DATA, {
                            key: "search",
                            data: SocketCti.cleanSearch(data.ctiMessage),
                        });
                    }

                    if (data.msgType === "CallHistory") {
                        let entries = [];
                        let current = "";
                        let now = moment().format("YYYY-MM-DD");
                        let yesterday = moment().subtract(1, "d").format("YYYY-MM-DD");

                        SocketCti.cleanHistories(data.ctiMessage).map(function (history) {
                            let momentHistory = moment(history.data.start);

                            let headerTitle = momentHistory.format("DD/MM/YYYY");
                            if (history.day === now) {
                                headerTitle = "Aujourd'hui";
                            }
                            if (history.day === yesterday) {
                                headerTitle = "Hier";
                            }
                            if (headerTitle !== current) {
                                entries.push({
                                    typeRow: "header",
                                    title: headerTitle,
                                });
                                current = headerTitle;
                            }
                            entries.push(history);
                        });

                        EventRegister.emit(EventNames.CTI.ADD_DATA, {
                            key: "histories",
                            data: entries,
                        });
                    }

                    if (data.msgType === "FavoriteUpdated") {
                        EventRegister.emit(EventNames.CTI.UPDATE_FAVORITES, {
                            contactId: data.ctiMessage.contact_id,
                            action: data.ctiMessage.action,
                        });
                    }

                    if (data.msgType === "VoiceMailStatusUpdate") {
                        EventRegister.emit(EventNames.CTI.UPDATE_VOICEMAIL, data.ctiMessage);
                    }

                    if (data.msgType === "UserConfigUpdate" && SocketCti.ignore.updateUser === false) {
                        EventRegister.emit(EventNames.CTI.ADD_DATA, {
                            key: "user",
                            data: data.ctiMessage,
                        });
                    }


                    if (data.msgType === "UserConfigUpdate" && SocketCti.ignore.updateUser) {
                        SocketCti.ignore.updateUser = false;
                    }


                    if (data.msgType === "Favorites") {
                        EventRegister.emit(EventNames.CTI.ADD_DATA, {
                            key: "favorites",
                            data: SocketCti.cleanFavorites(data.ctiMessage),
                        });
                    }

                    if (data.msgType === "IceConfig") {
                        SocketCti.processIceConfig(data.ctiMessage);
                    }
                }
            };
            SocketCti.socket.onerror = (error) => {                
                if (error && typeof error.message !== "undefined") {
                    console.error("[WebSockets] - CTI Error ", error.message);
                }
                SocketCti.status.initializationStarting = false;
                SocketSip.status.initializationStarting = false;
                checkSockets();
            };
            SocketCti.socket.onclose = (event) => {

                SocketCti.status.connected = false;
                SocketCti.status.logged = false;
                if (event && typeof event.message !== "undefined") {
                    console.log("[WebSockets] - CTI was closed", event.message);
                }

                if (this.ignore.disconnect) {
                    console.log("[WebSockets] - CTI was disconnected but ignored", event.message);
                    this.ignore.disconnect = false;
                    return;
                }

                SocketCti.status.initializationStarting = false;
                SocketCti.status.reconnexionWithPush = false;

                SocketSip.status.initializationStarting = false;
                SocketSip.status.reconnexionWithPush = false;
                SocketSip.status.connected = false;
                SocketSip.status.register = false;

                Toast.show({
                    type: 'error',
                    text1: 'Vous avez été déconnecté du serveur ⚠️',
                    position: "bottom",
                    bottomOffset: 110
                });

                checkSockets();
            };
        });
    },

    processIceConfig(iceCfg) {
        console.log("[Webrtc] - New ICE config received ", iceCfg);
        let stunConfig = iceCfg.stunConfig;
        let turnConfig = iceCfg.turnConfig;
        let data = [];

        [stunConfig, turnConfig]
            .filter(c => c !== undefined)
            .map(({ttl, ...c}) => c)
            .forEach(c => data.push(c));

        if (data.length > 0) {
            console.log("[Webrtc] - Set list of ICE Servers received", data);
            SocketSip.iceConfig = data;
            setLastIceConfig(data, getNowFormatted());
        }
    },
    hangUp: function () {
        SocketCti.sendMessage({
            command: "hangUp",
        });
    },

    dialFromMobile: function (destination) {
        SocketCti.sendMessage({
            destination,
            command: "dialFromMobile",
        });
    },

    completeTransfer: function () {
        SocketCti.sendMessage({
            command: "completeTransfer",
        });
    },

    cancelTransfer: function () {
        SocketCti.sendMessage({
            command: "cancelTransfer",
        });
    },

    getFavorites: function () {
        EventRegister.emit(EventNames.CTI.FETCH_DATA, "favorites");
        SocketCti.sendMessage({
            command: "getFavorites",
        });
    },

    getSearch: function (search) {
        EventRegister.emit(EventNames.CTI.FETCH_DATA, "search");
        SocketCti.sendMessage({
            command: "directoryLookUp",
            term: search,
        });
    },

    getHistories: function () {
        EventRegister.emit(EventNames.CTI.FETCH_DATA, "histories");
        SocketCti.sendMessage({
            command: "getUserCallHistoryByDays",
            days: 7,
        });
    },

    launchPing: function () {
        SocketCti.sendMessage({
            claz: "ping",
        });
    },

    removeOnFavorite: function (contactId, source) {
        SocketCti.sendMessage({
            command: "removeFavorite",
            contactId: contactId,
            source: typeof source !== "undefined" ? source : "internal",
        });
    },

    changeDistrub: function (value) {
        SocketCti.sendMessage({
            command: "dnd",
            state: value,
        });
    },

    changeTransfert: function (value, number, onlyNumber = false) {
        if (!onlyNumber) {
            SocketCti.ignore.updateUser = true;
            SocketCti.sendMessage({
                command: "naFwd",
                state: false,
                destination: "",
            });
        }

        if (onlyNumber && value === false) {
            SocketCti.ignore.updateUser = true;
            SocketCti.sendMessage({
                command: "uncFwd",
                state: true,
                destination: number,
            });
        }

        SocketCti.sendMessage({
            command: "uncFwd",
            state: value,
            destination: number,
        });
    },

    changeTransfertNoResponse: function (value, number, onlyNumber = false) {
        if (!onlyNumber) {
            SocketCti.ignore.updateUser = true;
            SocketCti.sendMessage({
                command: "uncFwd",
                state: false,
                destination: "",
            });
        }

        if (onlyNumber && value === false) {
            SocketCti.ignore.updateUser = true;
            SocketCti.sendMessage({
                command: "naFwd",
                state: true,
                destination: number,
            });
        }

        SocketCti.sendMessage({
            command: "naFwd",
            state: value,
            destination: number,
        });
    },

    setTransfert: function () {
        LoginHelper.getTransfert((number) => {
            if (number !== null && number !== "") {
                SocketCti.sendMessage({
                    command: "uncFwd",
                    destination: number,
                    state: true,
                });
            } else {
                Alert.alert(
                    "Erreur",
                    "Vous devez renseigner votre numéro de renvoi inconditionel",
                    [
                        {text: "OK"},
                    ],
                    {cancelable: false},
                );

            }

        });
    },

    removeTransfert: function () {
        SocketCti.sendMessage({
            command: "uncFwd",
            destination: "",
            state: false,
        });
    },

    attendedTransfer: function (destination) {
        SocketCti.sendMessage({
            destination,
            device: "MobileApp",
            command: "attendedTransfer",
        });
    },

    unregisterMobileApp: function () {
        SocketCti.sendMessage({
            command: "unregisterMobileApp",
        });
    },

    changePreferenceDevice: function (value) {
        if (value) {
            SocketCti.sendMessage({
                command: "setUserPreference",
                key: "PREFERRED_DEVICE",
                value: value,
                value_type: "String",
            });
        }
        SocketCti.preference.device = value;
    },

    changePreferenceUser: function (key, value, type = "String") {
        SocketCti.sendMessage({
            command: "setUserPreference",
            key: key,
            value: value,
            value_type: type,
        });
    },

    addOnFavorite: function (contactId, source) {
        SocketCti.sendMessage({
            command: "addFavorite",
            contactId: contactId,
            source: typeof source !== "undefined" ? source : "internal",
        });
    },

    getIceConfig: function () {
        SocketCti.sendMessage({
            command: "getIceConfig",
        });
    },

    getLineConfig: function () {
        SocketCti.sendMessage({
            command: "getConfig",
            objectType: "line"
        });
    },

    sendMessage: function (message) {

        if (
            SocketCti.socket !== false &&
            SocketCti.status.connected &&
            typeof SocketCti.socket.send === "function"
        ) {
            if (!message.claz) {
                message.claz = "web";
            }
            try {
                console.log("[WebSockets] - CTI message sent ", JSON.stringify(message));
                SocketCti.socket.send(JSON.stringify(message));
            } catch (error) {
                console.log("[WebSockets] - CTI Error stringify", message);
            }
        } else {
            console.warn("[WebSockets] - CTI message not sent as socket is not reachable");
        }
    },

    cleanFavorites: function (message) {
        return SocketCti.cleanItems(message);
    },

    cleanSearch: function (message) {
        return SocketCti.cleanItems(message, 50);
    },

    cleanHistories: function (message) {
        let histories = [];
        message.map((history) => {
            let name = "";
            let num = history.dstNum;
            if (history.dstFirstName !== null) {
                name += history.dstFirstName + " ";
            }
            if (history.dstLastName !== null) {
                name += history.dstLastName;
            }

            if (name == "") {
                name = history.dstNum;
            }

            if (history.status == "missed" || history.status == "answered") {
                num = history.srcNum;
                name = "";
                if (history.srcFirstName !== null) {
                    name += history.srcFirstName + " ";
                }
                if (history.srcLastName !== null) {
                    name += history.srcLastName;
                }

                if (name == "") {
                    name = history.srcNum;
                }
            }

            let hour = "";
            let day = "";
            if (history.start) {
                let temp = history.start.split(" ");
                if (temp.length === 2) {
                    day = temp[0];
                    hour = temp[1];
                }
            }
            histories.push({
                name,
                num,
                day,
                hour,
                type: history.status,
                data: history,
            });
        });
        return histories;
    },


    cleanItems: function (message, limit = 100) {
        let items = [];
        message.entries.map((item, index) => {
            if (index > limit) {
                return;
            }

            let entry = {};
            message.headers.map((name, index) => {
                if (typeof item.entry[index] !== "undefined") {
                    entry[name] = item.entry[index];
                }
            });

            let name = "";
            if (typeof entry.Nom !== "undefined") {
                name = entry.Nom;
            }
            if (typeof entry.name !== "undefined") {
                name = entry.name;
            }
            let number = "";
            if (typeof entry["Numéro"] !== "undefined") {
                number = entry["Numéro"];
            }
            if (typeof entry.number !== "undefined") {
                number = entry.number;
            }
            let mobile = "";
            if (typeof entry.Mobile !== "undefined") {
                mobile = entry.Mobile;
            }
            if (typeof entry.mobile !== "undefined") {
                mobile = entry.mobile;
            }

            let extra = "";
            if (typeof entry.external_number !== "undefined") {
                extra = entry.external_number;
            }

            let source = Constants.TYPES_ITEM.NUMBER;
            if (typeof item.source !== "undefined" && item.source === "xivo_meetingroom") {
                source = Constants.TYPES_ITEM.CONF;
            }

            items.push({
                name,
                num: number,
                mobile,
                extra,
                data: item,
                fav: item.favorite,
                status: item.status,
                contactId: item.contact_id,
                source,
            });
        });
        return items;
    },


};

const SocketsManager = {
    cti: SocketCti,
    sip: SocketSip,
    checkNetwork,
    checkSockets,
    setBlockCheckSocket: (val = true) => {
        if (currentState === "active" && val) {
            return;
        }
        blockCheckSocket = val;
    },
    isActiveState: () => {
        return currentState === "active";
    }
};
SessionSipManager.setSockets(SocketsManager);

export default SocketsManager;