import RNCallKeep from "react-native-callkeep";
import uuid from "react-native-uuid";
import {
    Platform,
} from "react-native";
import {EventRegister} from "react-native-event-listeners";
let ReactNativeForegroundService;
if (Platform.OS === 'android') {
  ReactNativeForegroundService = require('rn-foreground-service').default;
} else {
  ReactNativeForegroundService = null;
}

import EventNames from "@configs/events";
import SocketsManager from "@helpers/SocketsManager";
import NotificationManager from "@helpers/NotificationManager";

let options = {
    ios: {
        appName: "Avencall XiVO",
        imageName: "sim_icon",
        supportsVideo: false,
        maximumCallGroups: "1",
        maximumCallsPerCallGroup: "1",
    },
    android: {
        selfManaged: false,
        alertTitle: "Les permissions requises",
        alertDescription: "Cette application doit accéder à vos comptes téléphoniques",
        cancelButton: "Annuler",
        okButton: "OK",
        imageName: "phone_account_icon",
        additionalPermissions: [],
        foregroundService: { 
            channelId: "com.avencall.phone",
            channelName: "XiVO-service",
            notificationTitle: "XiVO service de gestion du microphone",
            notificationIcon: "",
        },
    },
};

let SessionSipManager = {
    uuidTemp: false,
    uuidTempFakeCall: "c413a9dd-dab9-4902-9295-79409afa2920",
    sessions: [],
    session: false,
    sockets: {},
    disabledHold: false,
    speaker: false,
    init: function (callback) {
        console.log('[Webrtc] - Callkit event listerners init')
        RNCallKeep.canMakeMultipleCalls(false);
        RNCallKeep.setup(options).then((accepted) => {
            if (typeof callback === "function") {
                callback();
            }
            SessionSipManager.addEventCallKit();
        });

    },
    activeSpeaker: function (id = false, value) {
        RNCallKeep.getAudioRoutes().then((routes) => {
            console.log("[Webrtc] - Available Audio peripherals", routes);

            if (id === false) {
                id = this.session.idInterne;
            }
    
            let session = this.getSessionById(id);
            if (session !== false) {
                let routeName;
                if (value) {
                    routeName = "Speaker"
                } else {
                    let btDevice = routes.find(route => route.type == "Bluetooth");
                    let phone = routes.find(route => route.type == "Phone");
    
                    routeName = btDevice ? btDevice.name : phone.name
                }    
                console.log("[Webrtc] - set output audio to ", routeName);
                RNCallKeep.setAudioRoute(id, routeName);
                SessionSipManager.speaker = value;
            }
        });
    }
    ,
    setSockets: function (sockets) {
        this.sockets = sockets;
    }
    ,
    checkIfCallPossible: function () {
        return this.sessions.length < 2;
    }
    ,
    displayCall: function (id, number, name) {
        console.log("[Webrtc] - display IncomingCall", id, number, name);
        RNCallKeep.backToForeground();

        if (Platform.OS === "android") {
            RNCallKeep.displayIncomingCall(id, number, name);
        } else {
            RNCallKeep.displayIncomingCall(id, number, name, "number");
        }
    }
    ,
    holdAllSession: function () {
        this.sessions.map((sessionData) => {
            this.holdCall(sessionData.item.idInterne, true);
        });
    }
    ,

    saveTempUdid: function (udid) {
        this.uuidTemp = udid;
    }
    ,
    activeCall: function (id) {
        console.log("[Webrtc] - set current call active", id);
        RNCallKeep.setCurrentCallActive(id);
        RNCallKeep.backToForeground();

        let activeSessionData = false;
        this.sessions.map((sessionData, index) => {
            if (sessionData.item.idInterne !== id) {
                this.holdCall(sessionData.item.idInterne, true);
            } else {
                activeSessionData = sessionData;
            }
        });
        if (activeSessionData !== false) {
            this.session = this.getSessionById(id);
            EventRegister.emit(EventNames.CTI.ADD_DATA, {
                key: "session",
                data: activeSessionData,
            });
        }

        this.holdCall(id, false);
    }   
    ,
    getHoldCall: function (id = false) {
        if (id === false) {
            id = this.session.idInterne;
        }
        let session = this.getSessionById(id);
        if (session !== false) {
            let value = session.isOnHold().local;
            return value;
        }
    }
    ,
    holdCall: function (id, hold = true, to = false) {
        if (id === false) {
            id = this.session.idInterne;
        }

        let session = this.getSessionById(id);
        if (session !== false) {
            if (hold && !session.isOnHold().local) {
                console.log("[Webrtc] - hold Call", id);
                session.hold();
            }

            if (hold === false && session.isOnHold().local) {
                console.log("[Webrtc] - unhold Call", id);
                session.unhold();
            }

            if (to === false) {
                this.disabledHold = true;
                RNCallKeep.setOnHold(id, hold);
            }
        }

        if (to === "event" && this.sessions.length > 1) {

            let otherSession = false;
            this.sessions.map((ss, indexSS) => {
                if (ss.item.idInterne !== this.session.idInterne) {
                    otherSession = ss.item;
                }
            });


            let currentHold = this.getHoldCall(this.session.idInterne);
            let otherHold = this.getHoldCall(otherSession.idInterne);

            if (hold && currentHold && otherHold) {
                this.switchSession(otherSession.idInterne, "auto");
            }
        }
    }
    ,
    getMuteCall: function (id = false) {
        if (id === false) {
            id = this.session.idInterne;
        }
        let session = this.getSessionById(id);
        if (session !== false) {
            return session.isMuted().audio;
        }
    }
    ,

    muteCall: function (id = false, mute, to = false) {
        if (id === false) {
            id = this.session.idInterne;
        }
        let session = this.getSessionById(id);
        if (session !== false) {
            if (mute) {
                console.log("[Webrtc] - mute Call", id);
                session.mute({audio: true});
            }
            if (mute === false) {
                console.log("[Webrtc] - unmute Call", id);
                session.unmute({audio: true});
            }

            if (to === false) {
                RNCallKeep.setMutedCall(id, mute);
            }

        }
    },
    startCall: function (id, number = "", name = "") {

        if (name === "" && number !== "") {
            name = number;
        }

        if (name === "") {
            name = "Inconnu";
        }
        if (number === "") {
            number = "Inconnu";
        }

        console.log("[Webrtc] - start Call", id, number, name);
        if (Platform.OS === "ios") {
            RNCallKeep.startCall(id, number, name, "generic", false);
            this.activeSpeaker(id, true);
            this.activeSpeaker(id, false); // naughty hack to force user gesture on applying correct audio source
        } else {
            RNCallKeep.startCall(id, number, name);
        }
        this.holdCall(id, false);
    }
    ,
    addSession: function (session, type, autoAnswer = false) {
        console.log("[Webrtc] - add Session", type);

        if (Platform.OS === "ios" && this.uuidTempFakeCall) {
            try {
                if (RNCallKeep.isCallActive(this.uuidTempFakeCall)) {
                    RNCallKeep.endCall(this.uuidTempFakeCall);
                }
            } catch (e) {
                console.error("[Webrtc] - Error with iOS fake call", e)
            }
        }

        EventRegister.emit(EventNames.DISPLAY_PHONE);
        EventRegister.emit(EventNames.CLOSE_ALL_MODAL);
        RNCallKeep.backToForeground();


        session.idInterne = this.uuidTemp !== false ? this.uuidTemp : uuid.v4();
        session.startedForTransfert = this.uuidTemp !== false ? true : false;
        this.uuidTemp = false;
        session.autoAnswer = autoAnswer;
        session.startedTerminate = false;
        session.startedAnswer = false;
        console.log("[Webrtc] - auto answer session: ", session.autoAnswer, " | start transfering session: ", session.startedForTransfert, " for id ", session.idInterne);

        if (this.session === false) {
            this.session = session;

            EventRegister.emit(EventNames.CTI.ADD_DATA, {
                key: "session",
                data: {
                    item: session,
                    direction: type,
                    event: {}
                },
            });
        }

        this.sessions.push({
            item: session,
            direction: type,
            event: {},
        });
        EventRegister.emit(EventNames.CTI.ADD_DATA, {
            key: "sessions",
            data: this.sessions,
        });


        if (Platform.OS !== "ios") {
            if (this.sessions.length > 0) {
                if (!ReactNativeForegroundService.is_running()) {
                    console.log("[System] - start foreground service");
                    ReactNativeForegroundService.start({
                        id: 18031988,
                        title: "Xivo service",
                        message: "Démarrage du service en tâche de fond",
                        ServiceType: "phoneCall"
                    });
                }
            }
        }

        console.log("[Webrtc] - Total ongoing session(s) ", this.sessions.length);
        return true;
    }
    ,
    switchSession: function (id, mode = false) {
        this.holdCall(this.session.idInterne, true);
        let session = false;
        let sessionData = false;
        this.sessions.map((ss) => {
            if (ss.item.idInterne === id) {
                session = ss.item;
                sessionData = ss;
            }
        });
        if (session !== false) {
            SessionSipManager.activeCall(id);
            EventRegister.emit(EventNames.CTI.ADD_DATA, {
                key: "session",
                data: sessionData,
            });
        }
    }
    ,
    transfert: function () {
        this.session.completeTransfer = true;
        this.sessions.map((ss) => {
            ss.item.completeTransfer = true;
        });

        EventRegister.emit(EventNames.CTI.ADD_DATA, {
            key: "session",
            data: this.session,
        });
        EventRegister.emit(EventNames.CTI.ADD_DATA, {
            key: "sessions",
            data: this.sessions,
        });

        SocketsManager.cti.completeTransfer();
        setTimeout(() => {
            RNCallKeep.endAllCalls();
        }, 500);
    },
    getTotalSessions: function () {
        return this.sessions && this.sessions.length;
    },
    removeSession: function (id = false, raison = 1, to = false, sendPush = true) {
        if (id === false) {
            id = this.session.idInterne;
        }

        RNCallKeep.reportEndCallWithUUID(id, raison);
        RNCallKeep.endCall(id);
        SessionSipManager.speaker = false;

        console.log("[Webrtc] - remove Session", id, raison);

        let session = this.getSessionById(id);
        if (session !== false && typeof session.terminate === "function") {
            if (session.startedTerminate) {
                console.log("[Webrtc] - Session removal ignored ! session termination in progress...", id);
                return;
            }
            if (!session.startedAnswer && typeof session.callerName !== "undefined" && sendPush) {
                NotificationManager.sendLocalPush(session.callerName);
            }

            session.startedTerminate = true;
            console.log("[Webrtc] - auto answer session: ", session.autoAnswer, " | start transfering session: ", session.startedForTransfert, " for id ", id);
            session.unhold();

            if (to !== "eventSIPfailed" && to !== "eventSIPended") {
                if (typeof session.completeTransfer !== "undefined") {
                    console.log("[Webrtc] - session removal ignored ! session already completed", id);
                }
                if (typeof session.completeTransfer === "undefined") {
                    if (session.startedForTransfert) {
                        console.log("[Webrtc] - cancel transfer", id);
                        SocketsManager.cti.cancelTransfer();
                    } else {
                        try {
                            console.log("[Webrtc] - Session terminated", id);
                            session.terminate();
                        } catch (e) {
                            console.error("[Webrtc] - Error while terminating session", e);
                        }
                    }
                }

            }

        }

        let newSessions = [];
        this.sessions.map((sessionData) => {
            if (sessionData.item.idInterne !== id) {
                newSessions.push(sessionData);
            }
        });


        EventRegister.emit(EventNames.CTI.ADD_DATA, {
            key: "sessions",
            data: newSessions,
        });

        this.sessions = newSessions;
        console.log("[Webrtc] - Total ongoing session(s) ", this.sessions.length);

        if (newSessions.length === 0) {
            this.session = false;
            EventRegister.emit(EventNames.CTI.ADD_DATA, {
                key: "session",
                data: false,
            });
            console.log("[Webrtc] - End all calls");
            RNCallKeep.endAllCalls();

            if (Platform.OS !== "ios") {
                if (ReactNativeForegroundService.is_running()) {
                    console.log("[System] - Stop foreground service as no more ongoing calls");
                    ReactNativeForegroundService.stop();
                }
            }
        } else {
            this.session = newSessions[0].item;
            EventRegister.emit(EventNames.CTI.ADD_DATA, {
                key: "session",
                data: newSessions[0],
            });
            SessionSipManager.activeCall(this.session.idInterne);
        }
    },
    getSessionById: function (id) {
        let session = false;
        this.sessions.map((ss, index) => {
            if (ss.item.idInterne === id) {
                session = ss.item;
            }
        });
        return session;
    }
    ,
    anwser: function (id = false, to = false) {
        if (id === false) {
            id = this.session.idInterne;
        }

        console.log("[Webrtc] - start anwsering call", id);
        let params = {};
        if (typeof this.sockets.sip.iceConfig !== "undefined") {
            params = {"pcConfig": {"iceServers": this.sockets.sip.iceConfig}};
        }
        let session = this.getSessionById(id);

        if (session !== false && typeof session.answer === "function") {

            if (session.startedAnswer) {
                console.warn("[Webrtc] - Answer ignored ! answer already in progress... ", id);
                return;
            }
            session.startedAnswer = true;

            console.log("[Webrtc] - anwser call", id, params);
            try {
                session.answer(params);
            } catch (e) {
                console.error("[Webrtc] - Error while answering call ", e);
            }

            if (to === false) {
                RNCallKeep.answerIncomingCall(id);
            }

            this.activeCall(id);

        }
    }
    ,
    addEventCallKit: function () {

        RNCallKeep.removeEventListener("didToggleHoldCallAction");
        RNCallKeep.removeEventListener("didPerformSetMutedCallAction");
        RNCallKeep.removeEventListener("answerCall");
        RNCallKeep.removeEventListener("endCall");

        RNCallKeep.addEventListener("didToggleHoldCallAction", (params) => {

            if (this.disabledHold) {
                this.disabledHold = false;
                return;
            }
            let {hold, callUUID} = params;
            if (callUUID === this.uuidTempFakeCall) {
                console.warn("[Webrtc] - ignore event as iOS fake call");
                return;
            }
            console.log("[Webrtc] - received didToggleHoldCallAction event", params);
            this.holdCall(callUUID, hold, "event");
        });

        RNCallKeep.addEventListener("didPerformSetMutedCallAction", (params) => {

            console.log("[Webrtc] - received didPerformSetMutedCallAction event", params);
            let {muted, callUUID} = params;
            if (callUUID === this.uuidTempFakeCall) {
                console.warn("[Webrtc] - ignore event as iOS fake call");
                return;
            }
            this.muteCall(callUUID, muted, "event");
        });

        RNCallKeep.addEventListener("answerCall", ({callUUID}) => {
            console.log("[Webrtc] - received answerCall event ", callUUID);
            if (callUUID === this.uuidTempFakeCall) {
                console.warn ("[Webrtc] - ignore event as iOS fake call");
                return;
            }
            this.anwser(callUUID, "event");
        });

        RNCallKeep.addEventListener("endCall", ({callUUID}) => {
            console.log("[Webrtc] - received endCall event ", callUUID);
            if (callUUID === this.uuidTempFakeCall) {
                console.warn("[Webrtc] - ignore event as iOS fake call");
                return;
            }
            this.removeSession(callUUID, 1, "event", false);
        });


        return () => {
            RNCallKeep.removeEventListener("didToggleHoldCallAction");
            RNCallKeep.removeEventListener("didPerformSetMutedCallAction");
            RNCallKeep.removeEventListener("answerCall");
            RNCallKeep.removeEventListener("endCall");
        };
    }
};


export default SessionSipManager;