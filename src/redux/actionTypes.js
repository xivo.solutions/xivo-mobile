const generateBaseExport = (prefix) => {
	return {
		GET: prefix + '-GET',
		FETCHING: prefix + '-FETCHING',
		LOADED: prefix + '-LOADED',
		ERROR: prefix + '-ERROR',
		RESET: prefix + '-RESET',
	};
};

export const DATA = {
	...generateBaseExport('DATA'),
	UPDATE_STATUS_DATA: 'DATA-UPDATE_STATUS_DATA',
	UPDATE_FAVORITES: 'DATA-UPDATE_FAVORITES',
	VOICEMAIL: 'DATA-VOICEMAIL',
	ATTACH_EVENT: 'DATA-ATTACH_EVENT',
};
