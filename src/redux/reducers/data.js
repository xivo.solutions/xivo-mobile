import {DATA} from "@redux/actionTypes";

const initialState = {
    contacts: {
        data: [],
        fetching: false,
        loaded: false,
        error: {
            status: false,
            message: "",
        },
    },
    favorites: {
        data: [],
        fetching: false,
        loaded: false,
        error: {
            status: false,
            message: "",
        },
    },
    histories: {
        countNotification: 0,
        data: [],
        fetching: false,
        loaded: false,
        error: {
            status: false,
            message: "",
        },
    },
    search: {
        data: [],
        fetching: false,
        loaded: false,
        error: {
            status: false,
            message: "",
        },
    },
    user: {
        data: [],
    },
    voiceMail: {
        notificationCount: 0,
        data: {},
    },
    message: {
        data: {},
    },
    sessions: {
        data: [],
    },
    session: {
        data: {},
    },
    phoneEvent: {
        data: {},
    },
    status: {
        count: 0,
        data: {},
    },
};

export default (state = initialState, action) => {
    switch (action.type) {
        case DATA.VOICEMAIL: {
            const data = action.payload;
            return {
                ...state,
                voiceMail: {
                    notificationCount: data.newMessages,
                    data: data,
                },
            };
        }
        case DATA.UPDATE_FAVORITES: {
            const data = action.payload;

            let favorites = state.favorites.data;
            let search = state.search.data;

            let cleanFavorites = [];
            favorites.map((favorite) => {
                if (data.action === "Removed" && data.contactId !== favorite.contactId) {
                    cleanFavorites.push(favorite);
                }
            });

            let cleanSearch = [];
            search.map((s) => {
                if (data.contactId === s.contactId) {
                    s.fav = data.action !== "Removed";
                }
                cleanSearch.push(s);
            });

            return {
                ...state,
                search: {
                    ...state.search,
                    data: cleanSearch,
                },
                favorites: {
                    ...state.favorites,
                    data: cleanFavorites,
                },
            };
        }
        case DATA.UPDATE_STATUS_DATA: {
            const {data} = action.payload;

            let {status} = state;

            status.data[data.number] = data.status;
            return {
                ...state,
                status: {
                    ...status,
                    count: status.count++
                }
            }
            /*let favorites = state.favorites.data;
            let search = state.search.data;
            let histories = state.histories.data;

            let cleanHistories = [];
            histories.map((history) => {
                if (history.num == data.number) {
                    history.statusUser = data.status;
                }
                cleanHistories.push(history);
            });

            let cleanFavorites = [];
            favorites.map((favorite) => {
                if (favorite.num == data.number) {
                    favorite.status = data.status;
                }
                cleanFavorites.push(favorite);
            });

            let cleanSearch = [];
            search.map((s) => {
                if (s.num == data.number) {
                    s.status = data.status;
                }
                cleanSearch.push(s);
            });

            return {
                ...state,
                search: {
                    ...state.search,
                    data: cleanSearch,
                },
                favorites: {
                    ...state.favorites,
                    data: cleanFavorites,
                },
                histories: {
                    ...state.histories,
                    data: cleanHistories,
                },
            };*/
        }
        case DATA.ATTACH_EVENT: {
            const event = action.payload;
            const idSessionOnEvent = event.userData.SIPCALLID;
            //console.debug("[Session] -  id received ", idSessionOnEvent)

            let sessions = [];
            if (state.sessions.data && typeof state.sessions.data.map === "function") {
                state.sessions.data.map((sessionData) => {
                    let idSession = sessionData.item.id;
                    if (idSession && idSession.indexOf(idSessionOnEvent) !== -1) {
                        sessionData.event = event;
                    }
                    sessions.push(sessionData);
                });
            }
            let session = {...state.session.data};
            if (session && session.item) {
                let idSession = session.item.id;
                if (idSession && idSession.indexOf(idSessionOnEvent) !== -1) {
                    session.event = event;
                }
            }
            //console.debug("[Session] - session ", session)
            //console.debug("[Session] - all sessions ", sessions)

            return {
                ...state,
                sessions: {
                    data: sessions,
                },
                session: {
                    data: session,
                },
            };
        }
        case DATA.GET: {
            if (typeof action.key === "undefined") {
                return state;
            }
            let otherInfo = {};
            if (typeof action.info !== "undefined") {
                for (const [key, value] of Object.entries(action.info)) {
                    otherInfo[key] = value;
                }
            }

            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    ...otherInfo,
                    data: action.payload,
                    loaded: true,
                    fetching: false,
                    error: {
                        status: false,
                        message: "",
                    },
                },
            };
        }
        case DATA.FETCHING: {
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    fetching: true,
                    loaded: false,
                    error: {
                        status: false,
                        message: "",
                    },
                },
            };
        }
        default:
            return state;
    }
};
