import { DATA } from "@redux/actionTypes";

export const fetchData = (key) => {
  return (dispatch) => {
    dispatch({
      type: DATA.FETCHING,
      key,
    });
  };
};

export const attachEvent = (event) => {
  return (dispatch) => {
    dispatch({
      type: DATA.ATTACH_EVENT,
      payload: event
    });
  };
};

export const addData = (key, data, info) => {
  return (dispatch) => {
    dispatch({
      type: DATA.GET,
      key,
      payload: data,
      info,
    });
  };
};

export const upateStatusData = (data) => {
  return (dispatch) => {
    dispatch({
      type: DATA.UPDATE_STATUS_DATA,
      payload: data,
    });
  };
};

export const updateFavorites = (data) => {
  return (dispatch) => {
    dispatch({
      type: DATA.UPDATE_FAVORITES,
      payload: data,
    });
  };
};

export const updateVoiceMail = (data) => {
  return (dispatch) => {
    dispatch({
      type: DATA.VOICEMAIL,
      payload: data,
    });
  };
};

