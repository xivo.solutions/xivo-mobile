import {PHONE} from '@redux/actionTypes';

export const cancelPhoneEvent = () => {
	return (dispatch) => {
		dispatch({
			type: PHONE.EVENT_CANCEL,
		});
	};
};

export const setPhoneEvent = (phoneEvent) => {
	return (dispatch) => {
		dispatch({
			type: PHONE.EVENT,
			payload: {
				phoneEvent,
			},
		});
	};
};

export const setActivePhoneEvent = () => {
	return (dispatch) => {
		dispatch({
			type: PHONE.EVENT_ACTIVE,
		});
	};
};
export const setSessionPhone = (session, dir, autoAnswer = false) => {
	return (dispatch) => {
		dispatch({
			type: PHONE.SET_SESSION_PHONE,
			payload: {
				session,
				dir,
				autoAnswer,
			},
		});
	};
};

export const setTimeSession = (time, session) => {
	return (dispatch) => {
		dispatch({
			type: PHONE.SET_TIME,
			payload: {
				session,
				time,
			},
		});
	};
};

export const switchSession = (session) => {
	return (dispatch) => {
		dispatch({
			type: PHONE.SWITCH_SESSION,
			payload: {
				session,
			},
		});
	};
};

export const attachPhoneEventOnSession = (phoneEvent) => {
	return (dispatch) => {
		dispatch({
			type: PHONE.ATTACH_PHONE_EVENT,
			payload: {
				phoneEvent,
			},
		});
	};
};

export const cancelSessionPhone = (sessionId) => {
	return (dispatch) => {
		dispatch({
			type: PHONE.CANCEL_SESSION_PHONE,
			payload: {
				sessionId,
			},
		});
	};
};
