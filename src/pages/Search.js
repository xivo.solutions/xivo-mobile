import {
    StyleSheet,
} from "react-native";
import React, {useEffect} from "react";
import {useSelector} from "react-redux";

import Wrapper from "@components/Wrapper";
import Loader from "@components/Loader";
import List from "@components/List";

import SocketsManager from "@helpers/SocketsManager";

const Search: () => React$Node = (props) => {
    const data = useSelector((state) => state.data.search);
    const dataContact = useSelector((state) => state.data.contact);
    const dataStatus = useSelector((state) => state.data.status);

    let search = "";
    if (props.route.params.search) {
        search = props.route.params.search;
    }

    useEffect(() => {
        SocketsManager.cti.getSearch(search);
    }, [search]);

    const compare = (a, b) => {
        if (typeof a.name === "undefined" || typeof b.name === "undefined") {
            return 0;
        }
        const bandA = a.name.toUpperCase();
        const bandB = b.name.toUpperCase();

        let comparison = 0;
        if (bandA > bandB) {
            comparison = 1;
        } else if (bandA < bandB) {
            comparison = -1;
        }
        return comparison;
    };
    let entries = [];
    if (data && data.loaded) {
        entries = [...data.data];
        if (dataContact && dataContact.loaded) {
            dataContact.data.map((contact, index) => {
                if (contact && typeof contact.name !== "undefined" && contact.name.toLowerCase().indexOf(search.toLowerCase()) !== -1) {
                    entries.push({
                        contactId: 0,
                        data: contact,
                        fav: undefined,
                        mobile: "",
                        name: contact.name,
                        num: contact.numbers[0],
                        status: false,
                    });
                }
            });
        }
        entries.sort(compare);
    }
    return (
        <Wrapper
            navigation={props.navigation}
            route={props.route}
            search={search}
            page="search"
        >
            {!data.loaded && (
                <Loader/>
            )}
            {data.loaded && (
                <List
                    entries={entries}
                    search={search}
                />
            )}
        </Wrapper>
    );
};

const styles = StyleSheet.create({});

export default Search;
