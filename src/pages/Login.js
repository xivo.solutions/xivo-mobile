import React, {useEffect, useRef} from "react";
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Linking,
    KeyboardAvoidingView,
    Appearance,
    Platform,
    Alert,
} from "react-native";
import {EventRegister} from "react-native-event-listeners";
import * as Keychain from "react-native-keychain";

import Wrapper from "@components/Wrapper";
import Button from "@components/Button";
import Loader from "@components/Loader";
import inputStyle from "@styles/inputs";
import Colors from "@styles/colors";
import Images from "@assets/images";
import EventNames from "@configs/events";
import PageNames from "@configs/pages";
import Constants from "@configs/constants";

import LoginHelper from "@helpers/Login";

import SocketsManager from "@helpers/SocketsManager";

let cacheSequenceNumber = 0;

const LoginScreen: () => React$Node = (props) => {

    const refLoginInput = useRef();
    const refPasswordInput = useRef();

    let [values, onChangeValues] = React.useState({
        server: "",
        login: "",
        password: "",
    });

    let [displayInterface, setDisplayInterface] = React.useState(false);
    let [loading, setLoading] = React.useState(false);
    let [error, setError] = React.useState(false);


    const onChangeText = (text, id) => {
        values[id] = text;
        onChangeValues(values);
    };

    let blockLogin = false;

    const onLogin = () => {
        console.log("[Login] - User clicked on login button using ", values.server, values.login);
        blockLogin = false;
        setLoading(true);
        LoginHelper.setServer(values.server.trimEnd(), () => {
            LoginHelper.signIn(values.login.trimEnd(), values.password.trimEnd(), (er, data) => {
                if (er) {
                    setError(data);
                    setLoading(false);
                    return;
                }
                startLogin(data.token, values);
            });
        });
    };

    const onCancel = () => {
        let temp = {...values};
        temp.password = "";
        setDisplayInterface(true);
        setLoading(false);    
        onChangeValues(temp);
    };    

    const getCredentials = async () => {
        let temp = {...values};
        try {
            const credentials = await Keychain.getGenericPassword();
            if (credentials) {
                temp.login = credentials.username;
                temp.password = credentials.password;
                onChangeValues(temp);
            } else {
                temp.login = "";
                temp.password = "";
                setDisplayInterface(true);
                setLoading(false);
            }
        } catch (error) {
            console.error("[System] Error Keychain", error);
            temp.login = "";
            temp.password = "";
            setDisplayInterface(true);
            setLoading(false);
            Alert.alert(
                "Connexion",
                "Nous n'arrivons pas à récupérer les informations de connexion. Veuillez resaisir vos identifiants",
                [
                    {
                        text: "OK",
                        style: "cancel",
                    },
                ],
                {
                    cancelable: true,
                },
            );
            temp.login = "";
            temp.password = "";
        }
        return temp;        
    };

    const startSockets = (token) => {        
        if (SocketsManager.cti.status.logged) {
            EventRegister.emit(EventNames.SKIP_LOGIN);
            setLoading(false);
            return;
        }    
        
        SocketsManager.cti.init(token, (success) => {
            if (!success) {
                console.error("[Websockets] - Error while initiliazing using XUC token", error);
            } else {               
                setLoading(false);
                setDisplayInterface(true);
            }   
        });
    }

    const startLogin = async (token, credentials) => {
        if (blockLogin) {
            console.log("[Login] blocked, either because user is logout or process already ongoing")
            return;
        }
        blockLogin = true;

        console.log('[Login] - Starting login process, display loading animation, using token', token);     
        setLoading(true);
        if (token) {
            startSockets(token);
        } else {
            if (!credentials) {            
                setLoading(false);    
                setDisplayInterface(true);                
                return;
            }
            LoginHelper.getValidToken(credentials, (oldOrNewToken) => {
                startSockets(oldOrNewToken);
            });
        }                
    };

    useEffect(() => {
        getCredentials().then((credentials) => {      
            LoginHelper.getServer((u) => {
                credentials.server = u && u !== null ? u : "";                
                if(credentials.login) {
                    console.log('[Login] - credentials found for ', credentials.login, credentials.server);      
                    startLogin(null, credentials);
                }
            });
        }); 
        EventRegister.addEventListener(EventNames.SKIP_LOGIN, () => {
            let routes = props.navigation.getState().routes;

            if (typeof routes[routes.length - 1] !== "undefined") {
                if (routes[routes.length - 1].name === "Login") {
                    props.navigation.navigate(PageNames.Phone);
                }
            }
        });
        EventRegister.addEventListener(EventNames.GO_LOGIN, () => {
            cacheSequenceNumber++;
        });

        return () => {
            EventRegister.removeEventListener(EventNames.SKIP_LOGIN);
            EventRegister.removeEventListener(EventNames.GO_LOGIN);
        };
    }, [cacheSequenceNumber]);

    return (
        <Wrapper
            navigation={props.navigation}
            footer={false}
            header={false}
            error={error}
            setError={setError}
        >

            <KeyboardAvoidingView
                style={styles.inner}
                behavior={Platform.OS === "ios" ? "padding" : "height"}
                enabled={Platform.OS === "ios"}
                keyboardVerticalOffset={100}
            >
                <Images.LogoHome
                    width={240}
                    height={167}
                    style={styles.logo}
                />
                <Text style={styles.text}>
                    Simplicité, évolutivité, performance : bienvenue sur votre espace XiVO. La solution Open Source de
                    téléphonie au service de votre entreprise.
                </Text>

                <Text style={styles.title}>CONNEXION</Text>

                {!loading && displayInterface && (<View style={[inputStyle.container, inputStyle.login]}>
                    <TextInput
                        key={"id " + values.server}
                        editable
                        autoCorrect={false}
                        autoCompleteType="off"
                        placeholder="SERVEUR.SOCIETE.COM"
                        placeholderTextColor={Appearance.getColorScheme() === "dark" ? Colors.greyMedium : Colors.grey}
                        defaultValue={values.server}
                        style={[inputStyle.input]}
                        autoCapitalize="none"
                        onChangeText={(text) => {
                            onChangeText(text, "server");
                        }}
                        onSubmitEditing={() => refLoginInput.current.focus()}
                        returnKeyType="next"
                    />
                </View>)}
                {!loading && displayInterface && (<View style={[inputStyle.container, inputStyle.login]}>
                    <TextInput
                        ref={refLoginInput}
                        key={"login" + values.login}
                        editable
                        autoCorrect={false}
                        autoCompleteType="off"
                        placeholder="IDENTIFIANT"
                        placeholderTextColor={Appearance.getColorScheme() === "dark" ? Colors.greyMedium : Colors.grey}
                        defaultValue={values.login}
                        style={[inputStyle.input]}
                        autoCapitalize="none"
                        onChangeText={(text) => {
                            onChangeText(text, "login");
                        }}
                        onSubmitEditing={() => refPasswordInput.current.focus()}
                        returnKeyType="next"
                    />
                </View>)}
                {!loading && displayInterface && (<View style={[inputStyle.container, inputStyle.login]}>
                    <TextInput
                        ref={refPasswordInput}
                        key={"pwd" + values.password}
                        autoCorrect={false}
                        autoCompleteType="off"
                        secureTextEntry={true}
                        placeholder="MOT DE PASSE"
                        placeholderTextColor={Appearance.getColorScheme() === "dark" ? Colors.greyMedium : Colors.grey}
                        editable
                        style={[inputStyle.input]}
                        defaultValue={values.password}
                        autoCapitalize="none"
                        onChangeText={(text) => {
                            onChangeText(text, "password");
                        }}
                        onSubmitEditing={onLogin}
                        returnKeyType="go"
                    />
                </View>)}

                <View style={styles.wrapperBtn}>
                    {!loading && displayInterface && (
                        <Button onPress={onLogin.bind(this)} text={"Me connecter"} color={"orange"} />
                    )}
                    {loading && (
                        <View>
                            <Loader />
                            <View style={styles.spacing} />
                            <Button onPress={onCancel} text={"Annuler"} color={"red"} />
                        </View>
                    )}
                </View>
            </KeyboardAvoidingView>

            <View style={styles.wrapperMention}>
                <Text
                    style={styles.mentions}
                    onPress={() => {
                        Linking.openURL(Constants.LINK_MENTIONS);
                    }}
                >Mentions légales</Text>
            </View>
        </Wrapper>
    );
};

const styles = StyleSheet.create({
    inner: {
        height: "100%",
        flex: 1,
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
    },
    spacing: {
        height: 50,
    },
    logo: {
        marginBottom: 20,
    },
    text: {
        width: "100%",
        fontFamily: "Gotham",
        color: Colors.black,
        fontSize: 12,
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 30,
        paddingRight: 30,
        textAlign: "center",
        lineHeight: 15,
        marginBottom: 20,
    },
    title: {
        marginBottom: 15,
        color: Colors.orange,
        letterSpacing: 0.8,
        textAlign: "center",
        fontSize: 18,
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Bold",
        } : {}),
    },
    wrapperBtn: {
        height: 20,
        flexDirection: "column",
        alignItems: "center",
        marginTop: 15,
    },
    wrapperMention: {
        flex: 1,
        width: "100%",
        justifyContent: "flex-end",
        alignItems: "flex-end",
    },
    mentions: {
        fontSize: 10,
        paddingRight: 15,
        paddingBottom: 10,
        textAlign: "left",
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Medium",
        } : {}),
    },
});

export default LoginScreen;
