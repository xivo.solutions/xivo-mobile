import {
    StyleSheet,
} from "react-native";
import React, {useEffect} from "react";
import {useSelector} from "react-redux";

import Wrapper from "@components/Wrapper";
import Loader from "@components/Loader";
import List from "@components/List";

import SocketsManager from "@helpers/SocketsManager";

const Favorites: () => React$Node = (props) => {
    const data = useSelector((state) => state.data.favorites);
    const dataStatus = useSelector((state) => state.data.status);

    let params = ["started"];
    if (
        typeof props.route !== "undefined" &&
        typeof props.route.params !== "undefined" &&
        typeof props.route.params.random !== "undefined"
    ) {
        params = [props.route.params.random];
    }
    useEffect(() => {
        SocketsManager.cti.getFavorites();
    }, [...params]);

    return (
        <Wrapper
            navigation={props.navigation}
            page="favorites"
        >
            {!data.loaded && (
                <Loader/>
            )}
            {data.loaded && (
                <List
                    entries={data.data}
                    noResults={"Aucun favoris"}
                />
            )}
        </Wrapper>
    );
};

const styles = StyleSheet.create({});

export default Favorites;
