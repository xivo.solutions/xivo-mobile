import {
    Platform,
    StyleSheet,
    Text,
    View,
} from "react-native";
import React, {useEffect} from "react";
import {useSelector} from "react-redux";
import {EventRegister} from "react-native-event-listeners";

import Wrapper from "@components/Wrapper";
import Images from "@assets/images";
import Colors from "@assets/styles/colors";
import PhoneInterface from "@components/PhoneInterface";
import Timer from "@components/Timer";
import ModalDTMF from "@components/ModalDTMF";
import PhoneMin from "@components/PhoneMin";
import SessionSipManager from "@helpers/SessionSipManager";
import Pages from "@configs/pages";
import EventNames from "@configs/events";
import SoundPlayer from "@helpers/Sound";
import ButtonChildren from "@components/ButtonChildren";
import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";

import {
    faPhone,
    faMicrophoneSlash,
    faVolumeUp,
    faPlus,
    faPlay,
    faPause,
    faTh
} from "@fortawesome/free-solid-svg-icons";

const Phone: () => React$Node = (props) => {

    const data = useSelector((state) => state.data);
    const [mute, setMute] = React.useState(false);
    const [hold, setHold] = React.useState(false);
    const [loudSpeaker, setLoudSpeaker] = React.useState(false);

    let since = 0;
    if (data.session && data.session.data && data.session.data.time) {
        since = data.session.data.time;
    }

    let hasActionCall = false;
    let icon = (null);
    let name = false;
    let num = false;
    let detail = (null);
    let direction = false;
    let text = (null);
    let displaySecondActionCall = false;
    let displayHangup = false;

    let sessions = data.sessions.data;
    let currentSession = data.session.data;
    let currentEventSession = currentSession && currentSession.event ? currentSession.event : false;
    //console.debug("[Phone] - Display current session with attached event ", currentEventSession);

    if (currentSession && currentSession.direction === "out") {
        hasActionCall = true;
        icon = (<Images.OutgoingcallLoad
            width={111.5}
            height={80.8}
        />);
        direction = "APPEL SORTANT";
        detail = (<View style={styles.wrapperTextDetail}>
            <Text style={styles.textDetail}>Votre appel est en transit,</Text>
            <Text style={styles.textDetail}>merci pour votre patience.</Text>
        </View>);
        displayHangup = true;
    }

    if (currentSession && currentSession.direction === "in") {
        hasActionCall = true;
        icon = (<Images.OutgoingcallLoad
            width={111.5}
            height={80.8}
        />);
        direction = "APPEL ENTRANT";
    }

    if (currentEventSession && currentEventSession.eventType === "EventDialing" || currentEventSession.eventType === "EventRinging") {
        hasActionCall = true;
        icon = (<Images.OutgoingcallRinging
            width={111.5}
            height={80.8}
        />);
    }
    if (currentEventSession && currentEventSession.eventType === "EventEstablished" || currentEventSession.eventType === "EventOnHold") {
        hasActionCall = true;
        icon = (<Images.InCall
            width={111.5}
            height={80.8}
        />);
        displaySecondActionCall = true;
    }

    if (currentEventSession && (currentEventSession.eventType === "EventDialing" || currentEventSession.eventType === "EventRinging" || currentEventSession.eventType === "EventEstablished" || currentEventSession.eventType === "EventOnHold")) {
        hasActionCall = true;

        if (currentEventSession.eventType !== "EventDialing") {
            detail = (null);
            displayHangup = false;
        }        
        
        name = currentEventSession.otherDName.trim();
        num = currentEventSession.otherDN.trim();

        if (currentEventSession.callDirection === "Outgoing") {
            direction = "APPEL SORTANT";
        } else {
            direction = "APPEL ENTRANT";
        }
    }

    if (currentEventSession && currentEventSession.eventType === "EventDialing") {
        hasActionCall = true;
        detail = (<View style={styles.wrapperTextDetail}>
            <Text style={styles.textDetail}>Votre appel sonne.</Text>
        </View>);
        displayHangup = true;
    }

    if (currentEventSession === false) {
        name = false;
        num = false;
        detail = (null);
        displayHangup = false;
    }

    const pickUp = () => {
        SessionSipManager.anwser(currentSession.item.idInterne);
    };

    const hangUp = () => {
        SessionSipManager.removeSession();
    };

    const sendDTMF = (value) => {
        if (currentSession !== false) {
            try {
                currentSession.item.sendDTMF(value);
            } catch (e) {
                console.log("[Webrtc] - Error while sending DTMF ",e);
            }
            SoundPlayer.playDTMF();
        }
    };

    const toggleMute = () => {
        let nextMute = SessionSipManager.getMuteCall();
        SessionSipManager.muteCall(false, !nextMute);
        updateBtnStatus();
    };

    const toggleHold = () => {
        let nextHold = SessionSipManager.getHoldCall();
        SessionSipManager.holdCall(false, !nextHold);
        updateBtnStatus();
    };

    const toggleLoudSpeaker = () => {
        SessionSipManager.activeSpeaker(false, !loudSpeaker);
        updateBtnStatus();
    };

    const openDTMF = () => {
        EventRegister.emit(EventNames.OPEN_MODAL_DTMF);
    };


    let otherSessions = [];
    let hasTransfer = false;
    sessions.map((sessionData) => {
        if (typeof sessionData.item !== "undefined" && typeof sessionData.item.autoAnswer !== "undefined") {
            hasTransfer = true;
        }
    });
    sessions.map((sessionData) => {
        if (sessionData.item && currentSession.item && sessionData.item.id !== currentSession.item.id) {            
            otherSessions.push(<PhoneMin
                key={"session" + sessionData.item.id}
                session={sessionData}
                transfer={hasTransfer}
            />);
        }
    });

    const updateBtnStatus = () => {
        if (data.session.data.item) {
            setHold(SessionSipManager.getHoldCall());
            setMute(SessionSipManager.getMuteCall());
            setLoudSpeaker(SessionSipManager.speaker);
        }
    };

    useEffect(() => {
        updateBtnStatus();
    }, [data]);

    useEffect(() => {
        updateBtnStatus();
    }, []);


    return (
        <Wrapper
            page="phone"
            navigation={props.navigation}
            route={props.route}>

            {otherSessions}

            <ModalDTMF sendDTMF={sendDTMF}/>

            <View style={styles.wrapper}>
                {!hasActionCall && (
                    <PhoneInterface/>
                )}

                {hasActionCall && (
                    <View style={styles.wrapperTop}>
                        <View style={styles.wrapperInfo}>
                            {icon}
                            {direction && (<Text style={styles.textDirection}>{direction} {hold ? " (En Attente)" : ""}</Text>)}
                        </View>

                        {(name !== false || num !== false || (icon === null && text)) && (
                            <View style={styles.wrapperText}>
                                <View>
                                    {num !== false && name.length == 0 && (<Text style={styles.textName}>{num}</Text>)}
                                    {name !== false && (<Text style={styles.textName}>{name}</Text>)}
                                    {num !== false && name.length > 0  && (<Text style={styles.textNum}>{num}</Text>)}
                                </View>
                                {icon === null && text && (<Text style={styles.text}>{text}</Text>)}
                            </View>)}
                        {detail}
                    </View>
                )}

                {displaySecondActionCall !== false && (
                    <View style={styles.wrapperActionSupCall}>
                        <View style={styles.wrapperActionSupCallTop}>
                            <ButtonChildren style={styles.btnAdd} onPress={() => {
                                props.navigation.navigate(Pages.Favorites);
                            }}>
                                <FontAwesomeIcon icon={faPlus} color={Colors.grey} size={28}/>
                            </ButtonChildren>

                            <ButtonChildren style={styles.btnPlayPause} onPress={toggleHold}>
                                <FontAwesomeIcon icon={hold ? faPlay : faPause} color={hold ? Colors.green : Colors.grey} size={28}/>
                            </ButtonChildren>

                            <ButtonChildren style={styles.btnDTMF} onPress={openDTMF}>
                                <FontAwesomeIcon icon={faTh} color={Colors.grey} size={28}/>
                            </ButtonChildren>
                        </View>
                        <View style={styles.wrapperActionSupCallBottom}>
                            <Timer since={since} session={currentSession}/>
                        </View>
                    </View>
                )}

                {((currentEventSession && (currentEventSession.eventType === "EventDialing" || currentEventSession.eventType === "EventRinging")) || displayHangup) && (
                    <View style={[styles.wrapperActionCall, styles.wrapperActionCallIn]}>
                        <ButtonChildren style={styles.btnHangUpIn} onPress={hangUp}>
                            <FontAwesomeIcon icon={faPhone} color={Colors.red} size={45} style={styles.btnHangUpIcon}/>
                        </ButtonChildren>

                        {currentEventSession.eventType !== "EventDialing" && !displayHangup && (
                            <ButtonChildren style={styles.btnHangUpIn} onPress={pickUp}>
                                <FontAwesomeIcon icon={faPhone} color={Colors.green} size={45}
                                                 style={styles.btnHangInIcon}/>
                            </ButtonChildren>
                        )}
                    </View>
                )}

                {currentEventSession && currentEventSession.eventType === "EventEstablished" && (
                    <View style={styles.wrapperActionCall}>
                        <ButtonChildren style={styles.btnMute} onPress={toggleMute}>
                            <FontAwesomeIcon icon={faMicrophoneSlash} color={mute ? Colors.orange : Colors.grey}
                                             size={32}/>
                        </ButtonChildren>
                        <ButtonChildren style={styles.btnHangUpIn} onPress={hangUp}>
                            <FontAwesomeIcon icon={faPhone} color={Colors.red} size={45} style={styles.btnHangUpIcon}/>
                        </ButtonChildren>
                        <ButtonChildren style={styles.btnSound} onPress={toggleLoudSpeaker}>
                            <FontAwesomeIcon icon={faVolumeUp} color={loudSpeaker ? Colors.orange : Colors.grey}
                                             size={32}/>
                        </ButtonChildren>
                    </View>
                )}
            </View>
        </Wrapper>);
};

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },

    wrapperTop: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        paddingTop: 25,
    },
    wrapperInfo: {
        alignItems: "center",
    },
    textDirection: {
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Medium",
        } : {}),
        fontSize: 15,
        letterSpacing: 0,
        color: Colors.greyDark,
        marginTop: 10
    },
    wrapperText: {
        width: "100%",
        borderTopWidth: 1,
        borderTopColor: Colors.grey,
        borderBottomWidth: 1,
        borderBottomColor: Colors.grey,
        paddingTop: 15,
        paddingBottom: 15,
        marginTop: 25,
        minHeight: 50,
        backgroundColor: Colors.whiteDark
    },
    textName: {
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Bold",
        } : {}),
        fontSize: 20,
        letterSpacing: 0,
        textAlign: "center",
        color: Colors.orange,
    },
    textNum: {
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham-Light",
        } : {}),
        fontSize: 15,
        marginTop: 5,
        letterSpacing: 0,
        textAlign: "center",
        color: Colors.greyDark,
    },
    text: {
        fontFamily: "Gotham",
        fontSize: 15,
        letterSpacing: 0,
        textAlign: "center",
        color: Colors.greyDark,
    },

    wrapperTextDetail: {
        marginTop: 25,
    },
    textDetail: {
        ...(Platform.OS !== "ios" ? {
            fontFamily: "Gotham",
        } : {}),
        fontSize: 15,
        letterSpacing: 0,
        textAlign: "center",
        color: Colors.greyDark,
    },

    wrapperActionSupCall: {},
    wrapperActionSupCallTop: {
        marginTop: 10,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    btnAdd: {
        paddingTop: 20,
        paddingBottom: 50,
    },
    btnPlayPause: {
        paddingTop: 20,
        paddingBottom: 50,
    },
    btnDTMF: {
        paddingTop: 20,
        paddingBottom: 50,
        paddingRight: 5,
    },
    wrapperActionSupCallBottom: {
        marginTop: 10,
        justifyContent: "center",
        alignItems: "center",
    },

    wrapperActionCall: {
        marginBottom: 50,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: "100%",
    },
    wrapperActionCallIn: {
        justifyContent: "center",
    },
    btnHangUpIn: {
        paddingTop: 25,
        paddingBottom: 55,
    },
    btnHangUpIcon: {
        transform: [{
            rotate: '135deg',
        }]
    },
    btnHangInIcon: {},

    btnMute: {
        paddingTop: 25,
        paddingBottom: 55,
    },
    btnSound: {
        paddingTop: 25,
        paddingBottom: 55,
    },
});

export default Phone;
