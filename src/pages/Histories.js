import {
    StyleSheet,
} from "react-native";
import React, {useEffect} from "react";
import {useSelector} from "react-redux";

import Wrapper from "@components/Wrapper";
import Loader from "@components/Loader";
import List from "@components/List";

import SocketsManager from "@helpers/SocketsManager";

const Histories: () => React$Node = (props) => {
    const data = useSelector((state) => state.data.histories);
    const dataStatus = useSelector((state) => state.data.status);

    let params = ["started"];
    if (
        typeof props.route !== "undefined" &&
        typeof props.route.params !== "undefined" &&
        typeof props.route.params.random !== "undefined"
    ) {
        params = [props.route.params.random];
    }
    useEffect(() => {
        SocketsManager.cti.changePreferenceUser("NB_MISSED_CALL", "0");
        SocketsManager.cti.getHistories();
    }, [...params]);

    return (
        <Wrapper
            navigation={props.navigation}
            page="histories"
        >
            {!data.loaded && (
                <Loader/>
            )}
            {data.loaded && (
                <List
                    entries={data.data}
                    noResults={"Aucun historique"}
                />
            )}
        </Wrapper>
    );
};

const styles = StyleSheet.create({});

export default Histories;
