import React, {useEffect, useState} from "react";
import RNCallKeep from "react-native-callkeep";
import {Provider} from "react-redux";
import { requestOverlayPermission, isOverlayPermissionGranted } from '@vokhuyet/react-native-draw-overlay';
import NetInfo from "@react-native-community/netinfo";
import DeviceInfo from 'react-native-device-info';

import { RequestDisableOptimization, BatteryOptEnabled } from "react-native-battery-optimization-check";
import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import {
    faBatteryQuarter,
    faWindowRestore,
    faBell,
    faMicrophone,
    faPhoneVolume,
    faContactCard,
    faCheck
} from "@fortawesome/free-solid-svg-icons";


import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import {EventRegister} from "react-native-event-listeners";
import {
    Platform,
    BackHandler,
    Modal,
    View,
    Text, StyleSheet,
} from "react-native";
import SplashScreen from "react-native-splash-screen";
import Toast from 'react-native-toast-message';

import {
    PERMISSIONS,
    check, request, checkNotifications, requestNotifications
} from "react-native-permissions";
import AsyncStorage from "@react-native-async-storage/async-storage";
let ReactNativeForegroundService;
if (Platform.OS === 'android') {
  ReactNativeForegroundService = require('rn-foreground-service').default;
} else {
  ReactNativeForegroundService = null;
}

import store from "@redux/store";

import Stepper from "@components/Stepper";
import Login from "@pages/Login";
import Favorites from "@pages/Favorites";
import Histories from "@pages/Histories";
import Search from "@pages/Search";
import Phone from "@pages/Phone";

import PageNames from "@configs/pages";
import EventNames from "@configs/events";

import Constants from "@configs/constants";
import Colors from "@styles/colors";


import Background from "@components/Background";
import SessionSipManager from "@helpers/SessionSipManager";
import SocketsManager from "@helpers/SocketsManager";

const Stack = createStackNavigator();
const sendPings = () => {
    SocketsManager.checkSockets();
};

let permissionAudio = "";
let permissionContact = "";
if (Platform.OS === "ios") {
    permissionAudio = PERMISSIONS.IOS.MICROPHONE;
    permissionContact = PERMISSIONS.IOS.CONTACTS;
} else {
    permissionAudio = PERMISSIONS.ANDROID.RECORD_AUDIO;
    permissionContact = PERMISSIONS.ANDROID.READ_CONTACTS;
}

if (Platform.OS !== "ios") {
    ReactNativeForegroundService.add_task(() => {
        sendPings();
    }, {
        delay: 5000,
        onLoop: true,
        taskId: "xivo-task",
        onError: (e) => console.error('[System] Error while creating foreground task:', e),
    });
}

let disabledObserveChangeState = false;

const App: () => React$Node = () => {
    let [openModalPermission, setOpenModalPermission] = React.useState(false);
    const [active, setActive] = useState(0);

    // CARFEUL - Chaining is mandatory to avoid error while trying to access multiple permissions at once !
    const checkStatusPermissions = async () => {
        check(permissionAudio)
            .then((result) => {
                console.log("[Permissions] - check Audio", result);
                if (result != 'granted') { askPermissionAudio(); }

                RNCallKeep.checkPhoneAccountEnabled().then((status) => {
                    console.log("[Permissions] - check Phone Account Enabled", status);
                    
                    if (Platform.OS !== "ios") {
                        BatteryOptEnabled().then((isEnabled)=>{
                            console.log("[Permissions] - check Battery Optimization disabled", !isEnabled);
                            if (isEnabled) { askPermissionBattery(); }
        
                                isOverlayPermissionGranted().then((status) => {
                                    console.log("[Permissions] - check Overlay Permission", status);
                                    if (!status) { askOverlay(); }

                                    check(permissionContact).then((status) => {
                                        console.log("[Permissions] - check Contacts visibility", status);
                                        if (DeviceInfo.getApiLevelSync() >= 33) {
                                            checkNotifications(
                                                PERMISSIONS.ANDROID.POST_NOTIFICATIONS
                                            ).then((result) => {
                                                console.log("[Permissions] - check Notification permission", result.status);
                                                if(result.status != 'granted') { askPermissionNotif(); }
                                            })
                                        }
                                    });
                                });
                        });
                    }    
                });
            });
    };


    const askOverlay = () => {
        requestOverlayPermission().then((status) => {
            console.log("[Permissions] - Overlay permission asked", status);
            setActive(active + 1);
        });
    };

    const askPermissionAudio = () => {
        request(permissionAudio).then((status) => {
            console.log("[Permissions] - Audio permision asked", status);
            setActive(active + 1);
        });
    }


    const askPermissionNotif = () => {
        if (DeviceInfo.getApiLevelSync() >= 33) {
            requestNotifications(
                PERMISSIONS.ANDROID.POST_NOTIFICATIONS
            ).then( (result) => {
                console.log("[Permissions] - Notification permission asked", result.status)
                setActive(active + 1);            
            }).catch((e) => console.error("[Permissions] - Cannot get notification permission", e));
        } else {
            console.log("[Permissions] - Notification permission ignored, as API version < 33");
            setActive(active + 1);   
        }    
    };


    const askPermissionBattery = () => {
        try {
            RequestDisableOptimization();
        } catch (e) {
            console.error("[Permissions] - Cannot get battery permission", e);
        } finally {
            setActive(active + 1);
        }
    };


    const askPermissionContact = () => {
        request(permissionContact).then((status) => {
            console.log("[Permissions] - Contact permission asked", status);
            setActive(active + 1);
        });
    };

    const allowLogin = () => {
        SessionSipManager.init();
    };


    const checkPermissions = () => {
        try {
            AsyncStorage.getItem(Constants.IS_PERSMISSIONS_ASKED).then((done) =>
            {
                if (done) {
                    checkStatusPermissions().then(() => allowLogin());
                }
                else {
                    setOpenModalPermission(true);
                }
            })              
        }
        catch(e) {
            console.error("[Permissions] - Cannot retrieve saved permissions from storage ", e)
        }
    };

    const getIconPermission = (value) => {
        return <View
            style={styles.iconPermission}
        >
            <FontAwesomeIcon
                icon={value}
                color={Colors.orange}
                size={80}
            />
        </View>
    };

    useEffect(() => {
        SplashScreen.hide();
        checkPermissions();
        
        NetInfo.addEventListener(networkState => {
            console.log("[Network] - Change", "type: " + networkState.type, "Connected: " + networkState.isConnected);
        });
        
        const intervalId = setInterval(() => {      
            if (Platform.OS === "ios") {
                sendPings();
                return;
            }
            if (!ReactNativeForegroundService.is_running()) {
                sendPings();
            }
        }, 5000);                
        
        BackHandler.addEventListener("hardwareBackPress", () => {
            EventRegister.emit(EventNames.BACKBUTTON);
            return true;
        });

        const unsubscribe = NetInfo.addEventListener(state => {
            console.log("[Network] - Received new Connection type ", state.type);
            console.log("[Network] - Is connection up ", state.isConnected);
            SocketsManager.checkNetwork(state);
        });

        return () => {
            BackHandler.removeEventListener("hardwareBackPress");
            clearInterval(intervalId);
            unsubscribe();
        };

    }, []);


    let content = [
        (
            <View style={styles.wrapperStep}>
                <Text style={styles.wrapperTextStep}>
                    Pour la bonne utilisation de l'application nous allons vous
                    demander {Platform.OS === "ios" ? "deux " : "six "}
                    autorisations.
                </Text>
                <Text style={styles.wrapperTextSupStep}>L'application XiVO n'utilise pas vos données
                    en dehors du bon fonctionnement
                    de
                    l'application.</Text>
            </View>
        ),
        (
            <View style={styles.wrapperStep}>
                <Text style={styles.wrapperTextStep}>L'arrêt de l'optimisation de la batterie pour
                    que l'application soit priorisée
                    par
                    le téléphone même en économie de batterie.</Text>
                {getIconPermission(faBatteryQuarter)}
            </View>
        ),
        (
            <View style={styles.wrapperStep}>
                <Text style={styles.wrapperTextStep}>La possibilité de mettre la fenêtre d'appel au dessus des autres
                    applications.</Text>
                    {getIconPermission(faWindowRestore)}
            </View>
        ),
        (
            <View style={styles.wrapperStep}>
                <Text style={styles.wrapperTextStep}>Accepter les notifications pour 
                    recevoir l' information d'un appel manqué.
                </Text>
                    {getIconPermission(faBell)}
            </View>
        ),
        (
            <View style={styles.wrapperStep}>
                <Text style={styles.wrapperTextStep}>L'accès au micro permettant de gérer votre son
                    pendant les appels.</Text>
                    {getIconPermission(faMicrophone)}
            </View>
        ),        
        (
            <View style={styles.wrapperStep}>
                <Text style={styles.wrapperTextStep}>L'accès aux contacts vous permettra d'ajouter
                    votre annuaire de téléphone à la
                    recherche XiVO.</Text>
                    {getIconPermission(faContactCard)}
            </View>
        ),        
        (
            <View style={styles.wrapperStep}>
                <Text style={styles.wrapperTextStep}>
                    Xivo a besoin de votre autorisation pour passer les appels.
                    Nous allons activer un compte téléphonique Xivo.
                </Text>
                <Text style={styles.wrapperTextSupStep}>
                    Si ce compte n'est pas actif l'application ne fonctionnera pas correctement.
                </Text>
                <View style={styles.iconPhone}>
                    {getIconPermission(faPhoneVolume)}
                </View>
            </View>
        ),
        (
            <View style={styles.wrapperStep}>
                <Text style={styles.wrapperTextStep}>Merci, votre application est prête à l'emploi !</Text>
                    {getIconPermission(faCheck)}
            </View>
        )   
    ];

    if (Platform.OS === "ios") {
        content.splice(1, 2);
        content.splice(2, 1);
    }

    return (
        <Provider store={store}>
            <Modal visible={openModalPermission} onRequestClose={() => {
                //setOpenModalPermission(false);
            }}>
                <View style={styles.wrapperStepper}>
                    <Stepper
                        active={active}

                        content={content}
                        onFinish={() => {
                            try {
                                AsyncStorage.setItem(Constants.IS_PERSMISSIONS_ASKED, "done").then(() => {
                                    console.log("[Permissions] - Writing permissions in storage");
                                    setOpenModalPermission(false);
                                    allowLogin();
                                });                                
                            } catch (error) {
                                console.error("[Permissions] - An error has occured while writing permissions in storage", error);
                            } 
                        }}
                        onNext={() => {
                            if (active === 0) {
                                setActive((p) => p + 1);
                            }

                            if (Platform.OS === "ios") {
                                if (active === 1) {
                                    disabledObserveChangeState = true;
                                    askPermissionAudio();
                                }
                                if (active === 2) {
                                    askPermissionContact();
                                }  
                                if (active === 3) {
                                    allowLogin();
                                    setActive(active + 1);
                                }                                        
                            } else  {
                                if (active === 1) {
                                    disabledObserveChangeState = true;
                                    askPermissionBattery();
                                }
                                if (active === 2) {
                                    askOverlay();
                                }

                                if (active === 3) {
                                    askPermissionNotif();
                                }

                                if (active === 4) {
                                    askPermissionAudio();
                                }

                                if (active === 5) {
                                    askPermissionContact();
                                }

                                if (active === 6) {
                                    allowLogin();
                                    setActive(active + 1);
                                }
                            }    
                        }}
                    />
                </View>
            </Modal>

            <Background/>
            <NavigationContainer>
                <Stack.Navigator
                    duration={0}
                    screenOptions={{
                        headerShown: false,
                    }}
                >
                    <Stack.Screen
                        name={PageNames.Login}
                        component={Login}
                        options={{
                            animationEnabled: false,
                        }}
                    />
                    <Stack.Screen
                        name={PageNames.Favorites}
                        component={Favorites}
                        options={{
                            animationEnabled: false,
                        }}
                    />
                    <Stack.Screen
                        name={PageNames.Histories}
                        component={Histories}
                        options={{
                            animationEnabled: false,
                        }}
                    />
                    <Stack.Screen
                        name={PageNames.Search}
                        component={Search}
                        options={{
                            animationEnabled: false,
                        }}
                    />

                    <Stack.Screen
                        name={PageNames.Phone}
                        component={Phone}
                        options={{
                            animationEnabled: false,
                        }}
                    />
                </Stack.Navigator>
            </NavigationContainer>
            <Toast />
        </Provider>
    );
};


const styles = StyleSheet.create({
    wrapperTextSupStep: {
        fontSize: 20,
    },
    wrapperTextStep: {
        marginBottom: 40,
        marginTop: 40,
        fontSize: 20,
    },
    iconPermission: {
        flex: 1,
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
    },
    iconPhone: {
        paddingTop: 30
    },
    wrapperStepper: {
        padding: 25,
        paddingTop: Platform.OS == 'ios' ? 100 : 25
    },
    wrapperStep: {
        flex: 1,
        height: "100%",
        paddingTop: 20,
    },
});
export default App;
