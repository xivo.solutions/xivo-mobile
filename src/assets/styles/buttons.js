import {StyleSheet, Platform} from "react-native";
import Colors from "@styles/colors";

export default StyleSheet.create({
	btn: {
		width: 220,
		height: 45,
		fontSize: 16,
		borderRadius: 25,
	},
	text: {
		flex: 1,
		alignSelf: "center",
		lineHeight: 45,
		fontSize: 12,
		...(Platform.OS !== "ios" ? {
			fontFamily: "Gotham-Medium",
		} : {}),
		textTransform: "uppercase",
		color: Colors.white,
	},
	orange: {
		backgroundColor: Colors.orange,
	},
	green: {
		backgroundColor: Colors.green,
	},
	red:{
		backgroundColor: Colors.red,
	},
	grey:{
		backgroundColor: Colors.grey,
	}
});
