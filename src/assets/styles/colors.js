export default {
  white: "#FFFFFF",
  whiteDark: "#EFEFEF",
  black: "#000000",
  orange: "#FF6308",
  red: "#C42F00",
  grey: "#8E8E93",
  greyLight: "#F8F8F8",
  greyMedium: "#D3D3D3",
  greyDark: "#4B4B4B",
  blueLight: "#DCF4FB",
  green: "#5cb85c"
};

export const PHONE_STATUS_COLOR = {
  "16": "#F7FE2E",
  "8": "#2E2EFE",

  "9": "#CC2EFA",
  "1": "#E67D39",
  "2": "#81BEF7",

  "0": "#2BA41B",

  "4": "#959595",
  "-1": "#959595",
  "-2": "#959595",
  "-99": "#959595",
};
