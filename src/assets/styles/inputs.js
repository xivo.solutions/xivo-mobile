import { Platform, StyleSheet } from "react-native";
import Colors from "@styles/colors";


const inputStyle = {
  height: 40,
  width: "100%",
  fontSize: 15,
  ...(Platform.OS !== "ios" ? {
    fontFamily: "Gotham-Book",
  } : {}),
};
export default StyleSheet.create({
  container: {
    height: 40,
    backgroundColor: Colors.white,
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 25,
    paddingLeft: 15,
    paddingRight: 15,
  },
  server: {
    marginRight: 15,
    marginLeft: 15
  },
  login: {
    height: 50,
    marginRight: 15,
    marginLeft: 15,
    marginTop: 5,
    marginBottom: 5
  },
  input: inputStyle,
  inputSearch: {
    ...inputStyle,
  }
});
