import Logo from "@assets/svg/connexion_logo.svg";
import Connexion from "@assets/svg/connexion.svg";
import Star from "@assets/svg/star.svg";
import Phone from "@assets/svg/phone.svg";
import LogoSmall from "@assets/svg/logo.svg";
import LogoHome from "@assets/svg/xivo_couleur2.svg";
import LogOut from "@assets/svg/power-off-solid.svg";
import Pause from "@assets/svg/pause.svg";
import Play from "@assets/svg/play.svg";
import Mute from "@assets/svg/mute.svg";
import MuteGrey from "@assets/svg/mute-grey.svg";
import UnMute from "@assets/svg/unmute.svg";
import Kick from "@assets/svg/kick.svg";
import Sound from "@assets/svg/sound.svg";
import MobileState from "@assets/svg/mobile_state.svg";
import RenvoiState from "@assets/svg/renvoi_state.svg";
import ChevronDown from "@assets/svg/chevron_down.svg";
import VoiceMail from "@assets/svg/voicemail.svg";
import SearchBack from "@assets/svg/search_back.svg";

import CallIncoming from "@assets/svg/incoming_call.svg";
import CallOutgoing from "@assets/svg/outgoing_call.svg";
import CallMissing from "@assets/svg/call_missed.svg";

import OutgoingcallRinging from "@assets/svg/outgoingcall_ringing.svg";
import InCall from "@assets/svg/incoming_incall.svg";
import OutgoingcallLoad from "@assets/svg/outgoingcall_anim_4.svg";

import LogoWebRTC from "@assets/svg/webrtc_logo.svg";
import LogoMobile from "@assets/svg/mobile_logo.svg";
import LogoWebRTCMobile from "@assets/svg/webrtcmobile_logo.svg";

import Dnd from "@assets/svg/status/dnd.svg";
import Mobile from "@assets/svg/status/mobile_logo.svg";
import MobileForward from "@assets/svg/status/mobile_forward_logo.svg";
import MobileForwardNa from "@assets/svg/status/mobile_forward_na_logo.svg";
import WebRTCForward from "@assets/svg/status/webrtc_forward_logo.svg";
import WebRTC from "@assets/svg/status/webrtc_logo.svg";
import WebRTCForwardNa from "@assets/svg/status/webrtc_forward_na_logo.svg";
import WebrtcMobileForward from "@assets/svg/status/webrtcmobile_forward_logo.svg";
import WebrtcMobileForwardNa from "@assets/svg/status/webrtcmobile_forward_na_logo.svg";
import WebrtcMobile from "@assets/svg/status/webrtcmobile_logo.svg";

export default {
	Logo,
	Connexion,
	Star,
	Phone,
	LogoSmall,
	LogOut,
	Pause,
	Play,
	Mute,
	UnMute,
	Kick,
	MuteGrey,
	Sound,
	MobileState,
	ChevronDown,
	VoiceMail,
	CallIncoming,
	CallOutgoing,
	CallMissing,
	OutgoingcallRinging,
	OutgoingcallLoad,
	InCall,
	SearchBack,
	RenvoiState,
	LogoHome,
	LogoWebRTC,
	LogoMobile,
	LogoWebRTCMobile,
	Dnd,
	Mobile,
	MobileForward,
	MobileForwardNa,
	WebRTCForward,
	WebRTC,
	WebRTCForwardNa,
	WebrtcMobileForward,
	WebrtcMobileForwardNa,
	WebrtcMobile

};
