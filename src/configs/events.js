export default {
  APP_CHANGE_STATE: "App/change/state",
  CTI: {
    ADD_DATA: "App/CTI/add/data",
    ATTACH_EVENT: "App/CTI/attach/event",
    FETCH_DATA: "App/CTI/fetch/data",
    UPDATE_STATUS_DATA: "App/CTI/update/status/data",
    UPDATE_FAVORITES: "App/CTI/update/favorites/data",
    UPDATE_VOICEMAIL: "App/CTI/update/voicemail/data",
  },
  BACKBUTTON: "App/Back/Button",
  OPENMENU: "App/open/menu",
  CLOSEMENU: "App/close/menu",
  GO_LOGIN: "App/go/login",
  SKIP_LOGIN: "App/skip/Login",
  OPEN_SEARCH: "App/Open/Search",
  CLOSE_SEARCH: "App/Close/Search",
  DISPLAY_PHONE: "App/Phone/ShowIncomingCallUi",
  OPEN_MODAL_DTMF: "App/Modal/Open/DTMF",
  CLOSE_ALL_MODAL: "App/Modals/Close/All",
  INTERNET_OFF: "App/Internet/Off",
  INTERNET_ON: "App/Internet/On",
  OPEN_ALERT_ROUTING: 'App/Modals/Open/alert/Routing'
};
