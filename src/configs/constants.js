const key = "@AvencallApp";
export default {
    KEY_STORAGE: key,
    STORAGE_SERVER_WSS: key + ":server_wss",
    STORAGE_SERVER_URL: key + ":server_url",
    STORAGE_LOGIN_CTI: key + ":login_cti",
    STORAGE_LOGIN_SIP: key + ":login_spi",
    STORAGE_ICE_CONFIG: key + ":ice_config",
    STORAGE_ICE_CONFIG_TIME: key + ":ice_config_time",
    STORAGE_LINECONFIG: key + ":lineconfig",
    STORAGE_SERVER_WSS: key + ":server_wss",
    STORAGE_SERVER_PUSH_TOKEN_SENT: key + ":push_token",
    STORAGE_NUMBER_TRANSFERT: key + ":number-transfert",
    STORAGE_NOTIFICATION_MISSING: key + ":history-notification-count",
    IS_PERSMISSIONS_ASKED: key + ":permissions-ask",
    LINK_MENTIONS: "https://www.xivo.solutions/mentions-legales/",
    SENDER_ID: "0033437497810",
    TYPES_ITEM: {
        NUMBER: "numero",
        CONF: "conference",
    },
    ROUTING: {
        webAppApp: "WebAppAndMobileApp",
        app: "MobileApp",
        webApp: "WebApp"
    },
    DATE_FORMAT: "YYYY-MM-DD HH:mm:ss"
};