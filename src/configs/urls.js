let domain = "xivocc-demo.xivo.solutions";
let endWs = "/xuc/api/2.0/cti?token=";
let endWsSip = "/wssip";
let endAPI = "/xuc/api/2.0/";
export default {
	URL_WS: "wss://" + domain + endWs,
	URL_API_REST: "https://" + domain + endAPI,
	DOMAIN: domain,
	DOMAIN_SIP: "xivo-demo.xivo.solutions",
	END_API: endAPI,
	END_WS: endWs,
	END_WS_SIP: endWsSip,
};
